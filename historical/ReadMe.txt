The CVS tree structures, jMusic/jm and jMusic/qt, here at SourceForge are no longer in use.
Please use jMusic/src for the latest jMusic code base, and jMusic/inst for the latest jMusic instruments.