/*

<This Java Class is part of the jMusic API version 1.4, February 2003.>

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

package jm.audio.synth;

import java.io.IOException;
import java.io.EOFException;
import jm.music.data.Note;
import jm.audio.AudioObject;
import jm.audio.Instrument;
import jm.audio.AOException;
import jm.JMC;

/**
 * Envelope which can be set with an arbitrary number of points
 * the envelope is constructed with linear lines between each 
 * specifed point.
 * The points excepted by this class are positioned as a percent
 * of the total length of the sound data being nextWorked on and 
 * the envelope itself is constructed in the build() method.<br>
 * Envelope objects can be used as either Generator Audio Objects
 * (ie the first in the chain) or as processor Audio Objects (ie
 * in the centre or the chain) depending on the constructor used.<br>
 * As a generator the Envelope can be used to pass each envelope position
 * onto another Audio Object as input data.<br>
 * As a processor the Envelope object is used to change the Amplitude
 * of incoming samples to reflect the shape of the envelope.<br>
 * NOTE: The important distinction here is that when being used as
 * a processor object the envelopes only possible function is to 
 * alter amplitude. But when used as a generator the Envelope can
 * be used to send data to any AudioObjects input. (the volume of a
 * volume object for example for doing crescendos on each note)
 * @author Andrew Sorensen, Andrew Brown, Joel Joslin
 * @version 1.0,Sun Feb 25 18:42:47  2001
 */
public class ADSR extends AudioObject implements JMC{
	//----------------------------------------------
	// Attributes
	//----------------------------------------------
	/** points on the graph */
	private EnvPoint[] graphPoints;
	/** a calculated graph with all points filled in */
	private float[] graphShape;
	/** how far through the envelope shape we are */
	private int position = 1;
	/** is the a primary object? */
	private boolean primary;
	// ADSR values
	private int attack, decay, release;
	private double sustain;
        // the number of samples that takes into account the release time
        private int totalSamples;
        // keep track of the number of sampled processed
        private int sampleCounter = 0;

	//----------------------------------------------
	// Constructors
	//----------------------------------------------
	
	/**
	 * An Envelope object can be used as a generator. This
	 * is a method to call to do this. 
	 * @param sampleRate the sampleRate for this AudioObject.
	 * @param @param breakPoints the data to construct the envelope from
	 */
	public ADSR(Instrument inst, int sampleRate, int channels,
             int attack, int decay, double sustain, int release){
            super(inst, sampleRate, "[Envelope]");
            this.channels = channels;
            this.attack = attack;
            this.decay = decay;
            this.sustain = sustain;
            this.release = release;            
            this.primary = true;
	}
	
	   /**
	 * This constructor takes a single AudioObject as input
	 * and in this form becomes a processor object which
	 * changes the amplitude of incoming samples based on
	 * the envelope. 
	 * @param breakPoints the data to construct the envelope from
	 * @param ao the single AudioObject to use as input. 
	 */
	public ADSR(AudioObject ao, int attack, int decay, double sustain, int release){
            super(ao, "[Envelope]");
            // ADSR -> points
            this.attack = attack;
            this.decay = decay;
            this.sustain = sustain;
            this.release = release;
            this.primary = false;
	}
	
	//----------------------------------------------
	// Protected Methods
	//----------------------------------------------
	/**
	 * Alter the samples value so that it meets the 
	 * shape of the graph, then send the new sample 
	 * onto the next audio object.<br>
	 * NOTE: if the nextWork method receives a value
	 * of 1.0 the graphs current poitional value
	 * will be passed on unchanged.
	 * @param input input data 
	 */
	public int work(float[] buffer)throws AOException{
            // claculate the number of samples processed
	if (sampleCounter > totalSamples * this.channels){ 
		this.finished = true;
		this.inst.finishedNewData = true;
	}else{
		this.inst.finishedNewData = false;
	}
		

        sampleCounter += buffer.length;
        // pass on data unchanged after the end of the envelope
        if(this.finished==true && this.inst.iterations<=0)return buffer.length;
        // process data
        if (primary) {
            int returned = buffer.length;
            int chancount=1;
            for(int i=0;i<returned;i++){
                    try{
                            buffer[i] = graphShape[this.position];
                            //System.out.println("buffer = " + buffer[i]);
                    }catch(ArrayIndexOutOfBoundsException aob){
                            buffer[i] = 0.0f;
                    }
                    if(chancount==channels){
                            chancount=1;
                            this.position++;
                            //System.out.println("Position incremeted to" + position);
                    }else{chancount++;}
            }
            return returned;
        } else {
            //System.out.println("in NOT primary");
            int returned = this.previous[0].nextWork(buffer);
            int chancount=1;
            for(int i=0;i<buffer.length;i++){
                    try{
                            //System.out.println("before BUF: "+buffer[i]);
                            buffer[i]=buffer[i] * graphShape[this.position];
                    }catch(ArrayIndexOutOfBoundsException aob){
                            buffer[i] = 0.0f;
                    }
                    if(chancount==channels){
                            chancount=1;
                            this.position++;
                    }else{chancount++;}
            }
            return returned;
        }
    }	


	//----------------------------------------------
	// Private Methods
	//----------------------------------------------

	private double getSamps(int milli){
	    return ((double)milli/1000.0)*(double)sampleRate;
	}
	
	/** 
	 * Calculates the sampleData for this Envelope
	 */
	public void build(){
		this.inst.finishedNewData = false;
		sampleCounter = 0;
		if(numOfSamples == 0){
			return;
		}
		// extend note to account for release
		// note; numOfSamples is in mono
		int durationAsSamples = this.numOfSamples + (int)getSamps(release);
		// new length for processing that takes release into account
		totalSamples = durationAsSamples; // * this.channels;
		
		//currentNote.setDuration(currentNote.getDuration() + (release / 1000.0)); // tempo
		// calulate new graph points
		//double noteTime = currentNote.getDuration(); // account for tempo! 60/tempo

		double attackPercentage = getSamps(attack)/(double)durationAsSamples;
		// check for overrun
		double maxPercentage = (double)this.numOfSamples /(double)durationAsSamples;
		if (attackPercentage > maxPercentage) attackPercentage = maxPercentage;
		double decayPercentage = getSamps(decay)/(double)durationAsSamples;
		// check for overrun
		if (attackPercentage + decayPercentage > maxPercentage)
		    decayPercentage = maxPercentage - attackPercentage;
		double releasePercentage = getSamps(release)/(double)durationAsSamples;
		double sustainPercentage = 1.0 - attackPercentage - decayPercentage - releasePercentage;

		// create an array of time - amp pairs
		double[] breakPoints = {0.0, 0.0, 
		    attackPercentage, 1.0, 
		    attackPercentage + decayPercentage, sustain, 
		    sustainPercentage + attackPercentage + decayPercentage, sustain,
		    sustainPercentage + attackPercentage + decayPercentage + (0.35 * releasePercentage), sustain * 0.2,
		    sustainPercentage + attackPercentage + decayPercentage + (0.65 * releasePercentage), sustain * 0.01,
		    1.0, 0.0};
		//
		this.graphPoints = new EnvPoint[breakPoints.length /2];
		int counter = 0;
		for (int i = 0; i < (breakPoints.length / 2); i++) {
		    graphPoints[i] = new EnvPoint((float)breakPoints[counter], 
		    (float)breakPoints[counter + 1]);
		    counter += 2;
		    // Continue past the end of the note length
		    this.finished=false;
		}
		
		
            //Adjust the Envelope to suit the length of the waveform
            if(this.graphPoints[0].x != -1.0){
                    for(int i=0; i<this.graphPoints.length; i++){
                            this.graphPoints[i].X = ((int)((float)(totalSamples) * this.graphPoints[i].x));
                            this.graphPoints[i].X -= 1;
                    }
            }
            
            //Make the new wave shape
            //Calculate linear lines between EnvPoints
            graphShape = new float[totalSamples];
            int j = 0;
            for(int i=0;i<(this.graphPoints.length)-1;i++){
                    float gradient = 
                            (this.graphPoints[i].y - this.graphPoints[i+1].y) / 
                            (this.graphPoints[i].X - this.graphPoints[i+1].X);
                    float yintercept = this.graphPoints[i+1].y - 
                            (gradient * this.graphPoints[i+1].X);
                    for(;;){

                            this.graphShape[j] = (gradient * (float)j) + yintercept;
                            if(j == this.graphPoints[i+1].X) break;
                            j++;
                    }

            }
		for(int k = 0;k<graphShape.length;k++){
		}
            this.position = 0; //this is set to -1 to start channels at 0
	}
}
