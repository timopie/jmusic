/*
 * Read.java 0.1.0.3 24th January 2001
 *
 * Copyright (C) 2000 Adam Kirby
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package jm.util.read2;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import jm.midi.SMF;
import jm.music.data.Score;

/**
 * A supplement to {@link jm.util.Read} that adds advanced messaging support
 * and dual MIDI/jm import.  Eventually these methods should be a part of that
 * class.
 *
 * @author Adam Kirby
 * @version 0.1.0.3, 24th January 2001
 */
public class Read {
    /** Not meant to be instantiated, only provides static members */
    private Read() {
    }

    /** Error message to display */
    private static String message = null;

    /**
     * Returns a Score read from a MIDI or JM file, without displaying error
     * messages.
     *
     * @param file  File to read
     * @return      Score read from file, or null if an error occured
     *
     * @see #midiOrJmWithNoMessaging(String, String)
     */
    public static synchronized Score midiOrJmWithNoMessaging(final File file) {
        if (file == null) {
            message = "The selected file is null.  No JM/MIDI information "
                      + "could be imported.";
            return null;
        }
        if (file.isDirectory()) {
            message = "The selected file is a directory.  No JM/MIDI "
                      + "information could be imported.";
            return null;
        }
        return midiOrJmWithNoMessaging(file.getParent() + file.separator,
                                       file.getName());
    }

    /**
     * Returns a Score read from a MIDI or JM file, without displaying error
     * messages.
     *
     * <P>The path of the file is separated into <CODE>directoryName</CODE> and
     * <CODE>fileName</CODE> so that the latter can be used as the title of the
     * score.  If <CODE>directoryName</CODE> is null then this method attempts
     * to read the file specified by <CODE>fileName</CODE>.
     *
     * @param directoryName String describing the directory structure of the
     *                      file to be read, which must include the terminating
     *                      separator
     * @param fileName      String describing the file name
     * @return              Score read from file, or null if an error occured
     *
     * @see #midiOrJmWithNoMessaging(File)
     */
    public static synchronized Score midiOrJmWithNoMessaging(
            final String directoryName, final String fileName) {
        if (fileName == null) {
            message = "The fileName String is null.  No JM/MIDI information "
                      + "could be imported.";
            return null;
        }

        Score score = new Score(fileName);
        message = null;

        /** Attempt to read file */
        try {
            if (directoryName == null) {
                new SMF().read(fileName, score);
            } else {
                new SMF().read(directoryName + fileName, score);
            }
        } catch (IOException e1) {
            message = e1.getMessage();
            if (message == null) {
                message = "Unknown IO Exception";
                score = null;
            } else if (message.equals("Track Started in wrong place!!!!"
                                      + "  ABORTING")) {
                message = "The MIDI file corrupted.  Track data started in the"
                          + " wrong place.";
                score = null;
            } else if (message.equals("This is NOT a MIDI file !!!")) {
                try {
                    FileInputStream fis = new FileInputStream(directoryName
                                                              + fileName);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    score = (Score) ois.readObject();
                    ois.close();
                    fis.close();
                } catch (SecurityException e2) {
                    message = "Read access not allowed to " + fileName;
                    score = null;
                } catch (ClassNotFoundException e2) {
                    message = "The file " + fileName
                              + " is neither a jm nor a MIDI file";
                    score = null;
                } catch (ClassCastException e2) {
                    message = "The file " + fileName 
                              + " is neither a jm nor a MIDI file";
                    score = null;
                } catch (StreamCorruptedException e2) {
                    message = "The file " + fileName 
                              + " is neither a jm nor a MIDI file";
                    score = null;
                } catch (IOException e2) {
                    message = e2.getMessage ();
                    if (message == null) {
                        message = "Unknown Exception.  No musical information "
                                  + "could be imported.";
                    }
                    score = null;
                }
            } else {
                score = null;
            }
        }
        return score;
    }

    /**
     * Returns a Score read from a MIDI or JM file, displaying errors in a
     * {@link Dialog}.
     *
     * <P>This method is for Dialogs whose control is to be taken from another
     * Dialog.  If the owner is a Frame use {@link
     * #midiOrJmWithAWTMessaging(File, Frame)} instead.
     *
     * @param file  File to read
     * @param owner Dialog whose control is to be suspended while the error
     *              messages are displayed
     * @return      Score read from file, or null if an error occured
     *
     * @see #midiOrJmWithAWTMessaging(File, Frame)
     * @see #midiOrJmWithAWTMessaging(String, String, Dialog)
     */
    public static synchronized Score midiOrJmWithAWTMessaging(final File file,
            final Dialog owner) {
        Score score = midiOrJmWithNoMessaging(file);
        displayErrorDialog(owner);
        return score;
    }

    /**
     * Returns a Score read from a MIDI or JM file, displaying errors in a
     * {@link Dialog}.
     *
     * <P>The path of the file is separated into <CODE>directoryName</CODE> and
     * <CODE>fileName</CODE> so that the latter can be used as the title of the
     * score.  If <CODE>directoryName</CODE> is null then this method attempts
     * to read the file specified by <CODE>fileName</CODE>.
     *
     * <P>This method is for Dialogs whose control is to be taken from another
     * Dialog.  If the owner is a Frame use {@link
     * #midiOrJmWithAWTMessaging(String, String, Frame)} instead.
     *
     * @param directoryName String describing the directory structure of the
     *                      file to be read, which must include the terminating
     *                      separator
     * @param fileName      String describing the file name
     * @param owner         Dialog whose control is to be suspended while the
     *                      error messages are displayed
     * @return              Score read from file, or null if an error occured
     *
     * @see #midiOrJmWithAWTMessaging(String, String, Frame)
     * @see #midiOrJmWithAWTMessaging(File, Dialog)
     */
    public static synchronized Score midiOrJmWithAWTMessaging(
            final String directoryName, final String fileName,
            final Dialog owner) {
        Score score = midiOrJmWithNoMessaging(directoryName, fileName);
        displayErrorDialog(owner);
        return score;
    }

    /**
     * Displays an error message in a {@link Dialog}.  The message displayed is
     * retrieved from the class variable {@link #message}.  This method is
     * designed for use by the {@link #midiOrWithAWTMessaging()} methods.
     *
     * <P>This method is for Dialogs whose control is to be taken from a Dialog.
     * If the owner is a Frame use {@link #displayErrorDialog(Frame)} instead.
     *
     * @param owner Dialog whose control is to be suspended while the error
     *              messages are displayed
     *
     * @see displayErrorDialog(Frame)
     * @see #midiOrJmWithAWTMessaging(File, Dialog);
     * @see #midiOrJmWithAWTMessaging(String, String, Dialog);
     */
    private static synchronized void displayErrorDialog(final Dialog owner) {
        if (message == null) {
                return;
        }
        Dialog dialog = new Dialog(owner, "Not a valid MIDI or jMusic File",
                                   true);
        completeErrorDialog(dialog);
    }

    /**
     * Returns a Score read from a MIDI or JM file, displaying errors in a
     * {@link Dialog}.
     *
     * <P>This method is for Dialogs whose control is to be taken from a Frame.
     * If the owner is a Dialog use {@link #midiOrJmWithAWTMessaging(File,
     * Dialog)} instead.
     *
     * @param file  File to read
     * @param owner Frame whose control is to be suspended while the error
     *              messages are displayed
     * @return      Score read from file, or null if an error occured
     *
     * @see #midiOrJmWithAWTMessaging(File, Dialog)
     * @see #midiOrJmWithAWTMessaging(String, String, Frame)
     */
    public static synchronized Score midiOrJmWithAWTMessaging(final File file,
              final Frame owner) {
        Score score = midiOrJmWithNoMessaging(file);
        displayErrorDialog(owner);
        return score;
    }

    /**
     * Returns a Score read from a MIDI or JM file, displaying errors in a
     * {@link Dialog}.
     *
     * <P>The path of the file is separated into <CODE>directoryName</CODE> and
     * <CODE>fileName</CODE> so that the latter can be used as the title of the
     * score.  If <CODE>directoryName</CODE> is null then this method attempts
     * to read the file specified by <CODE>fileName</CODE>.
     *
     * <P>This method is for Dialogs whose control is to be taken from a Frame.
     * If the owner is a Dialog use {@link #midiOrJmWithAWTMessaging(String,
     * String, Dialog)} instead.
     *
     * @param directoryName String describing the directory structure of the
     *                      file to be read, which must include the terminating
     *                      separator
     * @param fileName      String describing the file name
     * @param owner         Frame whose control is to be suspended while the
     *                      error messages are displayed
     * @return              Score read from file, or null if an error occured
     *
     * @see #midiOrJmWithAWTMessaging(String, String, Dialog)
     * @see #midiOrJmWithAWTMessaging(File, Frame)
     */
    public static synchronized Score midiOrJmWithAWTMessaging(
            final String directoryName, final String fileName,
            final Frame owner) {
        Score score = midiOrJmWithNoMessaging(directoryName, fileName);
        displayErrorDialog(owner);
        return score;
    }

    /**
     * Displays an error message in a {@link Dialog}.  The message displayed is
     * retrieved from the class variable {@link #message}.  This method is
     * designed for use by the {@link #midiOrWithAWTMessaging()} methods.
     *
     * <P>This method is for Dialogs whose control is to be taken from a Frame.
     * If the owner is a Dialog use {@link #displayErrorDialog(Dialog)} instead.
     *
     * @param owner Frame whose control is to be suspended while the error
     *              messages are displayed
     *
     * @see displayErrorDialog(Frame)
     * @see #midiOrJmWithAWTMessaging(File, Dialog);
     * @see #midiOrJmWithAWTMessaging(String, String, Dialog);
     */
    private static synchronized void displayErrorDialog(final Frame owner) {
        if (message == null) {
                return;
        }
        Dialog dialog = new Dialog(owner, "Not a valid MIDI or jMusic File",
                                   true);
        completeErrorDialog(dialog);
    }

    /**
     * Adds supplementary components to the error dialog and displays it.  This
     * method is executed by both {@link displayErrorDialog(Frame)} and {@link
     * dispayedErrorDialog(Dialog)} and stores the code common to both.
     *
     * @param dialog    Dialog that is to display the error message.
     */
    private static synchronized void completeErrorDialog(final Dialog dialog) {
        dialog.add(new Label(message), BorderLayout.CENTER);

        Button okButton = new Button("OK");
        okButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dialog.dispose();
                }
            }
        );
        Panel buttonPanel = new Panel();
        buttonPanel.add(okButton);
        dialog.add(buttonPanel, BorderLayout.SOUTH);

        dialog.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    dialog.dispose();
                }
            }
        );
        dialog.pack();
        dialog.show();
    }

    /**
     * Returns a Score read from a MIDI or JM file, displaying errors in a
     * {@link JDialog}.
     *
     * @param file  File to read
     * @param owner Component whose control is to be suspended while the error
     *              messages are displayed
     * @return      Score read from file, or null if an error occured
     *
     * @see #midiOrJmWithSwingMessaging(String, String, Component)
     */
    public static synchronized Score midiOrJmWithSwingMessaging(final File file,
            final Component owner) {
        Score score = midiOrJmWithNoMessaging(file);
        displayErrorJDialog(owner);
        return score;
    }

    /**
     * Returns a Score read from a MIDI or JM file, displaying errors in a
     * {@link JDialog}.
     *
     * <P>The path of the file is separated into <CODE>directoryName</CODE> and
     * <CODE>fileName</CODE> so that the latter can be used as the title of the
     * score.  If <CODE>directoryName</CODE> is null then this method attempts
     * to read the file specified by <CODE>fileName</CODE>.
     *
     * @param directoryName String describing the directory structure of the
     *                      file to be read, which must include the terminating
     *                      separator
     * @param fileName      String describing the file name
     * @param owner         Component whose control is to be suspended while the
     *                      error messages are displayed
     * @return              Score read from file, or null if an error occured
     *
     * @see #midiOrJmWithSwingMessaging(File, Component)
     */
    public static synchronized Score midiOrJmWithSwingMessaging(
            final String directoryName, final String fileName,
            final Component owner) {
        Score score = midiOrJmWithNoMessaging(directoryName, fileName);
        displayErrorJDialog(owner);
        return score;                                                   
    }

    /**
     * Displays an error message in a {@link JDialog}.  The message displayed is
     * retrieved from the class variable {@link #message}.  This method is
     * designed for use by the {@link #midiOrWithSwingMessaging()} methods.
     *
     * @param owner Component whose control is to be suspended while the error
     *              messages are displayed
     *
     * @see #midiOrJmWithSwingMessaging(File, Component);
     * @see #midiOrJmWithSwingMessaging(String, String, Component);
     */
    private static synchronized void displayErrorJDialog(
            final Component owner) {
        if (message == null) {
                return;
        }
        JOptionPane.showMessageDialog(owner, message,
                                      "Not a valid MIDI or jMusic File",
                                      JOptionPane.ERROR_MESSAGE);
    }
}


