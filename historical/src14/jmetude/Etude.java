package jmetude;

import java.util.Vector;
import java.net.URL;
import java.net.MalformedURLException;

import jm.JMC;
import jm.music.data.Score;
import jm.music.data.Part;
import jm.music.data.Phrase;
import jm.music.data.Note;
import jm.music.data.Rest;
import jm.util.Play;
import jm.util.Write;
import jm.util.Read;
import jm.music.tools.Mod;
import processing.core.PApplet;

/**
 * 
 * @author Daniel Dihardja
 * 
 */
public class Etude implements EtudeConst {
	
	/** The score list */
	Vector scores;
	
	/** The part list */
	Vector parts;
	
	/** The phrase list */
	Vector phrases;
	
	/** The processing applet */
	PApplet parent;
	
	public Etude() {
		scores = new Vector();
		parts = new Vector();
		phrases = new Vector();		
	}
	
	/**
	 * Create a new instance of etude
	 * @param p PApplet The processing applet
	 */
	public Etude(PApplet p) {
		scores = new Vector();
		parts = new Vector();
		phrases = new Vector();
		parent = p;
		parent.registerDispose(this);
	}

	/**
	 * Anything in here will be called automatically when 
     * the parent applet shuts down. for instance, this might
     * shut down a thread used by this library.
	 * 
	 */
	public void dispose() {
		stopMIDI();
	}

	// Score Methods

	/**
	 * Create a new score
	 * @param title String The title of the score
	 */
	public void createScore(String title) {
		Score s = new Score(title);
		s.setTempo(120.0);
		scores.add(s);
	}

	/**
	 * Create a score from a MIDI file
	 * @param title String  The title of the score
	 * @param midiFile String the name of the MIDI file
	 */
	public void createScore(String title, String midiFile) {
		Score s = new Score(title);
		scores.add(s);
		
		if(midiFile.startsWith("http://")) {
			URL url = null;
			try {
				url = new URL(midiFile);
			}
			catch(MalformedURLException e) {
				handleException(e);
			}
			Read.midi(s, url);
		}
		else {
			Read.midi(s, midiFile);
		}
		
		Vector tmpParts = s.getPartList();
		for (int i = 0; i < tmpParts.size(); i++) {
			String partTitle = "part_" + i;
			Part tmpPart = (Part) tmpParts.get(i);
			tmpPart.setTitle(partTitle);
			parts.add(tmpPart);
			Vector tmpPhrases = tmpPart.getPhraseList();
			for (int j = 0; j < tmpPhrases.size(); j++) {
				String phraseTitle = partTitle + "_phrase_" + j;
				Phrase tmpPhrase = (Phrase) tmpPhrases.get(j);
				tmpPhrase.setTitle(phraseTitle);
				phrases.add(tmpPhrase);
			}
		}
	}
	
	/**
	 * Add a part to a score
	 * @param scoreTitle String The title of the score
	 * @param partTitle String The title of the part
	 */
	public void addScorePart(String scoreTitle, String partTitle) {
		Score s = null;
		Part p = null;
		try {
			s = getScore(scoreTitle);
			p = getPart(partTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		s.addPart(p);
	}

	/**
	 * Get the list of parts from a score
	 * @param scoreTitle The title of the score
	 * @return The parts from the score
	 */
	public String[] getScoreParts(String scoreTitle) {
		Score s = null;
		try {
			s = getScore(scoreTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		Vector tmpParts = s.getPartList();
		String[] parts = new String[tmpParts.size()];
		for (int i = 0; i < tmpParts.size(); i++) {
			Part tmp = (Part) tmpParts.get(i);
			parts[i] = tmp.getTitle();
		}
		return parts;
	}

	/**
	 * Remove a part from a score
	 * @param scoreTitle The title of the score
	 * @param partTitle The title of the part
	 */
	public void removeScorePart(String scoreTitle, String partTitle) {
		Score s = null;
		Part p = null;
		try {
			s = getScore(scoreTitle);
			p = getPart(partTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		s.removePart(p);
	}

	// Part Methods

	/**
	 * Create a new part
	 * @param title The title of the part
	 */
	public void createPart(String partTitle) {
		createPart(partTitle, 0, 0);
	}
	
	/**
	 * 
	 * @param title
	 * @param instrument
	 */
	public void createPart(String partTitle, int instrument) {
		createPart(partTitle,instrument,0);
	}
	
	/**
	 * 
	 * @param title
	 * @param instrument
	 * @param channel
	 */
	public void createPart(String title, int instrument, int channel) {
		Part p = new Part(title, instrument, channel);
		p.setTempo(120);
		parts.add(p);
	}
	
	/**
	 * Add a phrase to a part
	 * @param partTitle The title of the part
	 * @param phraseTitle The title of the phrase
	 */
	public void addPartPhrase(String partTitle, String phraseTitle) {
		Part p = null;
		Phrase ph = null;
		try {
			p = getPart(partTitle);
			ph = getPhrase(phraseTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		p.addPhrase(ph);
	}

	/**
	 * Get the list of phrases from a part
	 * @param partTitle the title of the part
	 * @return The phrases from the part
	 */
	public String[] getPartPhrases(String partTitle) {
		Part p = null;
		try {
			p = getPart(partTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		Vector tmpPhrases = p.getPhraseList();
		String[] phrases = new String[tmpPhrases.size()];
		for (int i = 0; i < tmpPhrases.size(); i++) {
			Phrase tmp = (Phrase) tmpPhrases.get(i);
			phrases[i] = tmp.getTitle();
		}
		return phrases;
	}

	/**
	 * Remove a phrase from a part
	 * @param partTitle The title of the part
	 * @param phraseTitle The title of the phrase
	 */
	public void removePartPhrase(String partTitle, String phraseTitle) {
		Part p = null;
		Phrase ph = null;
		try {
			p = getPart(partTitle);
			ph = getPhrase(phraseTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		p.removePhrase(ph);
	}

	/**
	 * Set the instrument of a part
	 * @param instrument The number of the instrument
	 */
	public void setPartInstrument(String partTitle, int instrument) {
		Part p = null;
		try {
			p = getPart(partTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		p.setInstrument(instrument);
	}

	/**
	 * Get the instrument of a part
	 * @param partTitle The title of the part
	 * @return The number of the instrument
	 */
	public int getPartInstrument(String partTitle) {
		Part p = null;
		try {
			p = getPart(partTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		return p.getInstrument();
	}
	
	/**
	 * Set the MIDI channel of a part
	 * @param partTitle the title of the part
	 * @param channel The number of the MIDI channel
	 */
	public void setPartChannel(String partTitle, int channel) {
		Part p = null;
		try {
			p = getPart(partTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		p.setChannel(channel);
	}

	/**
	 * Get the MIDI channel from the part
	 * @param partTitle The title of the part
	 * @return The number of the MIDI channel
	 */
	public int getPartChannel(String partTitle) {
		Part p = null;
		try {
			p = getPart(partTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		return p.getChannel();
	}
	
	/**
	 * Set the pan of the part
	 * @param partTitle The title of the part
	 * @param pan the value of the pan (0.0 - 1.0)
	 */
	public void setPartPan(String partTitle, float pan) {
		Part p = null;
		try {
			p = getPart(partTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}		
		p.setPan((double)pan);
	}
	
	/**
	 * Get the pan value from the part
	 * @param partTitle The title of the part
	 * @return The value of the pan
	 */
	public float getPartPan(String partTitle) {
		Part p = null;
		try {
			p = getPart(partTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		return (float)p.getPan();		
	}
	
	/**
	 * Create a new phrase
	 * @param phraseTitle The title of the phrase
	 */
	public void createPhrase(String phraseTitle) {
		float[][] notes = null;
		createPhrase(phraseTitle, notes);
	}
	
	/**
	 * 
	 * @param phraseTitle
	 * @param note
	 */
	public void createPhrase(String phraseTitle, float[] note) {
		float notes[][] = new float[1][2];
		notes[0] = note;
		createPhrase(phraseTitle, notes);
	}
	
	/**
	 * create a new phrase and set the notes 
	 * @param phraseTitle
	 * @param notes
	 */
	public void createPhrase(String phraseTitle, float[][] notes) {
		Phrase ph = new Phrase(phraseTitle, 0.0);
		ph.setTempo(120);
		phrases.add(ph);
		if(notes != null) addPhraseNote(phraseTitle, notes);
	}
	
	/**
	 * Set the start time of the phrase
	 * @param phraseTitle The title of the phrase
 	 * @param startTime The value of the start time
	 */
	public void setPhraseStartTime(String phraseTitle, float startTime) {
		Phrase ph = null;
		try {
			ph = getPhrase(phraseTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		ph.setStartTime((double) startTime);
	}

	/**
	 * Get the start time from a phrase
	 * @param phraseTitle The title of the phrase
	 * @returnc The value of the start time
	 */
	public float getPhraseStartTime(String phraseTitle) {
		Phrase ph = null;
		try {
			ph = getPhrase(phraseTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		return (float) ph.getStartTime();
	}
	
	/**
	 * Repeat a phrase n times
	 * @param phraseTitle String the title of the phrase
	 * @param num int the value of how many times the phrase
	 * should be repeated
	 */
	public void repeatPhrase(String phraseTitle, int num) {
		Phrase ph = null;
		try {
			ph = getPhrase(phraseTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		Mod.repeat(ph, num);
	}
	
	/**
	 * Add a note to a phrase
	 * @param phraseTitle the title of the phrase
	 * @param note An array which contains the pitch and the rhitm value
	 */
	public void addPhraseNote(String phraseTitle, float[] note) {
		Phrase ph = null;
		try {
			ph = getPhrase(phraseTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		if (note[0] > 0.0) ph.addNote((int) note[0], (double) note[1]);
		else ph.addRest(new Rest(note[1]));
	}
	
	/**
	 * add a list of notes to a phrase
	 * @param phraseTitle String the title of the phrase
	 * @param note float[][] the list of notes where a single
	 * note is an array with 2 elements, pitch and rhytm value
	 */
	public void addPhraseNote(String phraseTitle, float[][] note) {
		for(int i=0; i<note.length;i++) {
			addPhraseNote(phraseTitle, note[i]);
		}
	}
	
	/**
	 * Add a list of notes to a phrase
	 * @param phraseTitle The title of the phrase
	 * @param notes A multi dimensional array which contains
	 * note arrays, a note array contains the pitch and the rhitm value
	 */
	public void addPhraseNoteList(String phraseTitle, float[][] notes) {
		Phrase ph = null;
		try {
			ph = getPhrase(phraseTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		for(int i=0; i<notes.length;i++) {
			addPhraseNote(phraseTitle, notes[i]);
		}
	}
	
	/**
	 * Get a list of the notes from a phrase
	 * @param String phraseTitle
	 * @return A multi dimensional array which contains
	 * note arrays, a note array contains the pitch and the rhitm value
	 */
	public float[][] getPhraseNotes(String phraseTitle) {
		Phrase ph = null;
		try {
			ph = getPhrase(phraseTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		Vector tmp = ph.getNoteList();
		float[][] notes = new float[tmp.size()][2];
		for (int i = 0; i < tmp.size(); i++) {
			Note n = (Note) tmp.get(i);
			notes[i][0] = (n.getPitch() >= 0) ? (float)n.getPitch() : -1;
			notes[i][1] = (float)n.getRhythmValue();
		}
		return notes;
	}

	/**
	 * Set the instrument of a phrase
	 * @param phraseTitle The title of the phrase
	 * @param instrument The number of the instrument
	 */
	public void setPhraseInstrument(String phraseTitle, int instrument) {
		Phrase ph = null;
		try {
			ph = getPhrase(phraseTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		ph.setInstrument(instrument);
	}

	/**
	 * Get the instrument of a phrase
	 * @param phraseTitle The title of the phrase
	 * @return The number of the instrument
	 */
	public int getPhraseInstrument(String phraseTitle) {
		Phrase ph = null;
		try {
			ph = getPhrase(phraseTitle);
		} 
		catch (Exception e) {
			handleException(e);
		}
		return ph.getInstrument();
	}

	// UTILITY METHODS

	/**
	 * Set the volume for a score, part or phrase
	 * depends on the title passed to this method
	 * @param String title The title of the object
	 * @param int volume The value of the volume
	 */
	public void setVolume(String title, int volume) {
		Object o = null;
		try {
			o = getObjectByTitle(title);
		} 
		catch (Exception e) {
			handleException(e);
		}
		if (o instanceof Score) {
			Score s = (Score) o;
			s.setVolume(volume);
		} 
		else if (o instanceof Part) {
			Part p = (Part) o;
			p.setVolume(volume);
		} 
		else if (o instanceof Phrase) {
			Phrase ph = (Phrase) o;
			ph.setVolume(volume);
		}
	}

	/**
	 * Get the volume for a score, part or phrase
	 * depends on the title passed to this method
	 * @param String title The title of the object
	 * @return
	 */
	public int getVolume(String title) {
		Object o = null;
		int volume = 0;
		try {
			o = getObjectByTitle(title);
		} 
		catch (Exception e) {
			handleException(e);
		}
		if (o instanceof Score) {
			Score s = (Score) o;
			volume = s.getVolume();
		} 
		else if (o instanceof Part) {
			Part p = (Part) o;
			volume = p.getVolume();
		} 
		else if (o instanceof Phrase) {
			Phrase ph = (Phrase) o;
			volume = ph.getVolume();
		}
		return volume;
	}

	/**
	 * Set the tempo for a score, part or phrase
	 * depends on the title passed to this method
	 * @param String title The title of the object
	 * @param float tempo The value of the tempo
	 */
	public void setTempo(String title, float tempo) {
		Object o = null;
		try {
			o = getObjectByTitle(title);
		} 
		catch (Exception e) {
			handleException(e);
		}
		if (o instanceof Score) {
			Score s = (Score) o;
			s.setTempo((double) tempo);
		} 
		else if (o instanceof Part) {
			Part p = (Part) o;
			p.setTempo((double) tempo);
		} 
		else if (o instanceof Phrase) {
			Phrase ph = (Phrase) o;
			ph.setTempo((double) tempo);
		}
	}

	/**
	 * Get the tempo from a score, part or phrase
	 * depends on the title passed to this method
	 * @param String title The title of the object
	 * @return float The value of the tempo
	 */
	public float getTempo(String title) {
		Object o = null;
		float tempo = 0.0f;
		try {
			o = getObjectByTitle(title);
		} 
		catch (Exception e) {
			handleException(e);
		}
		if (o instanceof Score) {
			Score s = (Score) o;
			tempo = (float) s.getTempo();
		} 
		else if (o instanceof Part) {
			Part p = (Part) o;
			tempo = (float) p.getTempo();
		} 
		else if (o instanceof Phrase) {
			Phrase ph = (Phrase) o;
			tempo = (float) ph.getTempo();
		}
		return tempo;
	}

	/**
	 * Get the end time of a score, part or phrase
	 * @param String title The title of the object
	 * @return float The value of the end time
	 */
	public float getEndTime(String title) {
		Object o = null;
		float endTime = 0.0f;
		try {
			o = getObjectByTitle(title);
		} 
		catch (Exception e) {
			handleException(e);
		}
		if (o instanceof Score) {
			Score s = (Score) o;
			endTime = (float) s.getEndTime();
		} 
		else if (o instanceof Part) {
			Part p = (Part) o;
			endTime = (float) p.getEndTime();
		} 
		else if (o instanceof Phrase) {
			Phrase ph = (Phrase) o;
			endTime = (float) ph.getEndTime();
		}
		return endTime;
	}

	/**
	 * Set a new title for a score, part or phrase
	 * @param String title The title of the object
 	 * @param String newTitle The new title
	 */
	public void setTitle(String title, String newTitle) {
		Object o = null;
		try {
			o = getObjectByTitle(title);
		} 
		catch (Exception e) {
			handleException(e);
		}
		if (o instanceof Score) {
			Score s = (Score) o;
			s.setTitle(newTitle);
		} 
		else if (o instanceof Part) {
			Part p = (Part) o;
			p.setTitle(newTitle);
		} 
		else if (o instanceof Phrase) {
			Phrase ph = (Phrase) o;
			ph.setTitle(newTitle);
		}
	}
	
	/**
	 * Transpose a score, parrt or a phrase
	 * @param String title The title of the object
	 * @param int transposition The value of the transposition
	 */
	public void transpose(String title, int transposition) {
		Object o = null;
		try {
			o = getObjectByTitle(title);
		} 
		catch (Exception e) {
			handleException(e);
		}
		if (o instanceof Score) {
			Score s = (Score) o;
			Mod.transpose(s,transposition);
		} 
		else if (o instanceof Part) {
			Part p = (Part) o;
			Mod.transpose(p,transposition);
		} 
		else if (o instanceof Phrase) {
			Phrase ph = (Phrase) o;
			Mod.transpose(ph,transposition);
		}		
	}
	
	/**
	 * This method delete all data from a score,
	 * part or phrase
	 * @param String title The title of the object
	 */
	public void clear(String title) {
		Object o = null;
		try {
			o = getObjectByTitle(title);
		} 
		catch (Exception e) {
			handleException(e);
		}
		if (o instanceof Score) {
			Score s = (Score) o;
			s.empty();
		}
		else if (o instanceof Part) {
			Part p = (Part) o;
			p.empty();
		}
		else if (o instanceof Phrase) {
			Phrase ph = (Phrase) o;
			ph.empty();
		}
	}

	/**
	 * Create a MIDI file from a score, part or a phrase
	 * @param String title The title of the object
	 * @param String fileName The name of the MIDI file
	 */
	public void createMIDI(String title, String fileName) {
		Object o = null;
		try {
			o = getObjectByTitle(title);
		} catch (Exception e) {
			handleException(e);
		}
		if (o instanceof Score) {
			Score s = (Score) o;
			Write.midi(s, fileName);
		}
		else if (o instanceof Part) {
			Part p = (Part) o;
			Write.midi(p, fileName);
		}
		else if (o instanceof Phrase) {
			Phrase ph = (Phrase) o;
			Write.midi(ph, fileName);
		}
	}

	/**
	 * Play a score, part or phrase in a MIDI format
	 * @param String title the title of the object
	 */
	public void playMIDI(String title) {
		Object o = null;
		try {
			o = getObjectByTitle(title);
		} catch (Exception e) {
			handleException(e);
		}
		if (o instanceof Score) Play.midi((Score) o, false);
		else if (o instanceof Part) Play.midi((Part) o, false);
		else if (o instanceof Phrase)Play.midi((Phrase) o, false);
	}
	
	/**
	 * Stop playing MIDI
	 *
	 */
	public void stopMIDI() {
		Play.stopMidi();
	}
	
	/**
	 * return a score with the given title
	 * @param String title The title of the score we want to get
	 * @return Score The score object
	 * @throws Exception If no score found 
	 */
	private Score getScore(String title) throws Exception {
		for (int i = 0; i < scores.size(); i++) {
			Score tmp = (Score) scores.get(i);
			if (tmp.getTitle().equals(title)) return tmp;
		}
		throw new Exception("Sorry, could not find score '" + title + " '");
	}

	/**
	 * 
	 * @param title
	 * @return
	 * @throws Exception
	 */
	private Part getPart(String title) throws Exception {
		for (int i = 0; i < parts.size(); i++) {
			Part tmp = (Part) parts.get(i);
			if (tmp.getTitle().equals(title)) return tmp;
		}
		throw new Exception("Sorry, could not find part '" + title + " '");
	}

	/**
	 * 
	 * @param title
	 * @return
	 * @throws Exception
	 */
	private Phrase getPhrase(String title) throws Exception {
		for (int i = 0; i < phrases.size(); i++) {
			Phrase tmp = (Phrase) phrases.get(i);
			if (tmp.getTitle().equals(title)) return tmp;
		}
		throw new Exception("Sorry, could not find part '" + title + " '");
	}

	/**
	 * 
	 * @param title
	 * @return
	 * @throws Exception
	 */
	private Object getObjectByTitle(String title) throws Exception {
		Object obj = null;

		try {
			obj = getPhrase(title);
		} 
		catch (Exception e) {}

		try {
			obj = getPart(title);
		} 
		catch (Exception e) {}

		try {
			obj = getScore(title);
		} 
		catch (Exception e) {}

		if (obj == null)
			throw new Exception(
					"Sorry, could not find any phrase, part or score with the title ' "
							+ title + " '");
		return obj;
	}

	/**
	 * Handle the exception
	 * @param e
	 */
	private void handleException(Exception e) {
		parent.stop();
		e.printStackTrace();
		System.exit(0);
	}
}