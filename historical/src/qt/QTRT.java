/*

< This Java Class is part of the jMusic API Version 2001.01>

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/ 
package qt;

import jm.JMC;
import jm.music.data.*;
import qt.QTUtil;
import jm.util.View;
import jm.music.tools.Mod;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;

/**
* QuickTime Real Time playback for jMusic.
* Score 1 is used first then score 2 and repeat infinitly.
* While one score is playing the other score is updating.
* Any class calling QTRT must implement two methods,
* updateScore1() and updateScore2()
* It is the responsibility of the calling app to
* update the scores as required. Syncronisation of
* this can be assisted by polling the value of the
* score1PlayingFlag boolean variable.
* A swing GUI is provided to adjust the scheduling 
* parameters as required.
* The scoreLength argument is critical as it determines the
* size of the scores created when updateScore methods
* are called, which (of course) also changes the delay 
* between update calls. It can be changed using the
* setScoreLength() method.
* Remember to call the startPlayback() method (only once) or else
* nothing will happen ;) Use suspendPlayback() and
* resumePlayback() to interupt the playback once started.
*
* @param Score the first score object
* @param Score the first score object
* @param Object the class the calls this class
*
* @author Andrew Brown
*/

public final class QTRT implements JMC, ChangeListener, ActionListener{
	private Thread myThread;
	private QTUtil qtu1; 
	private Object obj;
	private Score score = new Score();
	private double tempo = 120.0; // bpm
	private double nextTempo = 0.0; // bpm, 0.0 == off
	private double scoreLength = CROTCHET;
	// non accessable attributes
	private double time;
	private double waitTime;
	private double sleepTime = 480.0;
	private double theGap = 10.0;
	private JSlider tempoSlider, sleepSlider, gapSlider;
	private JLabel tempoNumber, sleepNumber, gapNumber;
	private JButton startButton, stopButton;
	private boolean newScoreFlag = true;
	private int cycleCount = 0;
	
	public QTRT(Score score) {
        this.obj = obj;
        this.score = score;
        this.scoreLength = score.getEndTime();
		qtu1 = new QTUtil(this.score); 
        
        // Thread Stuff - inner class
        myThread = new Thread(new Runnable() {
            public void run() { 
                while (true) {
                    playCycle();
                    try { Thread.sleep((int)(sleepTime * scoreLength * 60.0/tempo)); 
                    }
	                catch( InterruptedException e) {}
	                // sleep a bit to give the GC and other threads a chance to act
                    if (!newScoreFlag) System.gc(); // if (tuneFlag) if tempi < 130 /
	            }
	        }
        } );
        
        myThread.setPriority(Thread.MAX_PRIORITY);   
	}
	
	/**
	* Begin the cycling playback of the scores.
	*/
	public void startPlayback() {
	    myThread.start();
	    time = System.currentTimeMillis();
	}
	
	/**
	* Shedule score playback na d score updating when required.
	*/
	private void playCycle() {
	    waitTime = time + (1000.0 * scoreLength * 60.0/tempo) - theGap;
	    
	    if (newScoreFlag) {
	        //double ts = System.currentTimeMillis();
	        qtu1.setScore(score);
	        newScoreFlag = false;
	        //double te = System.currentTimeMillis();
	        //System.out.println("Time taken to set score was " + (te - ts) + " milliseconds");
	    }
	    while (System.currentTimeMillis() < waitTime) {};
	    //double t1 = System.currentTimeMillis();
	    updateTempo();
	    qtu1.setSpeed(tempo/60.0);
	    qtu1.replay();
	    time = System.currentTimeMillis();
	    cycleCount ++;
	    //System.out.println("Time taken by QTU was " + (time - t1) + " milliseconds");
	}
	
	/**
	* Specifies the length of a score to be played.
	* Changes the interval between score update calls.
	*/
	public void setScoreLength(double newLength) {
	    if (newLength > 0.0) scoreLength = newLength;
	}
	
	/**
	* Reports the current score length setting.
	*/
	public double getScoreLength() {
	    return scoreLength;
	}
	
	/**
	* Specifies the  score to be played.
	*/
	public void setScore(Score s) {
	    this.score = s;
	    scoreLength = s.getEndTime();
	    newScoreFlag = true;
	}
	
	/**
	* Specifies the speed in beats per minute.
	*/
	public void setTempo(double newTempo) {
	    if (newTempo > 0.0) tempo = newTempo;
	}
	
	/**
	* Reports the current speed in beats per minute.
	*/
	public double getTempo() {
	    return tempo;
	}
	
	/**
	* Specifies the speed in beats per minute.
	*/
	public void setNextTempo(double newTempo) {
	    if (newTempo > 0.0) nextTempo = newTempo;
	}
	/**
	* Updates the current speed in beats per minute
	* based on a waiting tempo change.
	* This prevents gaps or overlaps in the playback
	* which can occur from the use of setTempo()
	* which updates immediatly.
	*/
	public void updateTempo() {
	    if (nextTempo != 0.0) {
	        tempo = nextTempo;
	        nextTempo = 0.0;
	    }
	}
	
	/**
	* Specify the number ot count cycles from.
	*/
	public void setCycleCount(int newCount) {
	    this.cycleCount = newCount;
	}
	
	/**
	* Report the number of times the cycle method has been run.
	* Useful for counting beats , bars, phrases or 
	* whatever the chunk passed to the QTRT.
	*/
	public int getCycleCount() {
	    return this.cycleCount;
	}
	
	/**
	* If paused, restart playback.
	*/
	public void resumePlayback() {
	    myThread.resume();
	}
	
	/**
	* Pause playback.
	*/
	public void suspendPlayback() {
	    myThread.suspend();
	    qtu1.stopPlayback();
	}
	
	/**
	* Update the sleep time value..
	*/
	public void setSleepTime( double newSleepTime) {
	    this.sleepTime = newSleepTime;
	}
	
	
	/**
	* Report the current loop gap time.
	*/
	public double getSleepTime() {
	    return this.sleepTime;
	}
	
	/**
	* Update the sleep time value..
	*/
	public void setTheGap( double newGap) {
	    this.theGap = newGap;
	}
	
	
	/**
	* Report the current loop gap time.
	*/
	public double getTheGap() {
	    return this.theGap;
	}
	
	/**
	* Open a GUI with sliders to adjust the scheduler loading.
	* Used to optimise the timing and efficiecy of the real time
	* QuickTime playback.
	*/
	public void settings() {
		JFrame f = new JFrame("Thread GUI");
		f.setSize(400, 200);
		Box masterBox = new Box(0);
		// tempo
		Box b1 = new Box(1);
		JLabel tempoTitle = new JLabel("Tempo");
		b1.add(tempoTitle);
		tempoSlider = new JSlider(1, 20, 200, 120);
		tempoSlider.addChangeListener(this);
		b1.add(tempoSlider);
		tempoNumber = new JLabel("120");
		b1.add(tempoNumber);
		masterBox.add(b1);
		
		// sleep time
		Box b2 = new Box(1);
		JLabel sleepTitle = new JLabel("Sleep");
		b2.add(sleepTitle);
		sleepSlider = new JSlider(1, 0, 1200, (int)(sleepTime)); // 570 G4, 300 G3
		sleepSlider.addChangeListener(this);
		b2.add(sleepSlider);
		sleepNumber = new JLabel(Double.toString(sleepTime));
		b2.add(sleepNumber);
		masterBox.add(b2);
		
		// gap time
		Box b3 = new Box(1);
		JLabel gapTitle = new JLabel("Gap");
		b3.add(gapTitle);
		gapSlider = new JSlider(1, 0, 100, (int)(theGap));
		gapSlider.addChangeListener(this);
		b3.add(gapSlider);
		gapNumber = new JLabel(Double.toString(theGap));
		b3.add(gapNumber);
		masterBox.add(b3);
		
		// buttons
		Box b4 = new Box(1);
		startButton = new JButton("Start");
		startButton.addActionListener(this);
		b4.add(startButton);
		stopButton = new JButton("Stop");
		stopButton.addActionListener(this);
		b4.add(stopButton);
		masterBox.add(b4);
		
		f.getContentPane().add(masterBox);
		f.setVisible(true);
	}
	
	public void stateChanged(ChangeEvent e) {
		if(e.getSource() == tempoSlider) changeTempo();
		if(e.getSource() == sleepSlider) changeSleep();
		if(e.getSource() == gapSlider) changeGap();
	}
	
	private void changeTempo() {
		tempo = (double)(tempoSlider.getValue());
		tempoNumber.setText(Integer.toString(tempoSlider.getValue()));
	}
	
	private void changeSleep() {
		sleepTime = (double)(sleepSlider.getValue());
		sleepNumber.setText(Integer.toString(sleepSlider.getValue()));
	}
	
	private void changeGap() {
		theGap = (double)(gapSlider.getValue());
		gapNumber.setText(Integer.toString(gapSlider.getValue()));
	}
	
	public void actionPerformed(ActionEvent ae) {
	    if(ae.getSource() == startButton) myThread.resume();
	    if(ae.getSource() == stopButton) myThread.suspend();
	}
}
