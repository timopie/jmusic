/*
 * KeyChangeListener.java 0.0.1.0 20th February 2001
 *
 * Copyright (C) 2000 Adam Kirby
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package jmx.gui.cpn;

/**
 * The listener interface for receiving notification of a change of key.
 *
 * @author Adam Kirby
 * @version 1.0,Sun Feb 25 18:35:33  2001
 */
public interface KeyChangeListener {
    /**
     * Respond to key change.
     */
    public void keyChanged();
}
