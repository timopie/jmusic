/*

<This Java Class is part of the jMusic API version 1.0,Sun Feb 25 18:35:25  2001

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/ 

package jmx.util;

import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import javax.swing.JOptionPane;

import jm.midi.SMF;
import jm.music.data.*;
import jm.JMC;
import jm.gui.show.*;
import jm.gui.cpn.*;
import jm.gui.sketch.*;
import jm.audio.Instrument;
import jm.audio.Audio;

/**                                   
 * Supplements the {@link jm.util.Read} class, by providing methods while output
 * errors to Swing dialog boxes.
 *
 * @author Unascribed, Adam Kirby
 * @version 0.0.0.1, 27th February 2001
 */
public class Read extends jm.util.Read implements JMC{

    //--------- Simultaneous midi and jm reading with error messaging --------//

    /**
     * Returns a Score read from a MIDI or JM file, displaying errors in a
     * {@link javax.swing.JDialog}.
     *
     * @param file  File to read
     * @param owner Component whose control is to be suspended while the error
     *              messages are displayed
     * @return      Score read from file, or null if an error occured
     *
     * @see #midiOrJmWithSwingMessaging(String, String, Component)
     */
    public static Score midiOrJmWithSwingMessaging(final File file,
                                                   final Component owner) {
        JmMidiProcessor processor = new JmMidiProcessor(file);
        displayErrorJDialog(owner, processor.getMessage());
        return processor.getScore();
    }

    /**
     * Returns a Score read from a MIDI or JM file, displaying errors in a
     * {@link javax.swing.JDialog}.
     *
     * <P>The path of the file is separated into <CODE>directory</CODE> and
     * <CODE>filename</CODE> so that the latter can be used as the title of the
     * score.  If <CODE>directory</CODE> is null then this method attempts
     * to read the file specified by <CODE>filename</CODE>.
     *
     * @param directory String describing the directory structure of the file to
     *                  be read, which must include the terminating separator
     * @param filename  String describing the file name
     * @param owner     Component whose control is to be suspended while the
     *                  error messages are displayed
     * @return          Score read from file, or null if an error occured
     *
     * @see #midiOrJmWithSwingMessaging(File, Component)
     */
    public static Score midiOrJmWithSwingMessaging(final String directory,
                                                   final String filename,
                                                   final Component owner) {
        JmMidiProcessor processor = new JmMidiProcessor(directory, filename);
        displayErrorJDialog(owner, processor.getMessage());
        return processor.getScore();
    }                        

    /**
     * Displays an error message in a {@link javax.swing.JDialog}.  The message displayed is
     * retrieved from the class variable {@link #message}.  This method is
     * designed for use by the {@link #midiOrWithSwingMessaging()} methods.
     *
     * @param owner     Component whose control is to be suspended while the
     *                  error messages are displayed
     * @param message   String to display   
     *
     * @see #midiOrJmWithSwingMessaging(File, Component);
     * @see #midiOrJmWithSwingMessaging(String, String, Component);
     */
    private static void displayErrorJDialog(final Component owner,
                                            final String message) {
        if (message == null) {
            return;
        }
        JOptionPane.showMessageDialog(owner, message,
                                      "Not a valid MIDI or jMusic File",
                                      JOptionPane.ERROR_MESSAGE);
    }
}
                             
