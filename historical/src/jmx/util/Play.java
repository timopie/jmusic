/*

< This Java Class is part of the jMusic API Version 2000.11>

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/ 

package jmx.util;

import jmx.midi.MidiSynth;
import jm.music.data.*;
import jm.JMC;


public class Play implements JMC{
    
    public Play() {}
   
    //----------------------------------------------
    // MidiSynth - JavaSound MIDI playback
    //----------------------------------------------
    /**
    * Playback the jMusic score JavaSound MIDI
    * @param Score
    */ 
    public static void midi(Score s) {
        MidiSynth ms = new MidiSynth();
        try {
            System.out.println("-------------------- Playing MIDI file ----------------------");
            System.out.println("Score Title: " + s.getTitle());
            System.out.println("Score Tempo: " + s.getTempo() + " BPM");
            ms.play(s);
            System.out.println("--------------- Completed JavaSound Playback ----------------");
        }
        catch (Exception e) {
            System.err.println("MIDI Playback Error:" + e);
            return;
        }
    }
}
