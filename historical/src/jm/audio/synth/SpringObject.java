
package jm.audio.synth;

import java.awt.*;

/*
* The spring class for a spring mass network
* @author Andrew Brown
*/

public class SpringObject {
    /** where is the normal resting place? */
    private int restingLength;
    /** strength of the spring */
    private double k = 0.002;
    /** start pixel location of spring */
    
    public SpringObject() { }
    
    public SpringObject(double constant) {
    	this.k = constant;
    }

    public double getCurrentForce(double currStartPosition, double currEndPosition) {
        double currExtension = -1 * ((currEndPosition - currStartPosition) - restingLength);
        double force = k * currExtension;

        return force;
    }
    
    public void setRestingLength(int length) {
    	this.restingLength = length;
    }
}
