/*

<This Java Class is part of the jMusic API version 1.0,Sun Feb 25 18:34:01  2001

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
package jm;

/**
 * JMConstants holds constant values used across the whole
 * jMusic system.
 * @see jm.music.data.Note
 * @author Andrew Sorensen with lots of help from Andrew Brown and Andrew Troedson
 * @version 1.0,Sun Feb 25 18:42:40  2001
 */
public interface JMC{
	//----------------------------------------------
	// Programming Constants
	//----------------------------------------------
	/** A constant used to toggle debugging information */
	public static final boolean DEBUG = false;
	/** A constant used to toggle the verbosity of output */
	public static final boolean VERBOSE = true;

	/** Constant for 8 bit */
	public static final int EIGHT_BIT = 127;
	/** Constant for 16 bit */
	public static final int SIXTEEN_BIT = 32767;
	/** Constant for 32 bit */
	public static final int THIRTY_TWO_BIT = 214748647;
	/** Constant for program changes */
	public static final int PROG_EVT = Integer.MIN_VALUE + 1;
	/** Constant for tempo changes */
	public static final int TEMP_EVT = PROG_EVT + 1;
	/** Constant for key signature events */
	public static final int KEY_SIG_EVT = TEMP_EVT +1;
	/** Constant for time signature events */
	public static final int TIME_SIG_EVT = KEY_SIG_EVT + 1;
	/** Constant for no key signature */
	public static final int NO_KEY_SIGNATURE = Integer.MIN_VALUE;
	/** Constant for no key quality */
	public static final int NO_KEY_QUALITY = Integer.MIN_VALUE;
	/** Constant for no numerator */
	public static final int NO_NUMERATOR = Integer.MIN_VALUE;
	/** Constant for no denominator */
	public static final int NO_DENOMINATOR = Integer.MIN_VALUE;
	/** Constant for no instrument */
	public static final int NO_INSTRUMENT = 250;
	


	//----------------------------------------------
	// Note Durations
	//----------------------------------------------
	public static final double 
		SB = 4.0, SEMIBREVE = 4.0, WHOLE_NOTE = 4.0, WN = 4.0,
		MD = 3.0, DM = 3.0, DOTTED_MINIM = 3.0, DOTTED_HALF_NOTE = 3.0, DHN = 3.0,
		MDD = 3.5, DDM = 3.5, DOUBLE_DOTTED_MINIM = 3.5, DOUBLE_DOTTED_HALF_NOTE = 3.5, DDHN = 3.5,
		M = 2.0, MINIM = 2.0, HALF_NOTE = 2.0, HN = 2.0,
		MT = 4.0/3.0, MINIM_TRIPLET = 4.0/3.0, HALF_NOTE_TRIPLET = 4.0/3.0, HNT = 4.0/3.0,
		C = 1.0, CROTCHET = 1.0, QUARTER_NOTE = 1.0, QN = 1.0,
		CT = 2.0/3.0, CROTCHET_TRIPLET = 2.0/3.0, QUARTER_NOTE_TRIPLET = 2.0/3.0, QNT = 2.0/3.0,
		CD = 1.5, DC = 1.5, DOTTED_CROTCHET = 1.5,  DOTTED_QUARTER_NOTE = 1.5, DQN = 1.5,
		CDD = 1.75, DDC = 1.75, DOUBLE_DOTTED_CROTCHET = 1.75,  DOUBLE_DOTTED_QUARTER_NOTE = 1.75, DDQN = 1.75,
		Q = 0.5, QUAVER = 0.5, EIGHTH_NOTE = 0.5, EN = 0.5,
		QD = 0.75, DQ = 0.75, DOTTED_QUAVER = 0.75, DOTTED_EIGHTH_NOTE = 0.75, DEN = 0.75,
		QT = 1.0/3.0, QUAVER_TRIPLET = 1.0/3.0, EIGHTH_NOTE_TRIPLET = 1.0/3.0, ENT = 1.0/3.0,
	    DDQ = 0.875, QDD = 0.875, DOUBLE_DOTTED_QUAVER = 0.875, DOUBLE_DOTTED_EIGHTH_NOTE = 0.875, DDEN = 0.875,
		SQ = 0.25, SEMI_QUAVER = 0.25, SIXTEENTH_NOTE = 0.25, SN = 0.25,
		SQD = 0.375, DOTTED_SEMI_QUAVER = 0.375, DOTTED_SIXTEENTH_NOTE = 0.375, DSN = 0.375,
		SQT = 1.0/6.0, SEMI_QUAVER_TRIPLET = 1.0/6.0, SIXTEENTH_NOTE_TRIPLET = 1.0/6.0, SNT = 1.0/6.0,
		DSQ = 0.125, DEMI_SEMI_QUAVER = 0.125, THIRTYSECOND_NOTE = 0.125, TN = 0.125, TSN = 0.125,
		DSQT = 1.0/12.0, DEMI_SEMI_QUAVER_TRIPLET = 1.0/12.0, THIRTYSECOND_NOTE_TRIPLET = 1.0/12.0, TNT = 1.0/12.0, TSNT = 1.0/12.0;

	//----------------------------------------------
	// Pitch Constants
	//----------------------------------------------
	public static final int

		G9 = 127,
		GF9 = 126,
		FS9 = 126,
		F9 = 125,
		FF9 = 124,
		ES9 = 125,
		E9 = 124,
		EF9 = 123,
		DS9 = 123,
		D9 = 122,
		DF9 = 121,
		CS9 = 121,
		C9 = 120,
		CF9 = 119,
		BS8 = 120,
		B8 = 119,
		BF8 = 118,
		AS8 = 118,
		A8 = 117,
		AF8 = 116,
		GS8 = 116,
		G8 = 115,
		GF8 = 114,
		FS8 = 114,
		F8 = 113,
		FF8 = 112,
		ES8 = 113,
		E8 = 112,
		EF8 = 111,
		DS8 = 111,
		D8 = 110,
		DF8 = 109,
		CS8 = 109,
		C8 = 108,
		CF8 = 107,
		BS7 = 108,
		B7 = 107,
		BF7 = 106,
		AS7 = 106,
		A7 = 105,
		AF7 = 104,
		GS7 = 104,
		G7 = 103,
		GF7 = 102,
		FS7 = 102,
		F7 = 101,
		FF7 = 100,
		ES7 = 101,
		E7 = 100,
		EF7 = 99,
		DS7 = 99,
		D7 = 98,
		DF7 = 97,
		CS7 = 97,
		C7 = 96,
		CF7 = 95,
		BS6 = 96,
		B6 = 95,
		BF6 = 94,
		AS6 = 94,
		A6 = 93,
		AF6 = 92,
		GS6 = 92,
		G6 = 91,
		GF6 = 90,
		FS6 = 90,
		F6 = 89,
		FF6 = 88,
		ES6 = 89,
		E6 = 88,
		EF6 = 87,
		DS6 = 87,
		D6 = 86,
		DF6 = 85,
		CS6 = 85,
		C6 = 84,
		CF6 = 83,
		BS5 = 84,
		B5 = 83,
		BF5 = 82,
		AS5 = 82,
		A5 = 81,
		AF5 = 80,
		GS5 = 80,
		G5 = 79,
		GF5 = 78,
		FS5 = 78,
		F5 = 77,
		FF5 = 76,
		ES5 = 77,
		E5 = 76,
		EF5 = 75,
		DS5 = 75,
		D5 = 74,
		DF5 = 73,
		CS5 = 73,
		C5 = 72,
		CF5 = 71,
		BS4 = 72,
		B4 = 71,
		BF4 = 70,
		AS4 = 70,
		A4 = 69,
		AF4 = 68,
		GS4 = 68,
		G4 = 67,
		GF4 = 66,
		FS4 = 66,
		F4 = 65,
		FF4 = 64,
		ES4 = 65,
		E4 = 64,
		EF4 = 63,
		DS4 = 63,
		D4 = 62,
		DF4 = 61,
		CS4 = 61,
		C4 = 60,
		CF4 = 59,
		BS3 = 60,
		B3 = 59,
		BF3 = 58,
		AS3 = 58,
		A3 = 57,
		AF3 = 56,
		GS3 = 56,
		G3 = 55,
		GF3 = 54,
		FS3 = 54,
		F3 = 53,
		FF3 = 52,
		ES3 = 53,
		E3 = 52,
		EF3 = 51,
		DS3 = 51,
		D3 = 50,
		DF3 = 49,
		CS3 = 49,
		C3 = 48,
		CF3 = 47,
		BS2 = 48,
		B2 = 47,
		BF2 = 46,
		AS2 = 46,
		A2 = 45,
		AF2 = 44,
		GS2 = 44,
		G2 = 43,
		GF2 = 42,
		FS2 = 42,
		F2 = 41,
		FF2 = 40,
		ES2 = 41,
		E2 = 40,
		EF2 = 39,
		DS2 = 39,
		D2 = 38,
		DF2 = 37,
		CS2 = 37,
		C2 = 36,
		CF2 = 35,
		BS1 = 36,
		B1 = 35,
		BF1 = 34,
		AS1 = 34,
		A1 = 33,
		AF1 = 32,
		GS1 = 32,
		G1 = 31,
		GF1 = 30,
		FS1 = 30,
		F1 = 29,
		FF1 = 28,
		ES1 = 29,
		E1 = 28,
		EF1 = 27,
		DS1 = 27,
		D1 = 26,
		DF1 = 25,
		CS1 = 25,
		C1 = 24,
		CF1 = 23,
		BS0 = 24,
		B0 = 23,
		BF0 = 22,
		AS0 = 22,
		A0 = 21,
		AF0 = 20,
		GS0 = 20,
		G0 = 19,
		GF0 = 18,
		FS0 = 18,
		F0 = 17,
		FF0 = 16,
		ES0 = 17,
		E0 = 16,
		EF0 = 15,
		DS0 = 15,
		D0 = 14,
		DF0 = 13,
		CS0 = 13,
		C0 = 12,
		CF0 = 11,
		BSN1 = 12,
		BN1 = 11,
		BFN1 = 10,
		ASN1 = 10,
		AN1 = 9,
		AFN1 = 8,
		GSN1 = 8,
		GN1 = 7,
		GFN1 = 6,
		FSN1 = 6,
		FN1 = 5,
		FFN1 = 4,
		ESN1 = 5,
		EN1 = 4,
		EFN1 = 3,
		DSN1 = 3,
		DN1 = 2,
		DFN1 = 1,
		CSN1 = 1,
		CN1 = 0,
		g9 = 127,
		gf9 = 126,
		fs9 = 126,
		f9 = 125,
		ff9 = 124,
		es9 = 125,
		e9 = 124,
		ef9 = 123,
		ds9 = 123,
		d9 = 122,
		df9 = 121,
		cs9 = 121,
		c9 = 120,
		cf9 = 119,
		bs8 = 120,
		b8 = 119,
		bf8 = 118,
		as8 = 118,
		a8 = 117,
		af8 = 116,
		gs8 = 116,
		g8 = 115,
		gf8 = 114,
		fs8 = 114,
		f8 = 113,
		ff8 = 112,
		es8 = 113,
		e8 = 112,
		ef8 = 111,
		ds8 = 111,
		d8 = 110,
		df8 = 109,
		cs8 = 109,
		c8 = 108,
		cf8 = 107,
		bs7 = 108,
		b7 = 107,
		bf7 = 106,
		as7 = 106,
		a7 = 105,
		af7 = 104,
		gs7 = 104,
		g7 = 103,
		gf7 = 102,
		fs7 = 102,
		f7 = 101,
		ff7 = 100,
		es7 = 101,
		e7 = 100,
		ef7 = 99,
		ds7 = 99,
		d7 = 98,
		df7 = 97,
		cs7 = 97,
		c7 = 96,
		cf7 = 95,
		bs6 = 96,
		b6 = 95,
		bf6 = 94,
		as6 = 94,
		a6 = 93,
		af6 = 92,
		gs6 = 92,
		g6 = 91,
		gf6 = 90,
		fs6 = 90,
		f6 = 89,
		ff6 = 88,
		es6 = 89,
		e6 = 88,
		ef6 = 87,
		ds6 = 87,
		d6 = 86,
		df6 = 85,
		cs6 = 85,
		c6 = 84,
		cf6 = 83,
		bs5 = 84,
		b5 = 83,
		bf5 = 82,
		as5 = 82,
		a5 = 81,
		af5 = 80,
		gs5 = 80,
		g5 = 79,
		gf5 = 78,
		fs5 = 78,
		f5 = 77,
		ff5 = 76,
		es5 = 77,
		e5 = 76,
		ef5 = 75,
		ds5 = 75,
		d5 = 74,
		df5 = 73,
		cs5 = 73,
		c5 = 72,
		cf5 = 71,
		bs4 = 72,
		b4 = 71,
		bf4 = 70,
		as4 = 70,
		a4 = 69,
		af4 = 68,
		gs4 = 68,
		g4 = 67,
		gf4 = 66,
		fs4 = 66,
		f4 = 65,
		ff4 = 64,
		es4 = 65,
		e4 = 64,
		ef4 = 63,
		ds4 = 63,
		d4 = 62,
		df4 = 61,
		cs4 = 61,
		c4 = 60,
		cf4 = 59,
		bs3 = 60,
		b3 = 59,
		bf3 = 58,
		as3 = 58,
		a3 = 57,
		af3 = 56,
		gs3 = 56,
		g3 = 55,
		gf3 = 54,
		fs3 = 54,
		f3 = 53,
		ff3 = 52,
		es3 = 53,
		e3 = 52,
		ef3 = 51,
		ds3 = 51,
		d3 = 50,
		df3 = 49,
		cs3 = 49,
		c3 = 48,
		cf3 = 47,
		bs2 = 48,
		b2 = 47,
		bf2 = 46,
		as2 = 46,
		a2 = 45,
		af2 = 44,
		gs2 = 44,
		g2 = 43,
		gf2 = 42,
		fs2 = 42,
		f2 = 41,
		ff2 = 40,
		es2 = 41,
		e2 = 40,
		ef2 = 39,
		ds2 = 39,
		d2 = 38,
		df2 = 37,
		cs2 = 37,
		c2 = 36,
		cf2 = 35,
		bs1 = 36,
		b1 = 35,
		bf1 = 34,
		as1 = 34,
		a1 = 33,
		af1 = 32,
		gs1 = 32,
		g1 = 31,
		gf1 = 30,
		fs1 = 30,
		f1 = 29,
		ff1 = 28,
		es1 = 29,
		e1 = 28,
		ef1 = 27,
		ds1 = 27,
		d1 = 26,
		df1 = 25,
		cs1 = 25,
		c1 = 24,
		cf1 = 23,
		bs0 = 24,
		b0 = 23,
		bf0 = 22,
		as0 = 22,
		a0 = 21,
		af0 = 20,
		gs0 = 20,
		g0 = 19,
		gf0 = 18,
		fs0 = 18,
		f0 = 17,
		ff0 = 16,
		es0 = 17,
		e0 = 16,
		ef0 = 15,
		ds0 = 15,
		d0 = 14,
		df0 = 13,
		cs0 = 13,
		c0 = 12,
		cf0 = 11,
		bsn1 = 12,
		bn1 = 11,
		bfn1 = 10,
		asn1 = 10,
		an1 = 9,
		afn1 = 8,
		gsn1 = 8,
		gn1 = 7,
		gfn1 = 6,
		fsn1 = 6,
		fn1 = 5,
		ffn1 = 4,
		esn1 = 5,
		en1 = 4,
		efn1 = 3,
		dsn1 = 3,
		dn1 = 2,
		dfn1 = 1,
		csn1 = 1,
		cn1 = 0,
		REST = (int)jm.music.data.Note.REST;  //No Note

	//----------------------------------------------
	// Frequency Constants
	//----------------------------------------------
	// See also: jm.music.tools.Convert.MIDItoFreq()

	public static final double[] FRQ = {
				  /* (0) CN1 */ 8.1757989156,
				  /* (1) CSN1*/ 8.6619572180,
				  /* (2) DN1*/ 9.1770239974,
				  /* (3) DSN1*/ 9.7227182413,
				  /* (4) EN1*/ 10.3008611535,
				  /* (5) FN1*/ 10.9133822323,
				  /* (6) FSN1*/ 11.5623257097,
				  /* (7) GN1*/ 12.2498573744,
				  /* (8) GSN1*/ 12.9782717994,
				  /* (9) AN1*/ 13.7500000000,
				  /* (10) ASN1*/ 14.5676175474,
				  /* (11) BN1*/ 15.4338531643,
				  /* (12) C0*/ 16.3515978313,
				  /* (13) CS0*/ 17.3239144361,
				  /* (14) D0*/ 18.3540479948,
				  /* (15) DS0*/ 19.4454364826,
				  /* (16) E0*/ 20.6017223071,
				  /* (17) F0*/ 21.8267644646,
				  /* (18) FS0*/ 23.1246514195,
				  /* (19) G0*/ 24.4997147489,
				  /* (20) GS0*/ 25.9565435987,
				  /* (21) A0 */ 27.5000000000,
				  /* (22) AS0 */ 29.1352350949,
				  /* (23) B0 */ 30.8677063285,
				  /* (24) C1 */ 32.7031956626,
				  /* (25) CS1 */ 34.6478288721,
				  /* (26) D1 */ 36.7080959897,
				  /* (27) DS1 */ 38.8908729653,
				  /* (28) E1 */ 41.2034446141,
				  /* (29) F1 */ 43.6535289291,
				  /* (30) FS1 */ 46.2493028390,
				  /* (31) G1 */ 48.9994294977,
				  /* (32) GS1 */ 51.9130871975,
				  /* (33) A1 */ 55.0000000000,
				  /* (34) AS1 */ 58.2704701898,
				  /* (35) B1 */ 61.7354126570,
				  /* (36) C2 */ 65.4063913251,
				  /* (37) CS2 */ 69.2956577442,
				  /* (38) D2 */ 73.4161919794,
				  /* (39) DS2 */ 77.7817459305,
				  /* (40) E2 */ 82.4068892282,
				  /* (41) F2 */ 87.3070578583,
				  /* (42) FS2 */ 92.4986056779,
				  /* (43) G2 */ 97.9988589954,
				  /* (44) GS2 */ 103.8261743950,
				  /* (45) A2 */ 110.0000000000,
				  /* (46) AS2 */ 116.5409403795,
				  /* (47) B2 */ 123.4708253140,
				  /* (48) C3 */ 130.8127826503,
				  /* (49) CS3 */ 138.5913154884,
				  /* (50) D3 */ 146.8323839587,
				  /* (51) DS3 */ 155.5634918610,
				  /* (52) E3 */ 164.8137784564,
				  /* (53) F3 */ 174.6141157165,
				  /* (54) FS3 */ 184.9972113558,
				  /* (55) G3 */ 195.9977179909,
				  /* (56) GS3 */ 207.6523487900,
				  /* (57) A3 */ 220.0000000000,
				  /* (58) AS3 */ 233.0818807590,
				  /* (59) B3 */ 246.9416506281,
				  /* (60) C4 */ 261.6255653006,
				  /* (61) CS4 */ 277.1826309769,
				  /* (62) D4 */ 293.6647679174,
				  /* (63) DS4 */ 311.1269837221,
				  /* (64) E4 */ 329.6275569129,
				  /* (65) F4 */ 349.2282314330,
				  /* (66) FS4 */ 369.9944227116,
				  /* (67) G4 */ 391.9954359817,
				  /* (68) GS4 */ 415.3046975799,
				  /* (69) A4 */ 440.0000000000,
				  /* (70) AS4 */ 466.1637615181,
				  /* (71) B4 */ 493.8833012561,
				  /* (72) C5 */ 523.2511306012,
				  /* (73) CS5 */ 554.3652619537,
				  /* (74) D5 */ 587.3295358348,
				  /* (75) DS5 */ 622.2539674442,
				  /* (76) E5 */ 659.2551138257,
				  /* (77) F5 */ 698.4564628660,
				  /* (78) FS5 */ 739.9888454233,
				  /* (79) G5 */ 783.9908719635,
				  /* (80) GS5 */ 830.6093951599,
				  /* (81) A5 */ 880.0000000000,
				  /* (82) AS5 */ 932.3275230362,
				  /* (83) B5 */ 987.7666025122,
				  /* (84) C6 */ 1046.5022612024,
				  /* (85) CS6 */ 1108.7305239075,
				  /* (86) D6 */ 1174.6590716696,
				  /* (87) DS6 */ 1244.5079348883,
				  /* (88) E6 */ 1318.5102276515,
				  /* (89) F6 */ 1396.9129257320,
				  /* (90) FS6 */ 1479.9776908465,
				  /* (91) G6 */ 1567.9817439270,
				  /* (92) GS6 */ 1661.2187903198,
				  /* (93) A6 */ 1760.0000000000,
				  /* (94) AS6 */ 1864.6550460724,
				  /* (95) B6 */ 1975.5332050245,
				  /* (96) C7 */ 2093.0045224048,
				  /* (97) CS7 */ 2217.4610478150,
				  /* (98) D7 */ 2349.3181433393,
				  /* (99) DS7 */ 2489.0158697766,
				  /* (100) E7 */ 2637.0204553030,
				  /* (101) F7 */ 2793.8258514640,
				  /* (102) FS7 */ 2959.9553816931,
				  /* (103) G7 */ 3135.9634878540,
				  /* (104) GS7 */ 3322.4375806396,
				  /* (105) A7 */ 3520.0000000000,
				  /* (106) AS7 */ 3729.3100921447,
				  /* (107) B7 */ 3951.0664100490,
				  /* (108) C8 */ 4186.0090448096, 
				  /* (109) CS8 */ 4434.9220956300,
				  /* (110) D8 */ 4698.6362866785,
				  /* (111) DS8 */ 4978.0317395533,
				  /* (112) E8 */ 5274.0409106059,
				  /* (113) F8 */ 5587.6517029281,
				  /* (114) FS8 */ 5919.9107633862,
				  /* (115) G8 */ 6271.9269757080,
				  /* (116) GS8 */ 6644.8751612791,
				  /* (117 A8 */ 7040.0000000000,
				  /* (118 AS8 */ 7458.6202347565,
				  /* (119 B8 */ 7902.1328346582,
				  /* (120 C9 */ 8372.0180896192,
				  /* (121) CS9 */ 8869.8441912599,
				  /* (122) D9 */ 9397.2725733570,
				  /* (123) DS9 */ 9956.0634791066,
				  /* (124) E9 */ 10548.0818212118,
				  /* (125) F9 */ 11175.3034058561,
				  /* (126) FS9 */ 11839.8215267723,
				  /* (127) G9 */ 12543.8539514160
	};
	

	//----------------------------------------------
	// Volume constants
	//----------------------------------------------
	public static final int
		SILENT = 0,
		PPP = 10,
		PP = 25,
		P = 50,
		MP = 60,
		MF = 70,
		F = 85,
		FF = 100,
		FFF = 120;
	
	//----------------------------------------------
	// Pan constants
	//----------------------------------------------
	public static final double
		PAN_CENTER = 0.5,
		PAN_CENTRE = 0.5,
		PAN_LEFT = 0.0,
		PAN_RIGHT = 1.0;
		
	//----------------------------------------------
	// Duration Articulation constants
	// Multiply the rhythmValue by these to get the effect
	//----------------------------------------------
	public static final double
		STACCATO = 0.2,
		LEGATO = 0.95,
		SOSTENUTO = 1.2,
		TENUTO = 1.0;

	//----------------------------------------------
	// GM Program Changes constants
	//----------------------------------------------
	public static final int
		PIANO = 0,
		HONKYTONK = 3, HONKYTONK_PIANO = 3,
                EPIANO = 4, ELECTRIC_PIANO = 4, ELPIANO = 4, RHODES = 4,
                EPIANO2 = 5, DX_EPIANO = 5,
		HARPSICHORD = 6,
		CLAV = 7, CLAVINET = 7,
		CELESTE = 8, CELESTA = 8,
		GLOCKENSPIEL = 9, GLOCK = 9,
		MUSIC_BOX = 10,
		VIBRAPHONE = 11, VIBES = 11,
		MARIMBA = 12,
		XYLOPHONE = 13,
		TUBULAR_BELL = 14, TUBULAR_BELLS = 14,
		ORGAN = 16, ELECTRIC_ORGAN = 16,
                ORGAN2 = 17, JAZZ_ORGAN = 17,
                ORGAN3 = 18, HAMMOND_ORGAN = 17,
		CHURCH_ORGAN = 19, PIPE_ORGAN = 19,
		REED_ORGAN = 20,
		ACCORDION = 21, PIANO_ACCORDION = 21, CONCERTINA = 21,
		HARMONICA = 22,
		BANDNEON = 23,
		NYLON_GUITAR = 24, NGUITAR = 24, GUITAR = 24, ACOUSTIC_GUITAR = 24, AC_GUITAR = 24,
		STEEL_GUITAR = 25, SGUITAR = 25,
		JAZZ_GUITAR = 26, JGUITAR = 26,
		CLEAN_GUITAR = 27, CGUITAR = 27, ELECTRIC_GUITAR = 27, EL_GUITAR = 27,
		MUTED_GUITAR = 28, MGUITAR = 28,
		OVERDRIVE_GUITAR = 29, OGUITAR = 29,
		DISTORTED_GUITAR = 30, DGUITAR = 30, DIST_GUITAR = 30,
		GUITAR_HARMONICS = 31, GT_HARMONICS = 31, HARMONICS = 31,
		ACOUSTIC_BASS = 32, ABASS = 32, 
		FINGERED_BASS = 33, BASS = 33, FBASS = 33, ELECTRIC_BASS = 33, EL_BASS = 33, EBASS = 33,
		PICKED_BASS = 34, PBASS = 34,
		FRETLESS_BASS = 35, FRETLESS = 35,
		SLAP_BASS = 36, SBASS = 36, SLAP = 36,
		SYNTH_BASS = 38,
		VIOLIN = 40,
		VIOLA = 41,
		CELLO = 42, VIOLIN_CELLO = 42,
		CONTRABASS = 43, CONTRA_BASS = 43, DOUBLE_BASS = 43,
		TREMOLO_STRINGS = 44, TREMOLO = 44,
		PIZZICATO_STRINGS = 45, PIZZ = 45, PITZ = 45, PSTRINGS = 45, 
		HARP = 46,
		TIMPANI = 47, TIMP = 47,
		STRINGS = 48, STR = 48,
		SLOW_STRINGS = 51,
		SYNTH_STRINGS = 50, SYN_STRINGS = 50,
		AAH = 52, AHHS = 52, CHOIR = 52, 
		OOH = 53, OOHS = 53, VOICE = 53,
		SYNVOX = 54, VOX = 54, 
		ORCHESTRA_HIT = 55,
		TRUMPET = 56,
		TROMBONE = 57,
		TUBA = 58,
		MUTED_TRUMPET = 59,
		FRENCH_HORN = 60, HORN = 60,
		BRASS = 61,
		SYNTH_BRASS = 62,
		SOPRANO_SAX = 64, SOPRANO = 64, SOPRANO_SAXOPHONE = 64, SOP = 64,
		ALTO_SAX = 65, ALTO = 65, ALTO_SAXOPHONE = 65,
		TENOR_SAX = 66, TENOR = 66, TENOR_SAXOPHONE = 66, SAX = 66, SAXOPHONE = 66,
		BARITONE_SAX = 67, BARI = 67, BARI_SAX = 67, BARITONE = 67, BARITONE_SAXOPHONE = 67, 
		OBOE = 68,
		ENGLISH_HORN = 69,
		BASSOON = 70,
		CLARINET = 71, CLAR = 71,
		PICCOLO = 72, PIC = 72, PICC = 72,
		FLUTE = 73,
		RECORDER = 74,
		PAN_FLUTE = 75, PANFLUTE = 75,
		BOTTLE_BLOW = 76, BOTTLE = 76,
		SHAKUHACHI = 77, 
		WHISTLE = 78,
		OCARINA = 79,
		GMSQUARE_WAVE = 80, SQUARE = 80,
		GMSAW_WAVE = 81, SAW = 81, SAWTOOTH = 81,
		SYNTH_CALLIOPE = 82, CALLOPE = 82, SYN_CALLIOPE = 82,
		CHIFFER_LEAD = 83, CHIFFER = 83,
		CHARANG = 84,
		SOLO_VOX = 85,
		FANTASIA = 88,
		WARM_PAD = 89, PAD = 89,
		POLYSYNTH = 90, POLY_SYNTH = 90,
		SPACE_VOICE = 91,
		BOWED_GLASS = 92,
		METAL_PAD = 93,
		HALO_PAD = 94, HALO = 94,
		SWEEP_PAD = 95, SWEEP = 95,
		ICE_RAIN = 96, ICERAIN = 96,
		SOUNDTRACK = 97,
		CRYSTAL = 98,
		ATMOSPHERE = 99,
		BRIGHTNESS = 100,
		GOBLIN = 101,
		ECHO_DROPS = 102, DROPS = 102, ECHOS = 102, ECHO = 102, ECHO_DROP = 102,
		STAR_THEME = 103,
		SITAR = 104,
		BANJO = 105,
		SHAMISEN = 106,
		KOTO = 107,
		KALIMBA = 108, THUMB_PIANO = 108,
		BAGPIPES = 109, BAG_PIPES = 109, BAGPIPE = 109, PIPES = 109,
		FIDDLE = 110,
		SHANNAI = 111,
		TINKLE_BELL = 112, BELL = 112, BELLS = 112,
		AGOGO = 113,
		STEEL_DRUMS = 114, STEELDRUMS = 114, STEELDRUM = 114, STEEL_DRUM = 114,
		WOODBLOCK = 115, WOODBLOCKS = 115,
		TAIKO = 116, DRUM = 116,
		TOM = 119, TOMS = 119, TOM_TOM = 119, TOM_TOMS = 119,
		SYNTH_DRUM = 118, SYNTH_DRUMS = 118,
		REVERSE_CYMBAL = 119, CYMBAL = 119,
		FRETNOISE = 120, FRET_NOISE = 120, FRET = 120, FRETS = 120,
		BREATHNOISE = 121, BREATH = 121,
		SEASHORE = 122, SEA = 122, RAIN = 122, THUNDER = 122, WIND = 122, STREAM = 122, SFX = 122, SOUNDEFFECTS = 122, SOUNDFX = 122,
		BIRD = 123,
		TELEPHONE = 124, PHONE = 124,
		HELICOPTER = 125,
		APPLAUSE = 126;
		
	//----------------------------------------------
	// Scales on a root of C
	//----------------------------------------------
    public static final int[] CHROMATIC_SCALE = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	public static final int[] MAJOR_SCALE = {0, 2, 4, 5, 7, 9, 11};
	public static final int[] MINOR_SCALE = {0, 2, 3, 5, 7, 8, 11};
	public static final int[] HARMONIC_MINOR_SCALE = {0, 2, 3, 5, 7, 8, 11};
	public static final int[] MELODIC_MINOR_SCALE = {0, 2, 3, 5, 7, 8, 9, 10, 11}; // mix of ascend and descend
	public static final int[] NATURAL_MINOR_SCALE = {0, 2, 3, 5, 7, 8, 10};
	public static final int[] DIATONIC_MINOR_SCALE = {0, 2, 3, 5, 7, 8, 10};
	public static final int[] AEOLIAN_SCALE = {0, 2, 3, 5, 7, 8, 10};
	public static final int[] DORIAN_SCALE = {0, 2, 3, 5, 7, 9, 10};	
	public static final int[] LYDIAN_SCALE = {0, 2, 4, 6, 7, 9, 11};
	public static final int[] MIXOLYDIAN_SCALE = {0, 2, 4, 5, 7, 9, 10};
	public static final int[] PENTATONIC_SCALE = {0, 2, 4, 7, 9};
	public static final int[] BLUES_SCALE = {0, 2, 3, 4, 5, 7, 9, 10, 11};
	public static final int[] TURKISH_SCALE = {0, 1, 3, 5, 7, 10, 11};
	public static final int[] INDIAN_SCALE = {0, 1, 1, 4, 5, 8, 10};
	
	//----------------------------------------------
	// Audio constants
	//----------------------------------------------
	public static final int SINE_WAVE = 0;
        public static final int COSINE_WAVE = 1;
        public static final int TRIANGLE_WAVE = 2;
        public static final int SQUARE_WAVE = 3;
        public static final int SAWTOOTH_WAVE = 4;
        public static final int SAWDOWN_WAVE = 5;
        public static final int SABERSAW_WAVE = 6;
        public static final int PULSE_WAVE = 7;   
      // noise
      public static final int WHITE_NOISE = 0;
	public static final int STEP_NOISE = 1;
	public static final int SMOOTH_NOISE = 2;
	public static final int BROWN_NOISE = 3;
       public static final int FRACTAL_NOISE = 4; 
        /* modulation sources */
        public static final int AMPLITUDE = 0;
        public static final int FREQUENCY = 1;
        /* channels */
        public static final int MONO = 1;
        public static final int STEREO = 2;
        public static final int QUADRAPHONIC = 4;
        public static final int OCTAPHONIC = 8;
    
    //----------------------------------------------
	// Data type constants
	//----------------------------------------------
	public static final int PITCH = 0;
    public static final int RHYTHM = 1;
    public static final int DYNAMIC = 2;
    public static final int PAN = 3;

}
