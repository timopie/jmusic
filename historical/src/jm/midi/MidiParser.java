/*

<This Java Class is part of the jMusic API version 1.0,Sun Feb 25 18:34:44  2001

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
package jm.midi;

import java.io.*;
import java.util.Enumeration;
import java.util.Vector;

import jm.JMC;
import jm.midi.*;
import jm.midi.event.*;
import jm.music.data.*;

/**
 * A midi parser 
 * @author Andrew Sorensen
 */

public final class MidiParser implements JMC{

        //-----------------------------------------------------------
        //Converts a SMF into jMusic Score data
        //-----------------------------------------------------------
        /**
         * Convert a SMF into the jMusic data type
         */
        public static void SMFToScore(Score score, SMF smf){
                System.out.println("Convert SMF to JM");
                Enumeration enum = smf.getTrackList().elements();
                //Go through tracks
                while(enum.hasMoreElements()){
                        Part part = new Part();
                        Track smfTrack = (Track) enum.nextElement();
                        Vector evtList = smfTrack.getEvtList();
                        Vector phrVct = new Vector();
			sortEvents(evtList,phrVct,smf,part);
                        for(int i=0;i<phrVct.size();i++){
                                part.addPhrase((Phrase)phrVct.elementAt(i));
			}
                        score.addPart(part);
                	score.clean();
                }
        }


	private static void sortEvents(Vector evtList, Vector phrVct, SMF smf, 
					Part part){
        	double startTime = 0.0;
                double[] currentLength = new double[100];
                Note[] curNote = new Note[100];
                int numOfPhrases = 0;
                double oldTime = 0.0;
                int phrIndex = 0;
                //Go through evts
                for(int i=0;i<evtList.size();i++){
               		Event evt = (Event) evtList.elementAt(i);
                        startTime+=(double)evt.getTime()/(double)smf.getPPQN();
                        if(evt.getID() == 007){
                        	PChange pchg = (PChange)evt;
                                part.setInstrument(pchg.getValue());
                        //if this event is a NoteOn event go on
                        }else if(evt.getID() == 005){
                                NoteOn noteOn = (NoteOn) evt;
                                part.setChannel(noteOn.getMidiChannel());
                                short pitch = noteOn.getPitch();
                                int dynamic = noteOn.getVelocity();
                                short midiChannel = noteOn.getMidiChannel();
                        	//if you're a true NoteOn
                        	if(dynamic > 0){
					noteOn(phrIndex,curNote,smf,i,
						currentLength, startTime, 
						phrVct,midiChannel,
						pitch,dynamic,evtList);
				}
			}
             	}
	}

	private static void noteOn(int phrIndex, Note[] curNote,SMF smf,int i, 
		double[] currentLength, double startTime, Vector phrVct,
		short midiChannel, short pitch, int dynamic, Vector evtList){

        	phrIndex = -1;
                //work out what phrase is ready to accept a note
                for(int p=0;p<phrVct.size();p++){
                	//Warning 0.02 should really be fixed
                        if(currentLength[p]<=(startTime+0.08)){
                       	 	phrIndex = p;
                                break;
                        }
               	} 
                //need to create new phrase for a new voice?
                if(phrIndex == -1){
                	phrIndex = phrVct.size();
                        phrVct.addElement(new Phrase(startTime));
                        currentLength[phrIndex] = startTime;
                }
                //Do we need to add a rest ?
                if((startTime > currentLength[phrIndex])&&
			(curNote[phrIndex] != null)){
			double newTime=startTime - currentLength[phrIndex];
                        //perform a level of quantisation first
                        if(newTime < 0.25){
                        	double length=
					curNote[phrIndex].getRhythmValue();
                                curNote[phrIndex].setRhythmValue(
							length+newTime);
                        }else{
                                Note restNote =new Note(REST, newTime, 0);
                                restNote.setPan(midiChannel);
                                restNote.setDuration(newTime);
                                restNote.setOffset(0.0);
                                ((Phrase) phrVct.elementAt(phrIndex)).
							addNote(restNote);
			}
                        currentLength[phrIndex]+= newTime;
                }
		// get end time
                double time = MidiUtil.getEndEvt(pitch, evtList, i)/
							(double)smf.getPPQN();
                // create the new note
                Note tempNote = new Note(pitch,time, dynamic);
                tempNote.setDuration(time);
                curNote[phrIndex] = tempNote;
                ((Phrase)phrVct.elementAt(phrIndex)).addNote(curNote[phrIndex]);
                currentLength[phrIndex] += curNote[phrIndex].getRhythmValue();
	}

	//------------------------------------------------------------------
	// Converts a score into a SMF
	//------------------------------------------------------------------

        /**
         * Converts jmusic score data into SMF  data
         * @param Score score - data to change to SMF
         * @exception Exception
         */
        public static void scoreToSMF(Score score, SMF smf){
                if(VERBOSE) System.out.println("Converting to SMF data structure...");
                
                double scoreTempo = score.getTempo();
                double partTempoMultiplyer = 1.0;
                int phraseNumb;
                Phrase phrase1, phrase2;
                
                //Add a tempo track at the start of top of the list
                //Add time sig to the tempo track
                Track smfT = new Track();
                smfT.addEvent(new Tempo(0, score.getTempo()));
                smfT.addEvent(new TimeSig(0, score.getNumerator(),score.getDenominator()));
                smfT.addEvent(new KeySig(0, score.getKeySignature()));
                smfT.addEvent(new EndTrack());
                smf.getTrackList().addElement(smfT);
                //---------------------------------------------------
                System.out.print("Part:");
                int partCount = 1;
                Enumeration enum = score.getPartList().elements();
                Vector timeingList = new Vector();
                while(enum.hasMoreElements()){
                    System.out.print(" "+ partCount++);
                        if(!timeingList.isEmpty()) timeingList.removeAllElements();

                        Track smfTrack = new Track();
                        Part inst = (Part) enum.nextElement();

                        //order phrases based on their startTimes
                        phraseNumb = inst.getPhraseList().size();
                        for(int i=0; i< phraseNumb; i++){
                            phrase1 = (Phrase) inst.getPhraseList().elementAt(i);
                            for(int j=0; j<phraseNumb; j++){
                                phrase2 = (Phrase)inst.getPhraseList().elementAt(j);
                                if(phrase2.getStartTime() > phrase1.getStartTime()){
                                    inst.getPhraseList().setElementAt(phrase2, i);
                                    inst.getPhraseList().setElementAt(phrase1, j);
                                    break;
                                }
                            }
                        }
                        //break Note objects into NoteStart's and NoteEnd's
                        //as well as combining all phrases into one list
                        Vector startList = new Vector();

                        
                        if(inst.getTempo() != 0){
                                //System.out.println("Adding part tempo");
                                Note nn = new Note(TEMP_EVT, (double)inst.getTempo());
                                timeingList.addElement(nn);
                                startList.addElement(new Double(0.0));
                        }
                        
                        //if this part has a Program Change value of 16 or lessadd
                        //a program change event (faked as a note of value 250)
                        if(inst.getInstrument() != NO_INSTRUMENT){
                                Note nn = new Note(PROG_EVT,(double)inst.getInstrument());
                                timeingList.addElement(nn);
                                startList.addElement(new Double(0.0));
                        }

                        if(inst.getNumerator() != NO_NUMERATOR){
                                Note nn = new Note(TIME_SIG_EVT,(double)inst.getNumerator(),inst.getDenominator());
                                timeingList.addElement(nn);
                                startList.addElement(new Double(0.0));
                        }

                        if(inst.getKeySignature() != NO_KEY_SIGNATURE){
                                Note nn = new
Note(KEY_SIG_EVT,(double)inst.getKeySignature(),inst.getKeyQuality());
                                timeingList.addElement(nn);
                                startList.addElement(new Double(0.0));
                        }

                        Enumeration e2 = inst.getPhraseList().elements();
                        double max = 0;
                        double startTime = 0.0;
                        while(e2.hasMoreElements()) {
                                Phrase phrase = (Phrase) e2.nextElement();
                                Enumeration e3 = phrase.getNoteList().elements();
                                startTime = phrase.getStartTime();


                                ////////////////////////////////////////////////
                                //this is a quick hack to allow a phrase to set
                                //a program change. The pitch of this "fake note"
                                //will be set to 250 to indicate a program change
                                //the rhythm value represents the prog change value

                                //Shouldn't this section go after the next for loop ?????
                                //As the next for loop sets a NoteOn partners NoteOn(NoteOFF)
                                if(phrase.getInstrument() != NO_INSTRUMENT){
                                        Note nn = new Note(PROG_EVT,(double)phrase.getInstrument());
                                        timeingList.addElement(nn);
                                        startList.addElement(new Double(startTime));
                                }

                                /*
                                if(phrase.getTempo() != 0){
                                        Note nn = new Note(TEMP_EVT,(double)phrase.getTempo());
                                        timeingList.addElement(nn);
                                        startList.addElement(new Double(startTime));
                                }
                                */
                                ////////////////////////////////////////////////

                                while(e3.hasMoreElements()){
                                   
                                        Note note = (Note) e3.nextElement();
                                     //check for frequency rather than MIDI notes
                                    if (note.getPitchType() == Note.FREQUENCY) {
                                        System.err.println("jMusic MIDI error: can't use frequency as a pitch for MIDI notes.");
                                        System.exit(1);
                                    }
                                        timeingList.addElement(note);
                                        startList.addElement(new Double(startTime));
                                        Note notesEnd = null;
                                        notesEnd = (Note) note.copy();
                                        notesEnd.setDynamic(0);
                                        timeingList.addElement(notesEnd);
                                        //create a timing event at the end of the notes duration
                                        double endTime = startTime + note.getDuration();
                                        // Add the note off time to the list
                                        startList.addElement(new Double(endTime));
                                        // move the note on time forward by the rhythmic value
                                        startTime += note.getRhythmValue(); //time between start times
                                }
                        }
                        //Sort lists so start times are in the right order
                        Enumeration start = startList.elements();
                        Enumeration timeing = timeingList.elements();
                        Vector sortedStarts = new Vector();
                        Vector sortedNotes = new Vector();
                        while(start.hasMoreElements()){
                            double smallest = ((Double)start.nextElement()).doubleValue();
                            Note note = (Note) timeing.nextElement();
                            int index = 0, count = 0;
                            while(start.hasMoreElements()){
                                count++;
                                double d1 = ((Double)start.nextElement()).doubleValue();
                                Note note1 = (Note) timeing.nextElement();
                                if(smallest == d1){ //if note time is equal
                                    if(note1.getDynamic() == 0) {
                                        index = count;
                                    }
                                }
                                if(smallest > d1){
                                    smallest = d1;
                                    index = count;
                                }
                            }
                            sortedStarts.addElement(startList.elementAt(index));
                            sortedNotes.addElement(timeingList.elementAt(index));
                            startList.removeElementAt(index);
                            timeingList.removeElementAt(index);
                            //reset lists for next run
                            start = startList.elements();
                            timeing = timeingList.elements();
                        }
                        //make and save NoteOn events to a smfTrack
                        double st = 0.0; //start time
                        int sortedSize = sortedNotes.size();
                        for(int index=0;index<sortedSize;index++){
                                Note note = (Note) sortedNotes.elementAt(index);
                                Event event = null;
                                switch(note.getPitch()){
                                case REST:
                                        continue;
                                case TIME_SIG_EVT:
                                        event = new TimeSig((int)note.getRhythmValue(), note.getDynamic());
                                        break;
                                case KEY_SIG_EVT:
                                        event = new KeySig((int)note.getRhythmValue(), note.getDynamic());
                                        break;
                                case TEMP_EVT:
                                        event = new Tempo((int)note.getRhythmValue());
                                        partTempoMultiplyer = scoreTempo / note.getRhythmValue();
                                        break;
                                case PROG_EVT:
                                        event = new PChange((short)note.getRhythmValue(), 
                                                        (short)inst.getChannel(),0);
                                        break;
                                default:
                                        event = new NoteOn((short)note.getPitch(), (short)note.getDynamic(), 
                                                        (short)inst.getChannel(),0);
                                }
                                double sortStart=((Double)sortedStarts.elementAt(index)).doubleValue();
                                int time = (int) ((((sortStart - st) * (double)smf.getPPQN()))* partTempoMultiplyer + 0.5);
                                st = sortStart;
                                event.setTime(time);
                                smfTrack.addEvent(event);
                        }
                        smfTrack.addEvent(new EndTrack());
                        //add this track to the SMF
                        smf.getTrackList().addElement(smfTrack);
                }
                System.out.println(".");
        }
    
}
