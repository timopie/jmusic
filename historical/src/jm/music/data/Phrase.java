/*

<This Java Class is part of the jMusic API version 1.0,Sun Feb 25 18:35:02  2001

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

package jm.music.data;

import jm.JMC;
import jm.util.*;
import jm.gui.cpn.Notate;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Vector;
import java.util.Enumeration;

/**
 * The Phrase class is representative of a single musical phrase.
 * Phrases are held in Parts and can be played at any time
 * based on there start times. They may be played sequentially or in parallel.
 * Phrases can be added to an Part like this...
 * <pre>
 *     Part inst = new Part("Flute");
 *     //Phrase for the right hand
 *     Phrase rightHand = new Phrase(0.0) //start this phrase on the first beat
 *     //Phrase for the left hand
 *     Phrase leftHane = new Phrase(4.0) //start this phrase on the fifth beat
 *     inst.addPhrase(rightHand);
 *     inst.addPhrase(leftHand);
 * </pre>
 * @see Note
 * @see Part
 * @author Andrew Sorensen and Andrew Brown
 * @version 1.0,Sun Feb 25 18:43:32  2001
 */
 
public final class Phrase implements JMC, Cloneable, Serializable{
	//----------------------------------------------
    // Limits
	//-----------------------------------------------
    /** The smallest start time in beats */
	public static final double MIN_START_TIME = 0.0;

	//----------------------------------------------
    // Defaults
	//----------------------------------------------

    public static final String DEFAULT_TITLE = "Untitled Phrase";

    public static final double DEFAULT_START_TIME = MIN_START_TIME;

    public static final int DEFAULT_INSTRUMENT = NO_INSTRUMENT;

    public static final boolean DEFAULT_APPEND = false;

    public static final double DEFAULT_TEMPO = 0.0;

    public static final double DEFAULT_PAN = Note.DEFAULT_PAN;

    public static final int DEFAULT_NUMERATOR = 4;

    public static final int DEFAULT_DENOMINATOR = 4;

	//----------------------------------------------
	// Attributes
	//----------------------------------------------
	/** An array containing mutiple voices */
	private Vector noteList;
	/** The title/name given to this phrase */
	private String title;
	/** The phrases start time in beats */
	private double startTime;
	
	/** instrumet / MIDI program change number for this phrase */
	private int instrument;
        /** speed in beats per minute for this phrase */
    private double tempo = DEFAULT_TEMPO;
	/** Setting the phrase to append when added to a part
	* rather than use its start time.
	*/
	private boolean append = false;
	/** A phrase to have a relative start time with if required. */
    private Phrase linkedPhrase;
    /** The pan position for notes in this phrase.
    * This must be set delibertley to override a note's pan position. */
    private double pan = DEFAULT_PAN;

	/** the top number of the time signature */
	private int numerator;
	/** the bottom number of the time signature */
	private int denominator;

	//----------------------------------------------
	// Constructors
	//----------------------------------------------
	/**
	 * Creates an empty Phrase
	 * -1.0 start time is a flag which means the phrase will be
	 * appended to the end of any part it is added to.
	 */
	public Phrase(){
        this(DEFAULT_START_TIME);
        this.append = true;
	}

	/**
	 * Creates an empty Phrase
	 */
	public Phrase(double startTime){
        this(startTime, DEFAULT_INSTRUMENT);
	}

	/**
	 * Creates an empty Phrase
	 */
    public Phrase(double startTime, int instrument) {
        this(DEFAULT_TITLE, startTime, instrument);
	}
	
	/**
	 * Creates an empty Phrase
	 */
	public Phrase(String title){
        this(title, DEFAULT_START_TIME);
	}
	
	/**
	 * Creates an empty Phrase
	 */
	public Phrase(String title, double startTime){
        this(title, startTime, DEFAULT_INSTRUMENT);
	}
	
	/**
	 * Creates an empty Phrase
	 */
	public Phrase(String title, double startTime, int instrument){
        this(title, startTime, instrument, DEFAULT_APPEND);
	}
	
	/**
	 * Creates an empty Phrase
	 */
	public Phrase(String title, double startTime, int instrument, boolean append){
		this.title = title;
		this.startTime = startTime;
		this.append = append;
		if(instrument < 0){
			System.err.println(new Exception("jMusic EXCEPTION: instrument " +
											 "value must be greater than 0"));
			(new Exception()).printStackTrace();
			System.exit(1); //crash ungracefully 
		}
		this.instrument = instrument;
		this.noteList = new Vector();
        this.numerator = DEFAULT_NUMERATOR;
        this.denominator = DEFAULT_DENOMINATOR;
	}

    /**
     * Constructs a new Phrase containing the specified <CODE>note</CODE>.
     *
     * @param note  Note to be containing by the phrase.
     */
    public Phrase(Note note) {
        this();
        addNote(note);
    }

    /**
     * Constructs a new Phrase containing the specified <CODE>notes</CODE>.
     *
     * @param notes array of Note to be contained by the phrase.
     */
    public Phrase(Note[] notes) {
        this();
        addNoteList(notes);
    }

    /**
     * Constructs a new Phrase containing the specified <CODE>note</CODE> with
     * the specified <CODE>title</CODE>.
     *
     * @param note  Note to be containing by the phrase.
     * @param title String describing the title of the Phrase.
     */
    public Phrase(Note note, String title) {
        this(title);
        addNote(note);
    }

    /**
     * Constructs a new Phrase containing the specified <CODE>notes</CODE> with
     * the specified <CODE>title</CODE>.
     *
     * @param notes array of Note to be contained by the phrase.
     * @param title String describing the title of the Phrase.
     */
    public Phrase(Note[] notes, String title) {
        this(title);
        this.addNoteList(notes);
    }

        /**
            * Constructs a new Phrase containing the specified <CODE>note</CODE> with
         * the specified <CODE>title</CODE>.
         *
         * @param note  Note to be containing by the phrase.
         * @param title String describing the title of the Phrase.
         */
        public Phrase(Note note, double startTime) {
            this(note);
            this.setStartTime(startTime);
        }

	//----------------------------------------------
	// Data Methods
	//----------------------------------------------

	/**
	 * Return the program change assigned by this phrase
	 * @return int
	 */
	public int getInstrument(){
		return this.instrument;
	}

	/**
	 * Sets the program change value
	 * @param int program change
	 */
	public void setInstrument(int value){
		this.instrument = value;
	}
	
	/**
	 * Add a note to this Phrase
	 * @param Note note - add a note to this phrase
	 */
	public void addNote(Note note){
		noteList.addElement(note);
	}


        /**
        * Add a note to this Phrase
         * @param pitch -the pitch of the note
         *@param rv - the rhythmValue of the note
         */
        public void addNote(int pitch, double rv){
            Note note = new Note(pitch, rv);
            noteList.addElement(note);
        }
        
        /**
	 * Add a note to this Phrase
	 * @param Note note - add a note to this phrase
	 */
	public void add(Note note){
		this.addNote(note);
	}
        

    /**
     * Appends the specified notes to the end of this Phrase.
     *
     * @param   array of Notes to append.
     */
    public void addNoteList(Note[] notes) {
        for (int i = 0; i < notes.length; i++) {
            this.noteList.addElement(notes[i]);
        }
    }

	/**
	 * Adds a vector of notes to the phrase
	 * A boolean option when true appends the notes to the end of the list
	 * @param noteVector the vector of notes to add
	 * @param append do we append or not?
	 */
	public void addNoteList(Vector noteVector, boolean append){
		Enumeration enum = noteVector.elements();
		if(!append) this.noteList.removeAllElements();				
		while(enum.hasMoreElements()){
			try{
				Note note = (Note) enum.nextElement();
				this.noteList.addElement(note);
			}catch(RuntimeException re){
				System.err.println("The vector passed to this method must " + "contain Notes only!");
			}
		}
	}

    /**
     * Adds an array of notes to the phrase
     * A boolean option when true appends the notes to the end of the list
     * @param noteArray the array of notes to add
     * @param append do we append or not?
     */
    public void addNoteList(Note[] noteArray, boolean append){
        if(!append) this.noteList.removeAllElements();
        for(int i=0;i<noteArray.length;i++){
            this.noteList.addElement(noteArray[i]);
        }
    }
    
    /**
	 * Adds Multiple notes to the phrase from several arrays 
	 * @param pitchArray array of pitch values
	 * @param rhythmValue a rhythmic value
	 */
	public void addNoteList(int[] pitchArray, double rhythmValue){
		double[] rvArray = new double[pitchArray.length];
		for(int i=0;i<rvArray.length;i++){
			rvArray[i] = rhythmValue;
		}
		this.addNoteList(pitchArray, rvArray);
	}


	/**
	 * Adds Multiple notes to the phrase from several arrays 
	 * @param pitchArray array of pitch values
	 * @param rhythmArray array of rhythmic values
	 */
	public void addNoteList(int[] pitchArray, double[] rhythmArray){
		int[] dynamic = new int[pitchArray.length];
		for(int i=0;i<pitchArray.length;i++){
			dynamic[i] = Note.DEFAULT_DYNAMIC;
		}
		this.addNoteList(pitchArray, rhythmArray, dynamic);
	}

	/**
	 * Adds Multiple notes to the phrase from several arrays 
	 * @param pitchArray array of pitch values
	 * @param rhythmArray array of rhythmic values
	 * @param dynmaic array of dynamic values
	 */
	public void addNoteList(int[] pitchArray, double[] rhythmArray, 
							int[] dynamic){
		this.addNoteList(pitchArray, rhythmArray, dynamic, true); 
	}

	/**
	 * Adds Multiple notes to the phrase from several arrays 
	 * A boolean option when true appends the notes to the end of the list
	 * if non true the current list is errased and replaced by the new notes
	 * @param pitchArray array of pitch values
	 * @param rhythmArray array of rhythmic values
	 * @param dynamic int
	 * @param append do we append or not?
	 */
	public void addNoteList(int[] pitchArray, double[] rhythmArray, 
							int[] dynamic, boolean append){
		if(!append) this.noteList.removeAllElements();
		for(int i=0;i<pitchArray.length;i++){
			try{
				this.noteList.addElement(new Note(pitchArray[i],rhythmArray[i],
												  dynamic[i]));
			}catch(RuntimeException re){
				System.err.println("You must enter arrays of even length");
			}
		}
	}
	
	/**
	 * Adds Multiple notes to the phrase from one array of pitch, rhythm pairs
	 * @param pitchAndRhythmArray  - an array of pitch and rhythm values
	 */
	public void addNoteList(double[] pitchAndRhythmArray){
	    for(int i=0;i< pitchAndRhythmArray.length;i+=2){
	        try {
                    this.noteList.addElement(new Note((int)pitchAndRhythmArray[i], pitchAndRhythmArray[i+1]));
                }catch(RuntimeException re) {
                    System.err.println("Error adding note list: Possibly the wrong number of values in the pitch and rhythm array.");
                }
            }
        }
        
        /**
        * Adds Multiple notes to the phrase all of which start at the same time
         * and share the same duration.
         * @param pitches An array of pitch values
         * @param rv the rhythmValue
         */
        public void addChord(int[] pitches, double rv) {
            for(int i=0; i<pitches.length - 1; i++) {
                Note n = new Note(pitches[i], 0.0);
                n.setDuration(rv * Note.DEFAULT_DURATION_MULTIPLIER);
                this.addNote(n);
            }
            this.addNote(pitches[pitches.length - 1], rv);

            System.out.println("In phrase" + this.toString());
        }
    
	/**
	 * Deletes the specified note in the phrase
	 * @param int noteNumb the index of the note to be deleted
	 */
	 public void removeNote(int noteNumb) {
	    Vector vct = (Vector)this.noteList;
	    try{
	        vct.removeElement(vct.elementAt(noteNumb));
	    } catch (RuntimeException re){
			System.err.println("Note index to be deleted must be within the phrase.");
		}
	}
    
    /**
	 * Deletes the first occurence of the specified note in the phrase
	 * @param note  the note object to be deleted.
	 */
	 public void removeNote(Note note) {
        this.noteList.removeElement(note);
    }		
        
    /**
	 * Deletes the last note in the phrase
	 */
	 public void removeLastNote() {
	    Vector vct = (Vector)this.noteList;
	    vct.removeElementAt(vct.size()-1);
	}
	
	/**
	 * Returns the entire note list contained in a single voice
	 * @return Vector A vector containing all Note objects in this phrase
	 */
	public Vector getNoteList(){
		return this.noteList;
	}
	
	/**
	 * Replaces the entire note list with a new note list vector
	 * @param Vector of notes
	 */
	public void setNoteList(Vector newNoteList){
		this.noteList = newNoteList;
	}
	
	/**
	 * Returns the all notes in the phrase as a array of notes
	 * @return Note[] An array containing all Note objects in this phrase
	 */
	public Note[] getNoteArray(){
		Vector vct = (Vector) this.noteList;
		Note[] noteArray = new Note[vct.size()];
		for(int i=0;i< noteArray.length;i++){
		    noteArray[i] = (Note) vct.elementAt(i);
		}
		return noteArray;
	}
		
	/**
	 * Return the phrases startTime
	 * @return double the phrases startTime
	 */
	public double getStartTime(){
		return this.startTime;
	}
		
		
	/**
	 * Sets the phrases startTime
	 * @param double the time at which to start the phrase
	 */
	public void setStartTime(double startTime){
        if(startTime >= MIN_START_TIME){
            this.startTime = startTime;
            this.setAppend(false);
        }else{
            System.err.println("Error setting phrase start time value: You must enter values greater than "+MIN_START_TIME);
        }
	}
	
	/**
	 * Return the phrases endTime
	 * @return double the phrases endTime 
	 */
	public double getEndTime(){	
	    double tempStartTime = (startTime < MIN_START_TIME) ? MIN_START_TIME : startTime;
	    double endTime = tempStartTime;
		Enumeration enum = this.noteList.elements();
		while(enum.hasMoreElements()){
			Note nextNote = (Note)enum.nextElement();
			endTime += nextNote.getRhythmValue();
		}		
		return endTime;
	}
		
	/**
	 * Return this phrases title
	 * @return String the phrases title
	 */
	public String getTitle(){
		return this.title;
	}
	
	/**
	 * Gives the Phrase a new title 
	 * @param phrases title
	 */
	public void setTitle(String title){
		this.title = title;
	}
	
	/**
	 * Return this phrases append status
	 * @return boolean the phrases append value
	 */
	public boolean getAppend(){
		return this.append;
	}
	
	/**
	 * Gives the Phrase a new append status 
	 * @param boolean the append status
	 */
	public void setAppend(boolean append){
		this.append = append;
	}
	
	/**
	 * Return this phrases this phrase is linked to
	 * @return Phrase the phrases linked to
	 */
	public Phrase getLinkedPhrase(){
		return this.linkedPhrase;
	}
	
	/**
	 * Make a link from this phrase to another 
	 * @param Phrase the phrase to link to
	 */
	public void setLinkedPhrase(Phrase link){
		this.linkedPhrase = link;
	}
	
	/**
	 * Return the pan position for this phrase
	 * @return double the phrases pan setting
	 */
	public double getPan(){
		return this.pan;
	}
	
	/**
	 * Determine the pan position for all notes in this phrase. 
	 * @param double the phrase's pan setting
	 */
	public void setPan(double pan){
		this.pan = pan;
		Enumeration enum = noteList.elements();
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
			note.setPan(pan);
	    }
	}
        
        /**
	 * Return the tempo in beats per minute for this phrase
	 * @return double the phrase's tempo setting
	 */
	public double getTempo(){
		return this.tempo;
	}
        
         /**
	 * Determine the tempo in beats per minute for this phrase
	 * @param double the phrase's tempo
	 */
	public void setTempo(double newTempo){
		 this.tempo = newTempo;
	}

	/**
	 * Get an individual note object by its number 
	 * @param int number - the number of the Track to return
	 * @return Note answer - the note object to return
	 */
	public Note getNote(int number){
		Enumeration enum = noteList.elements();
		int counter = 0;
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
			if(counter == number){
				return note;
			}
			counter++;
		}
		return null;
	}
	
	/**
	 * Get the number of notes in this phrase
	 * @return int  length - the number of notes
	 */
	public int size(){
		return(noteList.size());
	}
    
    /**
	 * Get the number of notes in this phrase
	 * @return int  length - the number of notes
	 */
	public int getSize(){
		return(noteList.size());
	}

	
	/**
     * Returns the numerator of the Phrase's time signature 
	 * @return int time signature numerator
	 */
	public int getNumerator(){
		return this.numerator;
	}
	
	/**
     * Specifies the numerator of the Phrase's time signature
	 * @param int time signature numerator
	 */
	public void setNumerator(int num){
		this.numerator = num;
	}
	
	/**
     * Returns the denominator of the Phrase's time signature
	 * @return int time signature denominator
	 */
	public int getDenominator(){
		return this.denominator;
	}
	
	/**
     * Specifies the denominator of the Phrase's time signature
	 * @param int time signature denominator
	 */
	public void setDenominator(int dem){
		this.denominator= dem;
	}

	/**
	 * Returns a copy of the entire Phrase
	 * @return Phrase a copy of the Phrase
	 */
	public Phrase copy(){
	    Phrase phr = new Phrase(this.title + " copy", this.startTime, this.instrument);
	    phr.setAppend(this.append);
	    phr.setPan(this.pan);
	    phr.setLinkedPhrase(this.linkedPhrase);
	    Enumeration enum = this.noteList.elements();
		while(enum.hasMoreElements()){
	        phr.addNote( ((Note) enum.nextElement()).copy() );
	    }
	    return phr;
	}
	
	 /**
	 * Returns a copy of a specified section of the Phrase,
	 * pads beginning and end with shortedend notes and rests
	 * if notes or phrase boundaries don't align with locations.
	 * @param double start location
	 * @param double end location
	 * @return Phrase a copy of the Phrase
	 */
         public Phrase copy(double startLoc, double endLoc){
                Phrase phr = this.copy(startLoc, endLoc, true);
                return phr;
        }
        
        /**
	 * Returns a copy of a specified section of the Phrase,
	 * pads beginning and end with shortedend notes and rests
	 * if notes or phrase boundaries don't align with locations.
	 * @param double start location
	 * @param double end location
         * @param boolean requireNoteStart if true only notes that start inside
         * the copy range are included in the copy. Notes starting prior but 
         * overlapping are replaced by rests.
	 * @return Phrase a copy of the Phrase
	 */

	public Phrase copy(double startLoc, double endLoc, boolean requireNS){
	    // are the arguments valid?
	    if (startLoc >= endLoc || endLoc < this.startTime) {
                return null;
            }
            boolean requireNoteStart = requireNS;
	    Phrase tempPhr = new Phrase( this.title + " copy", startLoc, this.instrument);
	    tempPhr.setAppend(this.append);
	    tempPhr.setPan(this.pan);
	    tempPhr.setLinkedPhrase(this.linkedPhrase);
	    double beatCounter = this.startTime;
	    if (beatCounter < 0.0) beatCounter = 0.0;
	    //is it before the phrase?
	    if(startLoc < beatCounter) {
	        Note r = new Note(REST, beatCounter - startLoc);
	        tempPhr.addNote(r);
	    }
	    
	    // re there notes before the startLoc to pass up?
	    for(int i=0; i< this.size(); i++) {
	    
	        if (beatCounter < startLoc) { // this note starts before the space
	            if((beatCounter + this.getNote(i).getRhythmValue() > startLoc) &&
	                (beatCounter + this.getNote(i).getRhythmValue() <= endLoc)) { // ends within the space
                        if (requireNoteStart) {
                            Note n = new Note( REST, beatCounter + 
	                        this.getNote(i).getRhythmValue() - startLoc, this.getNote(i).getDynamic());
                            tempPhr.addNote(n);
                        } else {
                            Note n = new Note( this.getNote(i).getPitch(), beatCounter + 
	                        this.getNote(i).getRhythmValue() - startLoc, this.getNote(i).getDynamic());
                            tempPhr.addNote(n);
                        }
	                	            }
	            if(beatCounter + this.getNote(i).getRhythmValue() > endLoc) { // ends after the space
	                if (requireNoteStart) {
                            Note n = new Note( REST, beatCounter + 
	                        this.getNote(i).getRhythmValue() - startLoc, this.getNote(i).getDynamic());
                            tempPhr.addNote(n);
                        } else {
                            Note n = new Note( this.getNote(i).getPitch(), beatCounter + 
	                        endLoc - startLoc, this.getNote(i).getDynamic());
                            tempPhr.addNote(n);
                        }
	            }
	        }
	            
	        if ( beatCounter >= startLoc && beatCounter < endLoc) { // this note starts in the space
	            if (beatCounter + this.getNote(i).getRhythmValue() <= endLoc) { // also ends in it
	                tempPhr.addNote(this.getNote(i));
	            } else { //ends after the end. Make up last note.
	                Note n = new Note(this.getNote(i).getPitch(), endLoc - beatCounter, this.getNote(i).getDynamic());
	                tempPhr.addNote(n);
	            }
	        }
	        beatCounter += this.getNote(i).getRhythmValue();
	    }
	    // is there more space past the end of the phrase?
	    if (beatCounter < endLoc) { // make up a rest to fill the space
	        Note r = new Note(REST, endLoc - beatCounter);
	        tempPhr.addNote(r);
	    }
	    // done!
	    return tempPhr;
	}

	/**
	 * Prints the tracks attributes to stdout
	 */
	public String toString(){
		String phraseData = new String("--------- PHRASE: " + 
			title + ". Contains " + this.size() + " notes.  Start time: " + 
			startTime +" ---------" +'\n');
		Enumeration enum = getNoteList().elements();
		int counter = 0; 
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
			phraseData = phraseData + note.toString() + '\n';
		}
		return phraseData;
	}
	
	/**
	* Empty removes all elements in the vector
	*/
	public void empty(){
		noteList.removeAllElements();
	}
	
	/**
	 * Returns a carbon copy of a specified section of the Phrase
	 * Changes to notes in the original or the alias will be echoed in the other.
	 * Note: that for this to work other phrase classes must to change the 
	 * noteList attribute to point to another object, but instead
	 * should always update the noteList itself. See shuffle() as an example.
	 */
	public Phrase alias() { 
	    Phrase phr = new Phrase(this.title + " alias", this.startTime, this.instrument);	    
	    phr.noteList = this.noteList;
	    return phr;
	}
    
    /**
	 * Return the value of the highest note in the phrase.
     */
    public int getHighestPitch() {
        int max = 0;
        Enumeration enum = getNoteList().elements();
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
            if(note.getPitch() > max) max = note.getPitch();
        }
        return max;
    }
    
    /**
	 * Return the value of the lowest note in the phrase.
     */
    public int getLowestPitch() {
        int min = 127;
        Enumeration enum = getNoteList().elements();
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
            if(note.getPitch() < min) min = note.getPitch();
        }
        return min;
    }
    
    /**
	 * Return the value of the longest rhythm value in the phrase.
     */
    public double getLongestRhythmValue() {
        double max = 0.0;
        Enumeration enum = getNoteList().elements();
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
            if(note.getRhythmValue() > max) max = note.getRhythmValue();
        }
        return max;
    }
    
    /**
	 * Return the value of the shortest rhythm value in the phrase.
     */
    public double getShortestRhythmValue() {
        double min = 1000.0;
        Enumeration enum = getNoteList().elements();
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
            if(note.getRhythmValue() < min) min = note.getRhythmValue();
        }
        return min;
    }
    
    /**
	 * Change the dynamic value of each note in the phrase.
     */
    public void setDynamic(int dyn) {
        Enumeration enum = getNoteList().elements();
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
            note.setDynamic(dyn);
        }
    }
    
    /**
	 * Change the pitch value of each note in the phrase.
     */
    public void setPitch(int val) {
        Enumeration enum = getNoteList().elements();
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
            note.setPitch(val);
        }
    }
    
    /**
	 * Change the rhythmValue value of each note in the phrase.
     */
    public void setRhythmValue(int val) {
        Enumeration enum = getNoteList().elements();
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
            note.setRhythmValue(val);
        }
    }
    
    /**
	 * Change the Duration value of each note in the phrase.
     */
    public void setDuration(int val) {
        Enumeration enum = getNoteList().elements();
		while(enum.hasMoreElements()){
			Note note = (Note) enum.nextElement();
            note.setDuration(val);
        }
    }
}
	
