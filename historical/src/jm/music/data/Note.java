/*
<This Java Class is part of the jMusic API version 1.0,Sun Feb 25 18:35:00  2001
Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.
This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public Licens
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
package jm.music.data;

import java.io.Serializable;

/**
 * The Note class is representative of notes in standard western music
 * notation. It contains data relavent to music note information such
 * as time, duration and pitch. Notes get contained in Phrase objects. 
 * Like in real music (CPN) notes get played one after the other in the order in 
 * which they are added to the Phrase. <br>
 * !IMPORTANT NOTE: notes with a pitch of the minimum integer are rests, those between 0 <> 127
 * are exactly the same as normal sounding notes numbered like the MIDI specification.
 * Notes can be added to a phrase like this...
 * <pre>
 *     Phrase phrase = new Phrase(0.0);
 *     Note note = new Note(C4, CROTCHET, MF); 
 *     phrase.addNote(note);
 * </pre>  
 * The note also has the option to create notes with reasonable default values.
 * This allows a user who is not interested in any performance details to work
 * with notes solely using pitch value and rythmValue like this....
 * <pre>
 *     Note note = new Note(C4, CROTCHET);
 * </pre>
 * The final option for building a note also includes the dynamic parameter for
 * minimal performace information.
 * <pre>
 *     Note note = new Note(C4, CROTCHET, F);
 * </pre>
 *
 * <B>Comments about the offset parameter:</B>
 * The intention of offset was to allow for 'feel' in score playback. For example 
 * a snare drum might be played slightly ahead or behind the beat, or a sound with 
 * a slow attack might need to be triggered early in order to sound in time with other parts.
 *
 * With this in mind, offset should have no influence on rhythmValue or duration calculations 
 * within the jMusic data structure, only when translated to another format (eg MIDI). In your 
 * example (offset 0.1, duration 0.8, and rv 1.0) the rendering (MIDI or other that does not 
 * support the offset concept) should consider the start time of the note 0.1 bpm later than 
 * 'normal' but the duration of the note will be 0.8 bpm from that offset start time. The next
 *  note should start 1.0 bpm after the 'normal' startTime (ignoring offset). This allows for 
 * offset to be used in cases where performance of music varies from beat to beat, eg., the 
 * downbeat is anticipated slightly. In extreme cases this could cause overlapping notes in 
 * MIDI translation which need to be 'handled' but I consider this a weakness of the MIDI 
 * data specification rather than a problem for jMusic : )
 *
 * In short, think of offset as "A displacement from the normal start time of a note that allows 
 * performance interpretation without distorting the score for compositional or analysis purposes."
 *---------------
 * @see Phrase
 * @author Andrew Sorensen
 * @version 1.0,Sun Feb 25 18:43:31  2001
 */

public class Note implements Cloneable, Serializable{
	//----------------------------------------------
    // Defaults
	//-----------------------------------------------

    /** default pitch value */
	public static final double DEFAULT_PITCH = 60.0;

	/** default rhythmValue*/
	public static final double DEFAULT_RHYTHM_VALUE = 1.0;

	/** default dynamic*/
	public static final int DEFAULT_DYNAMIC = 85;

	/** default pan value*/
	public static final double DEFAULT_PAN = 0.5;

	/** default duration multiplier*/
	public static final double DEFAULT_DURATION_MULTIPLIER = 0.9;

	/** default duration*/
	public static final double DEFAULT_DURATION =
    DEFAULT_RHYTHM_VALUE * DEFAULT_DURATION_MULTIPLIER;

	/** default offset value*/
	public static final double DEFAULT_OFFSET = 0.0;
    
    /** The number of seconds into a sample to begin reading data */
    public static final double DEFAULT_SAMPLE_START_TIME = 0.0;


	//----------------------------------------------
	// Constants
	//-----------------------------------------------
	
    /** The smallest value for a pitch. */
    public final static double MIN_PITCH = 0.0;
    /** The largest value for a pitch. */
    public final static double MAX_MIDI_PITCH = 127.0;
    /** The largest value for a pitch. */
    public final static int MAX_PITCH = 127;
    /** The smallest value for a rhythmValue. */
    public final static double MIN_RHYTHM_VALUE = 0.0;
    /** The largest value for a rhythValue. */
    public final static double MAX_RHYTHM_VALUE= Double.MAX_VALUE;
    /** The smallest value for a dynamic. */
    public final static int MIN_DYNAMIC = 0;
    /** The largest value for a dynamic. */
    public final static int MAX_DYNAMIC = 127;
    /** The smallest pan value for a note. */
    public final static double MIN_PAN = 0.0;
    /** The largest pan value for a note. */
    public final static double MAX_PAN = Double.MAX_VALUE;
    /** The smallest value for a note's duration. */
    public final static double MIN_DURATION = 0.0;
    /** The largest value for a note's duration. */
    public final static double MAX_DURATION = Double.MAX_VALUE;
    /** The pitch value which indicates a rest. */
    public static final double REST = (double)Integer.MIN_VALUE;
    /** Pitch type flag - indicates that pitch should be a frequency in hertz. */
    public static final boolean FREQUENCY = true;
    /** Pitch type flag - indicates that pitch should be MIDI note number. */
    public static final boolean MIDI_NOTE = false;
	
	//----------------------------------------------
	// Attributes
	//----------------------------------------------
	/** Pitch value ranging from 0-127 (middle C = 60) */
	private double pitch;
	/** Dynamic value ranging from 0-127 (0 = off; 1 = quiet; 127=loud) */
	private int dynamic;
	/** 
	 * The length of the note. Constant values representing standard note
	 * lengths are defined in JMC
	 */
	private double rhythmValue;
	/** 0.0 (full left) 0.5 (center) 1.0 (full right) 2.0 (3rd channel) etc..*/
	private double pan;
	/** A notes duration of time in beats */ 
	private double duration;
	/** 
	 * A notes offset time. How far in front or behind of the rhythmic value
	 * should we place this note
	 */
	private double offset;
	/** The time into a sample for playback to begin in milliseconds - for audio only */ 
	private double sampleStartTime;
        /** The type of value that pitch is representing - frequency or MIDI note */ 
	private boolean pitchType;

    /**
    * Default constructor assigns null values to all note attributes
    */
    public Note() {
        this(DEFAULT_PITCH, DEFAULT_RHYTHM_VALUE);
        this.pitch = DEFAULT_PITCH;
        this.rhythmValue = DEFAULT_RHYTHM_VALUE;
        this.dynamic = DEFAULT_DYNAMIC;
        this.pan = DEFAULT_PAN; //centre pan
        this.duration = DEFAULT_DURATION;
        this.offset = DEFAULT_OFFSET;
    }

	/**
	 * Assigns pitch and rhythmic values to the note object upon creation
	 * Other values (e.g. dynamic) are given reasonable defaults
	 * @param MIDI pitch range is 0-127 (middle c = 60): Constant values
	 *              representing pitch values can be found in JMC
	 * @param rhythmValue 0.5 = quaver: constant values representing most 
	 *                 duration types can be found in JMC
	 */
	public Note(int pitch, double rhythmValue){
		this(pitch, rhythmValue, DEFAULT_DYNAMIC);
	}

	/**
	 * Assigns pitch and rhythmic values to the note object upon creation
	 * Other values (e.g. dynamic) and given reasonable defaults
	 * @param MIDI pitch range is 0-127 (middle c = 60): Constant values
	 *              representing pitch values can be found in JMC
	 * @param rhythmValue 0.5 = quaver: constant values representing most
	 *                    duration types can be found in JMC
	 * @param dynamic range is 0-127 (0 = off; 127 = loud): Constant values
	 *               representing some basic dynamic types can be found in JMC
	 */
	public Note(int pitch, double rhythmValue, int dynamic){
            //if (pitch > 0 ) {
                this.pitch = (double)pitch;
            //} else {
            //    System.err.println("jMusic Note constructor error: Pitch must be greater than zero!");
            //    System.exit(1);
            //}
            pitchType = MIDI_NOTE;
            this.rhythmValue = rhythmValue;
            this.dynamic = (dynamic < MIN_DYNAMIC) 
                            ? MIN_DYNAMIC 
                            : ((dynamic > MAX_DYNAMIC) ? MAX_DYNAMIC : dynamic);
            this.pan = DEFAULT_PAN;
            this.duration = rhythmValue * DEFAULT_DURATION_MULTIPLIER;
            this.offset = DEFAULT_OFFSET;
            this.sampleStartTime = DEFAULT_SAMPLE_START_TIME;
	}
    
    /**
    * Assigns frequency and rhythmic values to the note object upon creation
     * @param frequency Pitch in hertz, any double value (A4 = 400)
     * @param rhythmValue 0.5 = quaver: constant values representing most
     *                 duration types can be found in JMC
     */
    public Note(double frequency, double rhythmValue){
        this(frequency, rhythmValue, DEFAULT_DYNAMIC);
    }
        
    /**
    * Assigns frequency and rhythmic values to the note object upon creation
     * @param frequency Pitch in hertz, any double value (A4 = 400)
     * @param rhythmValue 0.5 = quaver: constant values representing most
     *                 duration types can be found in JMC
     * @param dynamic range is 0-127 (0 = off; 127 = loud): Constant values
    *               representing some basic dynamic types can be found in JMC
     */
    public Note(double frequency, double rhythmValue, int dynamic){
        if (frequency > 0.0) {
            this.pitch = frequency;
        } else {
            System.err.println("jMusic Note constructor error: Pitch must be greater than zero.");
            System.exit(1);
        }
        pitchType = FREQUENCY;
        this.rhythmValue = rhythmValue;
        this.dynamic = (dynamic < MIN_DYNAMIC)
            ? MIN_DYNAMIC
            : ((dynamic > MAX_DYNAMIC) ? MAX_DYNAMIC : dynamic);
        this.pan = DEFAULT_PAN;
        this.duration = rhythmValue * DEFAULT_DURATION_MULTIPLIER;
        this.offset = DEFAULT_OFFSET;
        this.sampleStartTime = DEFAULT_SAMPLE_START_TIME;
    }
        

	//----------------------------------------------
	// Data Methods
	//----------------------------------------------
    /**
    * Retrieve note's pitch type
     * @return boolean notes pitch type
     */
    public boolean getPitchType(){
        return this.pitchType;
    }

    /**
    * Specifies the note's pitch type. 
     * There are constants for FREQUENCY and MIDI_PITCH
     * @param boolean note's pitch type
     */
    public void setPitchType(boolean newType){
         this.pitchType = newType;
    }
    
    

    /**
	 * Retrieve the note's pitch as a frequency
	 * @return double note's pitch
	 */
	public double getFrequency(){
		return this.pitch;
	}

	/**
	 * Assign notes pitch as a frequency.  If the parameter <CODE>pitch</CODE> is less than 
     * {@link #MIN_PITCH} then the pitch of this note will be set to MIN_PITCH.  
     * Likewise, if <CODE>pitch</CODE> is greater than {@link #MAX_MIDI_PITCH}, pitch
	 * will be set to MAX_MIDI_PITCH.
	 *
	 * @param int notes pitch 
	 */
	public void setFrequency(double pitch){
	    if (pitch == REST) {
		    this.pitch = pitch;
		} else {
		    try {
                        this.pitch = (pitch < MIN_PITCH) 
                            ? MIN_PITCH : pitch;
                    }catch(RuntimeException re){
				System.err.println("Error setting pitch value: You must enter pitch values above "+MIN_PITCH);
			}
                }
            pitchType = FREQUENCY;
	}

    /**
    * Retrieve the note's pitch as an integer.
     * Useful for working with pitch as MIDI note numbers.
     * @return int note's pitch
     */
    public int getPitch(){
        return (int)this.pitch;
    }

    /**
    * Assign notes pitch.  If the parameter <CODE>pitch</CODE> is less than
     * {@link #MIN_PITCH} then the pitch of this note will be set to MIN_PITCH.
     * Likewise, if <CODE>pitch</CODE> is greater than {@link #MAX_MIDI_PITCH}, pitch
     * will be set to MAX_MIDI_PITCH.
     *
     * @param int notes pitch
     */
    public void setPitch(int pitch){
            try {
                this.pitch = (pitch < MIN_PITCH)
                ? MIN_PITCH
                : ((pitch > MAX_MIDI_PITCH) ? MAX_MIDI_PITCH : pitch);
            }catch(RuntimeException re){
                System.err.println("Error setting pitch value: You must enter pitch values between "
                                   + MIN_PITCH + " and " +MAX_MIDI_PITCH);
            }
        pitchType = MIDI_NOTE;
    }
    

	/**
	 * Retrieve note's rhythmValue
	 * @return float notes rhythmValue 
	 */
	public double getRhythmValue(){
		return this.rhythmValue;
	}

	/**
	 * Assign notes rhythmValue
	 * @param float notes rhythmValue
	 */
	public void setRhythmValue(double rhythmValue){
		this.rhythmValue = (rhythmValue < MIN_RHYTHM_VALUE) 
		                   ? MIN_RHYTHM_VALUE 
		                   : ((rhythmValue > MAX_RHYTHM_VALUE) 
		                      ? MAX_RHYTHM_VALUE 
		                      : rhythmValue);
	}
	/**
	 * Retrieve notes dynamic
	 * @return int notes dynamic 
	 */
	public int getDynamic(){
		return this.dynamic;
	}

	/**
	 * Assign notes dynamic
	 * @param int notes dynamic
	 */
	public void setDynamic(int dynamic){
        this.dynamic = (dynamic < MIN_DYNAMIC) 
		             ? MIN_DYNAMIC 
		             : ((dynamic > MAX_DYNAMIC) ? MAX_DYNAMIC : dynamic);
	}

	/**
	 * Retrieves note's pan
	 * @return notes pan
	 */
	public double getPan(){
		return this.pan;
	}

	/**
	 * Assign notes pan
	 * @param double note's pan
	 */
	public void setPan(double pan){
        this.pan = (pan < MIN_PAN) 
                   ? MIN_PAN 
		           : ((pan > MAX_PAN) ? MAX_PAN : pan);
	}

	/**
	 * Return note duration
	 * @return double note's duration
	 */
	public double getDuration(){
		return this.duration;
	}

	/**
	 * Set notes duration
	 * @param double note's duration
	 */
	public void setDuration(double duration){
        this.duration = (duration < MIN_DURATION) 
		             ? MIN_DURATION 
		             : ((duration > MAX_DURATION) ? MAX_DURATION : duration);
	}
	
	/**
	 * Return note offset
	 * @return double note's offset
	 */
	public double getOffset(){
		return this.offset;
	}

	/**
	 * Set notes offset
	 * @param double note's offset
	 */
	public void setOffset(double offset){
		this.offset = offset;
	}
	
	/**
	 * Return note sampleStartTime
	 * @return int note's sampleStartTime
	 */
	public double getSampleStartTime(){
		return this.sampleStartTime;
	}

	/**
	 * Set notes sampleStartTime
	 * @param int note's sampleStartTime
	 */
	public void setSampleStartTime(double sampleStartTime){
		this.sampleStartTime = sampleStartTime;
	}


	/**
	 * Returns a copy of this note
	 * @return Note
	 */
	public Note copy(){
            Note note;
            if (pitchType == MIDI_NOTE) {
                note = new Note((int)this.pitch, this.rhythmValue, this.dynamic);
            } else note = new Note(this.pitch, this.rhythmValue, this.dynamic);
            note.setPan(this.pan);
            note.setDuration(this.duration);
            note.setOffset(this.offset);
            note.setSampleStartTime(this.sampleStartTime);
            return (Note)note;
	}
    
	//----------------------------------------------
	// Utility Methods
	//----------------------------------------------
	/**
	 * Collects a string of the notes attributes
	 */
	public String toString(){
		String noteDetails =  new String("Note: " +
                            "[Pitch = " + pitch + 
                           "][RhythmValue = " + rhythmValue + 
                            "][Dynamic = " + dynamic + 
                            "][Pan= " + pan +
                            "][Duration = " + duration + "]");
	    return noteDetails;
	}
	
	/**
	 * Checks if the note is within a particular scale
	 * There are a number of scale constants specified in the JMC
	 * which can be used with this method,
	 * these include MAJOR_SCALE, MINOR_SCALE, and PENTATONIC_SCALE
	 * @param int[] - an array of scale degrees
	 * @return boolean - true means it is in the scale
	 */
	public boolean isScale(int[] scale) {
        for (int j = 0; j < scale.length; j++) {
            if (this.pitch % 12 == scale[j]) {
                return true;
            }
        }
        return false;
    }
}
