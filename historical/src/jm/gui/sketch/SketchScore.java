/*

<This Java Class is part of the jMusic API version 1.0,Sun Feb 25 18:34:41  2001

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/ 

package jm.gui.sketch;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import jm.JMC;
import jm.music.data.*;
import jm.midi.*;

/**
* A jMusic tool which displays a score as a simple
* 'piano roll' display in a window.
* The tool displays a jMusic class as a simple piano roll view. To use it write:
* new ViewScore(scoreName);
* Where scoreName is the jMusic Score object.
* Alternately:
* new ViewScore(scoreName, xpos, ypos);
* Where xpos and ypos are intergers specifying the topleft position of the window. 
* This is useful if you want to use ViewScore in conjunction with some other GUI interface 
* which is already positioned in the top left corner of the screen.
* @author Andrew Brown and Andrew Troedson
 * @version 1.0,Sun Feb 25 18:43:13  2001
*/

public class SketchScore extends Frame implements WindowListener {
	//attributes
	private static int maxWidth;
	private static int maxParts;
	protected static Score score;
	protected double beatWidth = 10.0;
	private Panel pan;
	private SketchScoreArea da;
	private SketchRuler ruler;
	
	//--------------
	//constructors
	//--------------
	public SketchScore(Score score) {
		this(score, 0, 0);
	}
	
	public SketchScore(Score score, int xPos, int yPos) {
		super(score.getTitle());
		
		this.score = score;
		this.getWidthAndParts();
		
		//register the closebox event
		this.addWindowListener(this);
		
		//add a scroll pane
		ScrollPane sp = new ScrollPane(ScrollPane.SCROLLBARS_AS_NEEDED);
		sp.getHAdjustable().setUnitIncrement(20); //set scroll speed
		pan = new Panel();
		pan.setLayout(new BorderLayout());
		da = new SketchScoreArea(score, maxWidth, beatWidth);
		pan.add("Center", da);
		
		//add a ruler
		ruler = new SketchRuler(this);
		pan.add("South", ruler);
		
		sp.add(pan);
		sp.setSize(600,160);
		this.add(sp);
		
		//construct and display						
		this.pack();
		this.setLocation(xPos,yPos);							
		this.show();
	}
	
	//--------------
	// Class Methods
	//--------------
	
	// Deal with the window closebox
	public void windowClosing(WindowEvent we) {
		this.dispose(); //System.exit(0);
	}
	//other WindowListener interface methods
	//They do nothing but are required to be present
	public void windowActivated(WindowEvent we) {};
	public void windowClosed(WindowEvent we) {};
	public void windowDeactivated(WindowEvent we) {};
	public void windowIconified(WindowEvent we) {};
	public void windowDeiconified(WindowEvent we) {};
	public void windowOpened(WindowEvent we) {};
	
	public void update() {
        pan.repaint();
        da.setSize( (int)Math.round( score.getEndTime()*beatWidth),240);
        da.setBeatWidth(beatWidth);
        da.repaint();
        ruler.repaint();
        //this.repaint();
        this.pack();
    }
    
   
    
	private void getWidthAndParts() {
		Enumeration enum = score.getPartList().elements();
		while(enum.hasMoreElements()){
			Part part = (Part) enum.nextElement();
			maxParts++;
			Enumeration enum2 = part.getPhraseList().elements();
			while(enum2.hasMoreElements()){
				Phrase phrase = (Phrase) enum2.nextElement();
				Enumeration enum3 = phrase.getNoteList().elements();
				maxWidth = (int) (phrase.getStartTime() * beatWidth);
				while(enum3.hasMoreElements()){
					Note aNote = (Note) enum3.nextElement();
					maxWidth = maxWidth + (int)(aNote.getRhythmValue() * beatWidth);
				}
			}
		}
	}
	
	
}
