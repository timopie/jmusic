/*
 * PianoRoll.java 0.0.1 21st July 2000
 *
 * Copyright (C) 2000 Adam Kirby
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package jm.gui.notation;

import java.awt.ScrollPane;

import jm.music.data.Score;
import jm.music.data.Part;
import jm.music.data.Phrase;
import jm.music.data.Note;

public class PianoRoll extends ScrollPane {
    protected PianoRollCanvas prc;

    public PianoRoll() {
        this(new Note());
    }

    public PianoRoll(Score score) {
        super();
        prc = new PianoRollCanvas(score);
        add(prc);
        validate();
//        repaint();
    }

    public PianoRoll(Note note) {
        super();
        prc = new PianoRollCanvas(note);
        add(prc);
        validate();
//        repaint();
    }

    public PianoRoll(Phrase phrase) {
        super(ScrollPane.SCROLLBARS_ALWAYS);
        prc = new PianoRollCanvas(phrase);
        add(prc);
        validate();
//        repaint();
        prc.repaint();
    }

    public void setMusic(Score score) {
//        prc.setMusic(score);
    }

    public void setMusic(Part part) {
    }

    public void setMusic(Phrase phrase) {
        prc = new PianoRollCanvas(phrase);
        removeAll();
        add(prc);
        validate();
//        repaint();
    }

    public void setMusic(Note note) {
    }
}
