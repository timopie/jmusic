/*
 * PianoRollCanvass.java 0.0.1 21st July 2000
 *
 * Copyright (C) 2000 Adam Kirby
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package jm.gui.notation;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;
import java.awt.Dimension;

import jm.music.data.Score;
import jm.music.data.Note;
import jm.music.data.Phrase;

/**
 * A jMusic tool which displays a score as a
 * psudo Common Practice Notation in a window.
 * @author Adam Kirby 
 * @version 1.0,Sun Feb 25 18:43:08  2001
 */
public class PianoRollCanvas extends NotationCanvas {
    protected boolean[] isBlank = {false, false, false, false, true,  false,
                                   false, false, false, false, false, true};

    protected int[] lineDarkness = {204, 204, 0, 204, 204, 0, 204, 204, 204,
                                    0, 204, 204, 0, 204, 204, 204, 0, 204, 204,
                                    0, 204, 204, 204, 0};

    protected boolean[] lineExceptions = {true, false, false, false, false,
                                          false};

    protected String[] label = {" C", " C#/Db", " D", " D#/Eb", " E",
                                " F", " F#/Gb", " G", " G#/Ab", " A",
                                " A#/Bb", " B", };

    private int lineSeparation = 3;

    protected Dimension preferredSize = new Dimension(400, 146  * lineSeparation
                                                      + 1); // 877);

    protected Image pitchImage;

    protected Graphics pitchGraphics;

    protected Image musicImage;

    protected Graphics musicGraphics;

    protected boolean hasMusicChanged = true;

    public PianoRollCanvas() {
        super();
    }

    public PianoRollCanvas(Score score) {
        super(score);
    }

    public PianoRollCanvas(Note note) {
        super(note);
    }

    public PianoRollCanvas(Phrase phrase) {
        super(phrase);
    }

    public Dimension getPreferredSize() {
        return preferredSize;
    }

    protected void paintPitch() {
        pitchImage = createImage(58, 292 * lineSeparation + 1); // 1753);
        pitchGraphics = pitchImage.getGraphics();

        int cumulativeOffset = 0;

        for (int i = 127; i > -1; i--) {
            if (isBlank[i % isBlank.length]) {
                cumulativeOffset += lineSeparation * 2; // 12;
            }
            pitchGraphics.drawString(Integer.toString(i)
                                     + label[i % label.length], 0,
                                     lineSeparation * 2 * (128 - i)
                                     + cumulativeOffset);
        }
    }

    protected void paintBuffer() {
        if (pitchImage == null) {
            paintPitch();
        }

        if (hasMusicChanged) {
            try {
                paint((Note) music);
            } catch (ClassCastException e) {
            }
            try {
                paint((Phrase) music);
            } catch (ClassCastException e) {
            }

            hasMusicChanged = false;

            image = createImage(pitchImage.getWidth(this)
                                + musicImage.getWidth(this),
                                146 * lineSeparation + 1);
            preferredSize = new Dimension(pitchImage.getWidth(this)
                                          + musicImage.getWidth(this),
                                          146 * lineSeparation + 1);
            graphics = image.getGraphics();                            
            graphics.drawImage(pitchImage.getScaledInstance(29, -1,
                                                            Image.SCALE_SMOOTH),
                               0, 0, this);
            graphics.drawImage(musicImage, 30, 0, this);
        }
    }

    protected void paint(Note note) {
        musicImage = createImage(note.getRhythmValue() > 0
                                 ? (int) (64 * note.getRhythmValue() + 1)
                                 : 1,
                                 146 * lineSeparation + 1);
        musicGraphics = musicImage.getGraphics();
        paintLines((int) (64 * note.getRhythmValue()));
        paintBarLines(64, (int) note.getRhythmValue(), 4);
        musicGraphics.setColor(new Color((float) Math.random(),
                                         (float) Math.random(),
                                         (float) Math.random()));
        paintNote(note, 0.0);
    }

    protected void paint(Phrase phrase) {
        musicImage = createImage(phrase.getEndTime() > 0
                                 ? (int) (64 * phrase.getEndTime() + 1)
                                 : 1,
                                 146 * lineSeparation + 1);
        musicGraphics = musicImage.getGraphics();
        paintLines((int) (64 * phrase.getEndTime()));
        paintBarLines(64, (int) phrase.getEndTime(), 4);
        musicGraphics.setColor(new Color((float) Math.random(),
                                         (float) Math.random(),
                                         (float) Math.random()));
        double time = phrase.getStartTime();
        for (int i = 0; i < phrase.size(); i++) {
            paintNote(phrase.getNote(i), time);
            time += phrase.getNote(i).getRhythmValue();
        }
    }

    protected void paintBarLines(int width, int beats, int beatsPerBar) {
        for (int i = 0; i < beats; i++) {
            musicGraphics.setColor(Color.black);
            musicGraphics.drawString(Integer.toString(i), width * (i), 10);
            if (i % beatsPerBar != beatsPerBar - 1) {
                musicGraphics.setColor(Color.lightGray);
            }
            musicGraphics.drawLine(width * (i + 1), 0,
                                   width * (i + 1), musicImage.getHeight(this));
        }
    }

    protected void paintLines(int width) {
        int cumulativeOffset = -1; // -3;
        int exceptionCount = 4;
        for (int i = 127; i > -1; i--) {
            if (isBlank[i % isBlank.length]) {
                cumulativeOffset += lineSeparation;
            }
            musicGraphics.setColor(new Color(lineDarkness[i % lineDarkness.length],
                                             lineDarkness[i % lineDarkness.length],
                                             lineDarkness[i % lineDarkness.length]));
            if (lineDarkness[i % lineDarkness.length] == 0) {
                if (exceptionCount == 5) {
                    musicGraphics.setColor(new Color(204, 204, 204));
                    musicGraphics.drawLine(0, lineSeparation * (128 - i) + cumulativeOffset,
                                           width, lineSeparation * (128 - i) + cumulativeOffset);

                    exceptionCount = 0;
                } else {
                    ++exceptionCount;
                    musicGraphics.drawLine(0, lineSeparation * (128 - i) + cumulativeOffset - 1,
                                       width, lineSeparation * (128 - i) + cumulativeOffset - 1);
                    musicGraphics.drawLine(0, lineSeparation * (128 - i) + cumulativeOffset,
                                       width, lineSeparation * (128 - i) + cumulativeOffset);
                    musicGraphics.drawLine(0, lineSeparation * (128 - i) + cumulativeOffset + 1,
                                       width, lineSeparation * (128 - i) + cumulativeOffset + 1);
                }
            } else {
                musicGraphics.drawLine(0, lineSeparation * (128 - i) + cumulativeOffset,
                                       width, lineSeparation * (128 - i) + cumulativeOffset);
            }
        }
    }

    protected void paintNote(Note note, double startTime) {
        int pitch = note.getPitch();
        if (pitch < 128 && pitch >= 0) {            
            int maxPitch = maxPitchLocation(pitch);
            int minPitch = minPitchLocation(pitch);
            int startLocation = timeLocation(startTime);
            int endRVLocation = timeLocation(note.getRhythmValue() + startTime);
            int endDLocation = timeLocation(note.getDuration() + startTime);
            musicGraphics.drawRect(startLocation, maxPitch,
                                   endRVLocation - startLocation,
                                   minPitch - maxPitch);
            musicGraphics.fillRect(startLocation, maxPitch,
                                   endDLocation - startLocation,
                                   minPitch - maxPitch);
        }
    }

    protected int maxPitchLocation(int pitch) {
        int modifier = 0;
        for (int i = pitch; i < 128; i++) {
            if (isBlank[i % isBlank.length]) {
                modifier += lineSeparation;
            }
        }
        return (127 - pitch) * lineSeparation + modifier;
    }

    protected int minPitchLocation(int pitch) {
        int modifier = 0;
        for (int i = pitch; i < 128; i++) {
            if (isBlank[i % isBlank.length]) {
                modifier += lineSeparation;
            }
        }

        return (127 - pitch) * lineSeparation + modifier + 4;
    }

    protected int timeLocation(double time) {
        return (int) (64.0 * time);
    }
}
