/*
 * NotationCanvas.java 0.0.1 30th July 2000
 *
 * Copyright (C) 2000 Adam Kirby
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package jm.gui.notation;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;
import java.awt.Dimension;

import jm.music.data.Score;
import jm.music.data.Note;
import jm.music.data.Phrase;

/**
 * A jMusic tool which displays a score as a
 * psudo Common Practice Notation in a window.
 * @author Andrew Brown 
 * @version 1.0,Sun Feb 25 18:43:06  2001
 */
public abstract class NotationCanvas extends Canvas {
    protected Object music;

    protected Image image;

    protected Graphics graphics;

    protected Dimension preferredSize = new Dimension(1, 1);

    protected Dimension minimumSize = new Dimension(1, 1);

    public Dimension getMinimumSize() {
        return minimumSize;
    }

    public Dimension getPreferredSize() {
        return preferredSize;
    }

    public NotationCanvas() {
        this(new Score());
    }

    public NotationCanvas(Score score) {
        music = score;
    }

    public NotationCanvas(Note note) {
        music = note;
    }

    public NotationCanvas(Phrase phrase) {
        music = phrase;
    }

    protected abstract void paintBuffer(); 

    public void update(Graphics g) {
        paint(g);
    }        

    public void paint(Graphics g) {
        if (image == null) {
            image = createImage(1, 1);
            graphics = image.getGraphics();
        }

        paintBuffer();

        g.drawImage(image, 0, 0, this);
    }
}
 
