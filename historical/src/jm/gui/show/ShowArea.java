/*

<This Java Class is part of the jMusic API version 1.0,Sun Feb 25 18:34:36  2001

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
/**
* A java canvas object which displays a score as a
* psudo Common Practice Notation in a window.
* Used as part of jMusic ShowScore, and other apps.
* @author Andrew Brown 
 * @version 1.0,Sun Feb 25 18:43:09  2001
* ---------------------
*/
package jm.gui.show;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import jm.JMC;
import jm.music.data.*;
import jm.midi.*;
//import java.net.*; //used here to get gifs from a URL
//import java.awt.image.*; //


//--------------
//second class!!
//--------------
public class ShowArea extends Canvas {
	//attributes
	private int oldX;
	private int maxColours = 4;
	private float[][] theColours = new float[maxColours][3];
	private int areaHeight = 240;
	private int[] noteOffset = {0,0,3,3,6,9,9,12,12,15,15,18};
	private Font f = new Font("Helvetica",Font.PLAIN, 10);
	private ShowPanel sp;
	
	public ShowArea(ShowPanel sp){
		super();
		this.sp = sp;
		this.setSize((int)(sp.score.getEndTime()*sp.beatWidth),areaHeight); //width and height of score notation area
		
		for (int i=0; i<maxColours;i++) {
			theColours[i][0] = (float)(Math.random()/ maxColours/2) + (float)(1.0/ maxColours *i);
			theColours[i][1] = (float)(Math.random()/ maxColours) + (float)(1.0/ maxColours *i);
		}
	}
	
	//public void update(Graphics g) {
	//	paint(g);
	//}
	
	public void paint(Graphics offScreenGraphics) {
	    //Image offScreenImage = this.createImage();
		//Graphics offScreenGraphics = g.create();//offScreenImage.getGraphics();
		int rectLeft, rectTop, rectRight, rectBot;
		//clear
		//offScreenGraphics.setColor(Color.white);
		//offScreenGraphics.fillRect(0,0,maxWidth,areaHeight);
		//get current maxWidth
		offScreenGraphics.setFont(f);
		//paint staves
		offScreenGraphics.setColor(Color.black);
		// e above middle C is at 255
		//treble
		double beatWidth = sp.beatWidth;
		int maxWidth = (int)Math.round(sp.score.getEndTime()*beatWidth);
		int e = 126;
		int w = 6; //width between stave lines
		offScreenGraphics.drawLine(0,(e),maxWidth,(e));
		offScreenGraphics.drawLine(0,(e - w),maxWidth,(e - w));
		offScreenGraphics.drawLine(0,(e - w*2),maxWidth,(e - w*2));
		offScreenGraphics.drawLine(0,(e - w*3),maxWidth,(e - w*3));
		offScreenGraphics.drawLine(0,(e - w*4),maxWidth,(e - w*4));
		//bass
		offScreenGraphics.drawLine(0,(e + w*2),maxWidth,(e + w*2));
		offScreenGraphics.drawLine(0,(e + w*3),maxWidth,(e + w*3));
		offScreenGraphics.drawLine(0,(e + w*4),maxWidth,(e + w*4));
		offScreenGraphics.drawLine(0,(e + w*5),maxWidth,(e + w*5));
		offScreenGraphics.drawLine(0,(e + w*6),maxWidth,(e + w*6));
		// upper treble
		offScreenGraphics.setColor(Color.lightGray);
		offScreenGraphics.drawLine(0,(e - w*7),maxWidth,(e - w*7));
		offScreenGraphics.drawLine(0,(e - w*8),maxWidth,(e - w*8));
		offScreenGraphics.drawLine(0,(e - w*9),maxWidth,(e - w*9));
		offScreenGraphics.drawLine(0,(e - w*10),maxWidth,(e - w*10));
		offScreenGraphics.drawLine(0,(e - w*11),maxWidth,(e - w*11));
		//lower bass
		offScreenGraphics.drawLine(0,(e + w*9),maxWidth,(e + w*9));
		offScreenGraphics.drawLine(0,(e + w*10),maxWidth,(e + w*10));
		offScreenGraphics.drawLine(0,(e + w*11),maxWidth,(e + w*11));
		offScreenGraphics.drawLine(0,(e + w*12),maxWidth,(e + w*12));
		offScreenGraphics.drawLine(0,(e + w*13),maxWidth,(e + w*13));		
		//Paint each phrase in turn
		Enumeration enum = sp.score.getPartList().elements();
		int i = 0;
		while(enum.hasMoreElements()){
			Part part = (Part) enum.nextElement();
			
			Enumeration enum2 = part.getPhraseList().elements();
			while(enum2.hasMoreElements()){
				Phrase phrase = (Phrase) enum2.nextElement();
				
				Enumeration enum3 = phrase.getNoteList().elements();
				oldX = (int) (Math.round (phrase.getStartTime() * beatWidth));
				// calc the phrase rectangles
				rectLeft = oldX;
				rectTop = 100000;
				rectRight = oldX;
				rectBot = 0;
				
				while(enum3.hasMoreElements()){
					Note aNote = (Note) enum3.nextElement();
					int currNote = (int)aNote.getPitch();
					if ((currNote < 127) && (currNote >= 0)) {
						// 10 - numb of octaves, 12 notes in an octave, 21 is the height of an octave, 156 is offset to put in position
						int y = (int)(((10 - currNote / 12) * 21 + (e-99)) - noteOffset[currNote % 12]);
						int x = (int)(Math.round(aNote.getDuration() * beatWidth)); //480 ppq note
						int xRV = (int)(Math.round(aNote.getRhythmValue() * beatWidth)); //480 ppq note
						// check if the width of the note is less than 1 so that it will be displayed
						if (x<1) x=1;
						if(y<rectTop) rectTop = y;
						if(y>rectBot) rectBot = y;//update values to phrase rectangle
						//set the colour change brightness for dynamic
						offScreenGraphics.setColor(Color.getHSBColor(theColours[i% maxColours][0], theColours[i% maxColours][1],(float)(0.7-(aNote.getDynamic()*0.002))));
						offScreenGraphics.fillRect(oldX,y-1,x,3);
						offScreenGraphics.drawRect(oldX,y-1,xRV,3);
						//add a sharp if required
						if ((currNote % 12) == 1 || (currNote % 12) == 3 || 
								(currNote % 12) == 6 || (currNote % 12) == 8 || 
								(currNote % 12) == 10) { 
							offScreenGraphics.drawString("#", oldX - 6, y + 5);
						}
					}
					oldX = oldX + (int) (Math.round(aNote.getRhythmValue() * beatWidth));
					rectRight = oldX - rectLeft; //update value for phrase rectangle
				}
				//offScreenGraphics.setColor(Color.lightGray);
				offScreenGraphics.setColor(Color.getHSBColor(theColours[i% maxColours][0], theColours[i% maxColours][1],(float)(0.95)));
				offScreenGraphics.drawRect(rectLeft, rectTop-2, rectRight+1, rectBot+(w/2) - rectTop +1);
			}
			i++; //increment the part index
		}
		//g.drawImage(offScreenImage, 0, 0, this);
		//offScreenImage.dispose();
		//offScreenGraphics.dispose();
	}
}
