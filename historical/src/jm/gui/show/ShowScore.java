/*

<This Java Class is part of the jMusic API version 1.0,Sun Feb 25 18:34:39  2001

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/


/* --------------------
* A jMusic tool which displays a score as a
* piano roll dispslay on Common Practice Notation staves.
* @author Andrew Brown 
 * @version 1.0,Sun Feb 25 18:43:12  2001
* ---------------------
*/
package jm.gui.show;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import jm.JMC;
import jm.music.data.*;
import jm.midi.*;
import java.awt.image.*; //

/**
* The tool displays a jMusic class as music notation. To use it write:
* new ShowScore(scoreName);
* Where scoreName is the jMusic Score object.
* Alternately:
* new ShowScore(scoreName, xpos, ypos);
* Where xpos and ypos are intergers specifying the topleft position of the window. 
* This is useful if you want to use DrawScore in conjunction with some other GUI interface 
* which is already positioned in the top left corner of the screen.
*
*/

public class ShowScore extends Frame implements WindowListener {
	//attributes
	private Panel pan;
	
	//--------------
	//constructors
	//--------------
	public ShowScore(Score score) {
		this(score, 0, 0);
	}
	
	public ShowScore(Score score, int xPos, int yPos) {
		super(score.getTitle());
				
		//register the closebox event
		this.addWindowListener(this);
		
		//add a scroll pane
		ShowPanel sp = new ShowPanel(this, score);
        sp.setSize(650,290);
		
		this.add(sp);

		//construct and display						
		this.pack();
		this.setLocation(xPos,yPos);							
		this.show();
	}
	
	//--------------
	// Class Methods
	//--------------
	
	// Deal with the window closebox
	public void windowClosing(WindowEvent we) {
		this.dispose(); //System.exit(0);
	}
	//other WindowListener interface methods
	//They do nothing but are required to be present
	public void windowActivated(WindowEvent we) {};
	public void windowClosed(WindowEvent we) {};
	public void windowDeactivated(WindowEvent we) {};
	public void windowIconified(WindowEvent we) {};
	public void windowDeiconified(WindowEvent we) {};
	public void windowOpened(WindowEvent we) {};
	
}
