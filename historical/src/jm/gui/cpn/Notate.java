/*

<This Java Class is part of the jMusic API version 1.0,Sun Feb 25 18:34:22  2001

Copyright (C) 2000 Andrew Brown and Andrew Sorensen

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/ 

// Some GPL changes for jMusic CPN Written by Al Christians 
// (achrist@easystreet.com).
// Contributed by Trillium Resources Corporation, Oregon's
// leading provider of unvarnished software.

package jm.gui.cpn; 

import java.awt.event.*;
import java.awt.*; 
import java.lang.Integer;
import java.io.*;
import javax.sound.midi.*;
import jmx.gui.cpn.JmMidiPlayer;
import jm.music.data.*;
import jm.midi.*;
import jm.util.Read;
import jm.util.Write;
import jm.gui.cpn.Stave;


public class Notate extends Frame implements 
                ActionListener, 
                WindowListener {
    private Score s;
    private Stave stave;
    private Phrase phrase;
    private int scrollHeight = 130, locationX = 0, locationY = 0;
    private Dialog keyDialog, timeDialog;
    private MenuItem keySig, open, openjm, play, stop, delete, clear, 
                    newStave, close, timeSig,saveJM, saveMidi, quit,
                    trebleStave, bassStave, pianoStave, grandStave,
                    
                    // Some menu options added
    				appendMidiFile, 
    				insertMidiFile,
    				setParameters,
    				playAll,
    				playMeasure,
    				repeatAll,
    				repeatMeasure,
    				stopPlay,
    				earTrain,
    				addNotes,
    				adjustTiming,
    				viewDetails,
    				viewZoom,
        barNumbers; 	
    				
    public boolean timeToStop;                 				
    				
    private Panel scoreBG;
    private ScrollPane scroll;

    private String lastFileName   = "*.mid";
    private String lastDirectory  = "";
    private String fileNameFilter = "*.mid";

    private boolean     zoomed;
    private Phrase      beforeZoom = new Phrase();
    private Phrase      afterZoom = new Phrase();

    public Notate() {
        this(new Phrase(), 0, 0);
        clearZoom();
    }
    
    public Notate(int locX, int locY) {
        this(new Phrase(), locX, locY);
        clearZoom();
    }
    
    public Notate(Phrase phr) {
        this(phr, 0, 0);
        clearZoom();
    }
    
    private void clearZoom() {
        zoomed      = false;
    }        

    public Notate(Phrase phrase, int locX, int locY) {
        super("CPN: "+ phrase.getTitle());
        clearZoom();
        this.phrase = phrase;
        locationX = locX;
        locationY = locY;
        addWindowListener(this);
        // menus
        MenuBar menus = new MenuBar();
        Menu edit  = new Menu("File", true);
        Menu features = new Menu("Tools", true);
        Menu player   = new Menu("Play", true);
        Menu view     = new Menu("View", true);
        //------
        newStave = new MenuItem("New", new MenuShortcut(110));
        newStave.addActionListener(this);
        edit.add( newStave);
        //------
        openjm = new MenuItem("Open jMusic file", new MenuShortcut(106));
        openjm.addActionListener(this);
        // edit.add(openjm);
        //------
        open = new MenuItem("Open MIDI file", new MenuShortcut(111));
        open.addActionListener(this);
        edit.add(open);
        //------
        close = new MenuItem("Close", new MenuShortcut(99));
        close.addActionListener(this);
        edit.add(close);
        //------
        edit.add("-");
        delete = new MenuItem("Delete last note");
        delete.addActionListener(this);
        edit.add(delete);
        //------
        clear = new MenuItem("Clear all notes" , new MenuShortcut(120));
        clear.addActionListener(this);
        edit.add(clear);
        //------
        edit.add("-");
        //------
        keySig = new MenuItem("Key Signature", new MenuShortcut(107));
        keySig.addActionListener(this);
        edit.add(keySig);
        //------
        timeSig = new MenuItem("Time Signature", new MenuShortcut(116));
        timeSig.addActionListener(this);
        edit.add(timeSig);
        //------
        edit.add("-");
        //------
        saveJM = new MenuItem("Save as a jm file...", new MenuShortcut(115));
        saveJM.addActionListener(this);
        // edit.add(saveJM);
        //------
        saveMidi = new MenuItem("Save as a MIDI file...", new MenuShortcut(109));
        saveMidi.addActionListener(this);
        edit.add(saveMidi);
        //------
        edit.add("-");
        //-Features Added by Al C -----

        //------        
        insertMidiFile = new MenuItem("Insert a MIDI file..." );
        insertMidiFile.addActionListener(this);
        features.add(insertMidiFile);

        appendMidiFile = new MenuItem("Append a MIDI file..." );
        appendMidiFile.addActionListener(this);
        features.add(appendMidiFile);

        setParameters = new MenuItem("Set Parameters..." );
        setParameters.addActionListener(this);
        features.add(setParameters);
        //------        
        //------        
        
        addNotes = new MenuItem("Add Notes by Letter" );
        addNotes.addActionListener(this);
        features.add(addNotes);
        
        //------        
        adjustTiming = new MenuItem("Quantize Timing");
        adjustTiming.addActionListener(this);
        features.add(adjustTiming);

        //------        
        playAll = new MenuItem("Play All" );
        playAll.addActionListener(this);
        player.add(playAll);
        repeatAll = new MenuItem("Repeat All" );
        repeatAll.addActionListener(this);
        player.add(repeatAll);

        //------        
        playMeasure = new MenuItem("Play Last Measure" );
        playMeasure.addActionListener(this);
        player.add(playMeasure);
        
        repeatMeasure = new MenuItem("Repeat Measure" );
        repeatMeasure.addActionListener(this);
        player.add(repeatMeasure);

        stopPlay = new MenuItem("Stop Play" );
        stopPlay.addActionListener(this);
        player.add(stopPlay);
        
        //earTrain = new MenuItem("Ear Train" );
        //earTrain.addActionListener(this);
        //player.add(earTrain);

        Menu staveMenu = new Menu( "Stave");
        edit.add(staveMenu);
        trebleStave = new MenuItem("Treble");
        trebleStave.addActionListener(this);
        staveMenu.add(trebleStave);
        bassStave = new MenuItem("Bass");
        bassStave.addActionListener(this);
        staveMenu.add(bassStave);
        pianoStave = new MenuItem("Piano");
        pianoStave.addActionListener(this);
        staveMenu.add(pianoStave);
        grandStave = new MenuItem("Grand");
        grandStave.addActionListener(this);
        staveMenu.add(grandStave);
        
        //------
        edit.add("-");
        
        //------
        quit = new MenuItem("Quit", new MenuShortcut(113));
        quit.addActionListener(this);
        edit.add(quit);

        //------
        viewDetails = new MenuItem("Note data as text", 
                        new MenuShortcut(117));
        viewDetails.addActionListener(this);
        view.add(viewDetails);
        
        viewZoom = new MenuItem("View phrase section", 
                        new MenuShortcut(121));
        viewZoom.addActionListener(this);
        view.add(viewZoom);
        barNumbers = new MenuItem("Bar Numbers");
        barNumbers.addActionListener(this);
        view.add(barNumbers);

        //-------
        menus.add(edit);
        menus.add(features);
        menus.add(player);
        menus.add(view);
        this.setMenuBar(menus);
        
        // components
        scroll = new ScrollPane(1);
       
        scroll.getHAdjustable().setUnitIncrement(10);
        scroll.getVAdjustable().setUnitIncrement(10);
                
        scoreBG = new Panel();
        scoreBG.setLayout(new GridLayout(1,1));
        
        stave = new PianoStave(phrase);
        stave.setBarNumbers(true);
        scoreBG.add(stave);
        
        scroll.setSize(800, stave.getSize().height + 20); //scroll.getHScrollbarHeight()); //scrollHeight);
        
        scroll.add(scoreBG);
        
        this.add(scroll);
        this.pack();
        this.setLocation(locationX, locationY);
        this.show();
    }

    private void makeTrebleStave() {
            // store current phrase
            phrase = stave.getPhrase().copy();
            int tempKey = stave.getKeySignature();
            double tempTime = stave.getMetre();
            boolean tempBarNumbers = stave.getBarNumbers();
            // create new stave panel
            stave = new TrebleStave();
            scoreBG.removeAll();
            scoreBG.add(stave);
            scroll.setSize(800, stave.getSize().height + 20);
            this.pack();
            // replace stave
            stave.setPhrase(phrase);
            stave.setKeySignature(tempKey);
            stave.setMetre(tempTime);
            stave.setBarNumbers(tempBarNumbers);
    }        

    private void makeBassStave() {
        
            // store current phrase
            phrase = stave.getPhrase().copy();
            int tempKey = stave.getKeySignature();
            double tempTime = stave.getMetre();
            boolean tempBarNumbers = stave.getBarNumbers();
            // create new stave panel
            stave = new BassStave();
            scoreBG.removeAll();
            scoreBG.add(stave);
            scroll.setSize(800, stave.getSize().height + 20);
            this.pack();
            // replace stave
            stave.setPhrase(phrase);
            stave.setKeySignature(tempKey);
            stave.setMetre(tempTime);
            stave.setBarNumbers(tempBarNumbers);
    }
    
    private void makePianoStave() {
            // store current phrase
            phrase = stave.getPhrase().copy();
            int tempKey = stave.getKeySignature();
            double tempTime = stave.getMetre();
            boolean tempBarNumbers = stave.getBarNumbers();
            // create new stave panel
            stave = new PianoStave();
            scoreBG.removeAll();
            scoreBG.add(stave);
            scroll.setSize(800, stave.getSize().height + 20);
            this.pack();
            // replace stave
            stave.setPhrase(phrase);
            stave.setKeySignature(tempKey);
            stave.setMetre(tempTime);
            stave.setBarNumbers(tempBarNumbers);
    }        
        
    private void makeGrandStave() {
        
            // store current phrase
            phrase = stave.getPhrase().copy();
            int tempKey = stave.getKeySignature();
            double tempTime = stave.getMetre();
            boolean tempBarNumbers = stave.getBarNumbers();
            // create new stave panel
            stave = new GrandStave();
            scoreBG.removeAll();
            scoreBG.add(stave);
            scroll.setSize(800, stave.getSize().height + 20);
            this.pack();
            // replace stave
            stave.setPhrase(phrase);
            stave.setKeySignature(tempKey);
            stave.setMetre(tempTime);
            stave.setBarNumbers(tempBarNumbers);
    }        

    class PlayRepeater extends Thread {
        
        JmMidiPlayer midiPlayer;
        Notate       n; 
        
        public PlayRepeater(         
                String       str,
                Notate       nParm,                
                JmMidiPlayer midiPlayerParm ) {
            super(str);           
            n         = nParm;         
            midiPlayer = midiPlayerParm;                    
        }                    
        
        public void run() {
            do {
                midiPlayer.play();                                                        
            } while(!n.timeToStop);                
        }
    }        
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == close) dispose();
        else
        if(e.getSource() == newStave) new Notate();
        else
        if(e.getSource() == open) openMidi();
        else
        if(e.getSource() == openjm) openJM();
        else
        if(e.getSource() == keySig) {
            if(stave.getKeySignature() == 0) {
                stave.setKeySignature(2);
                stave.repaint();
            } else {
                stave.setKeySignature(0);
                stave.repaint();
            }
        }
        else
        if(e.getSource() == timeSig) {
            if(stave.getMetre() == 0.0) {
                stave.setMetre(4.0);
                stave.repaint();
            } else {
                stave.setMetre(0.0);
                stave.repaint();
            }
        }
        else
        if(e.getSource() == saveJM) saveJM();
        else
        if(e.getSource() == saveMidi) saveMidi();
        else
        if(e.getSource() == quit) System.exit(0);
        else
        if(e.getSource() == delete) stave.deleteLastNote();
        else
        if(e.getSource() == clear) {
            stave.getPhrase().empty();
            stave.repaint();
        }
        else
        if(e.getSource() == trebleStave) {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            makeTrebleStave();
            stave.repaint();
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        else
        if(e.getSource() == bassStave) {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            makeBassStave();
            stave.repaint();
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        else
        if(e.getSource() == pianoStave) {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            makePianoStave();
            stave.repaint();
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        else
        if(e.getSource() == grandStave) {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            makeGrandStave();
            stave.repaint();
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        else
        if(e.getSource() == insertMidiFile) {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            Phrase savePhrase = phrase;
        	phrase = readMidiPhrase();
            phrase.addNoteList(
                savePhrase.getNoteList(),
                true
            );        
            stave.setPhrase(phrase);
            stave.repaint();
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        else
        if(e.getSource() == appendMidiFile) {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            phrase.addNoteList(
                readMidiPhrase().getNoteList(),
                true
            );        
			stave.setPhrase(phrase);
			stave.repaint();
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        else
        if(e.getSource() == setParameters) {
			ParmScreen parmScreen 
					= new ParmScreen(this);
			double oldTempo = stave.getPhrase().getTempo(); 					
			parmScreen.getParms(
							stave.getPhrase(),
					 		15, 15 );				
			stave.repaint();
        }
        else
        if( e.getSource() == playAll )  {
            try {
			    JmMidiPlayer midiPlayer = new JmMidiPlayer();
			    Score 	s = new Score();
			    Part  	p = new Part();
			    s.addPart(p);
			    p.addPhrase(stave.getPhrase());	
			    Write.midi(s, midiPlayer);
                midiPlayer.play();
                midiPlayer.close();									       	
            }
            catch ( MidiUnavailableException ex ) {
                System.out.println("Midi Not Available");                    
            }   
		}        	
        else
        if  (e.getSource() == repeatAll) {
            try {
                JmMidiPlayer midiPlayer = new JmMidiPlayer();
                Score   s = new Score();
                Part    p = new Part();
                s.addPart(p);
                p.addPhrase(stave.getPhrase()); 
                timeToStop = false;
                Write.midi(s, midiPlayer);
                PlayRepeater pr =
                    new PlayRepeater(
                            "A", this, midiPlayer);
                pr.start();                           
                midiPlayer.close();                                         
            }
            catch ( MidiUnavailableException ex ) {
                System.out.println("Midi Not Available");                    
            }   
        }           
        else
        if (e.getSource() == repeatMeasure) {
            try {
                timeToStop = false;
                JmMidiPlayer midiPlayer = new JmMidiPlayer();
                Score   s = new Score();
                Part    p = new Part();
                s.addPart(p);
                p.addPhrase(getLastMeasure(stave.getPhrase()));                 
                Write.midi(s, midiPlayer);
                PlayRepeater pr =
                    new PlayRepeater(
                           "A", this, midiPlayer);
                pr.start();                           
                midiPlayer.close();                                         
            }
            catch ( MidiUnavailableException ex ) {
                System.out.println("Midi Not Available" );                    
            }                                
        }           
        else
        if(e.getSource() == playMeasure )  {
            try {
                JmMidiPlayer midiPlayer = new JmMidiPlayer();
                Score   s = new Score();
                Part    p = new Part();
                s.addPart(p);
                p.addPhrase(getLastMeasure(stave.getPhrase()));                 
                Write.midi(s, midiPlayer);
                midiPlayer.play();
                midiPlayer.close();                                         
            }
            catch ( MidiUnavailableException ex ) {
                System.out.println("Midi Not Available");                    
            }                                
        }           
        else
        if(e.getSource() == addNotes) {
            LetterNotesEditor notesScreen 
                    = new LetterNotesEditor(this);
            notesScreen.getNotes(stave);
            stave.repaint();
        }
        else
        if(e.getSource() == adjustTiming) {
            adjustTimeValues(stave.getPhrase());
            stave.repaint();
        }
        else
        if(e.getSource() == stopPlay) {
            timeToStop = true;
        }
        else
        if(e.getSource() == viewDetails) {
            PhraseViewer phraseViewer 
                    = new PhraseViewer(this);
            phraseViewer.showPhrase(
                            stave,
                            stave.getPhrase(),
                            15, 15 );               
        }
        else
        if(e.getSource() == viewZoom) {
            if (!zoomed) {
                CpnZoomScreen zoomSelector 
                        = new CpnZoomScreen(this);
                beforeZoom = stave.getPhrase().copy();                        
                afterZoom  = stave.getPhrase().copy();                        
                beforeZoom.empty();
                afterZoom.empty();
                zoomSelector.zoomIn(
                            beforeZoom,
                            stave.getPhrase(),
                            afterZoom);               
                if (beforeZoom.size() +
                    afterZoom.size() > 0 ) {
                    zoomed = true;
                    viewZoom.setLabel("View complete phrase");                        
                    stave.repaint();
                }                                                    
            }
            else {
                CpnZoomScreen.zoomOut(
                            beforeZoom,
                            stave.getPhrase(),
                            afterZoom );               
                zoomed = false;
                viewZoom.setLabel("View phrase section");                        
                stave.repaint();
            }                                                            
        }
        else if (e.getSource() == barNumbers) {
            stave.setBarNumbers(!stave.getBarNumbers());
            stave.repaint();
        }
        //else if (e.getSource() == earTrain ) {
        //    (new EarTrainer().show());
        //}            
    }
    /**
     * Dialog to import a MIDI file
     */
     
     public void openMidi() {
        FileDialog loadMidi = new FileDialog(this, "Select a MIDI file.", FileDialog.LOAD);
        loadMidi.setDirectory( lastDirectory );
        loadMidi.setFile( lastFileName );
        loadMidi.show();
        String fileName = loadMidi.getFile();
        if (fileName != null) {
            lastFileName = fileName;
            lastDirectory = loadMidi.getDirectory();                        
            Phrase phr = new Phrase();
            Read.midi(phr, lastDirectory + fileName);  
        
            try {            
                StavePhraseProperties props 
                = new StavePhraseProperties( 
                        lastDirectory + fileName );
                props.updatePhrase(phr);             
                if (props.isTrebleStave()) {
                    makeTrebleStave();                
                }                
                else if (props.isGrandStave()) {
                    makeGrandStave();                
                }                
                else if (props.isPianoStave()) {
                    makePianoStave();                
                }                
                else if (props.isBassStave()) {
                    makeBassStave();                
                }                
                else {
                    makeGrandStave();                
                }   
                props.updateStave(stave);             
            }
            catch ( FileNotFoundException e) {
                System.out.println(
                    "No Properties File for " +
                    lastDirectory + fileName
                    + " " + e.getMessage() );  
            }
            catch ( IOException e) {
                System.out.println(
                    "Properties Error in File " +
                    lastDirectory + fileName
                    + " " + e.getMessage() );  
            }
                            
            s = new Score();
			Part p = new Part();
			adjustTimeValues(phr);
			p.addPhrase(phr);
			s.addPart(p);
			phrase = phr;
            stave.setPhrase(s.getPart(0).getPhrase(0)); 
            stave.repaint();    
        }
    }
    
    /**
     * Dialog to import a jm file
     */
     
     public void openJM() {
        FileDialog loadjm = new FileDialog(this, "Select a jm phrase file.", FileDialog.LOAD);
        loadjm.show();
        String fileName = loadjm.getFile();
        if (fileName != null) {
            Score s = new Score();
				s.addPart(new Part());
				s.getPart(0).addPhrase(new Phrase());
            Read.jm(s.getPart(0).getPhrase(0), loadjm.getDirectory() + fileName);
            phrase = s.getPart(0).getPhrase(0);
            stave.setPhrase(phrase);
            stave.repaint();
        }
    }

    
    /**
     * Dialog to save phrase a s MIDI file.
     */
    public void saveMidi() {
        if(stave.getPhrase().size() != 0) {
            FileDialog fd = new FileDialog(this, "Save as a MIDI file...",FileDialog.SAVE);
		    fd.show();
		        	
    	    //write a MIDI file to disk
    	    if ( fd.getFile() != null) {
    	        Write.midi(stave.getPhrase(),fd.getDirectory()+fd.getFile());
                StavePhraseProperties props =
                    new StavePhraseProperties(
                            stave, stave.getPhrase());
                try {                            
                    props.writeToFile(                                         
                        fd.getDirectory()+fd.getFile());   
                }
                catch ( Throwable e) {
                    System.out.println(
                        "Error Saving Properties " +
                        e.getMessage() );                                                
                }                                            
    	    }
    	} else {System.out.println("Input notes before saving.");}
    }
    
    /**
     * Dialog to save phrase as a jMusic file.
     */
    public void saveJM() {
        if(stave.getPhrase().size() != 0) {
            FileDialog fd = new FileDialog(this, "Save as a jm file...",FileDialog.SAVE);
		    fd.show();
		        	
    	    //write a MIDI file to disk
    	    if ( fd.getFile() != null) {
    	        Score s = new Score();
    	        Part p = new Part();
    	        p.addPhrase(stave.getPhrase());
    	        s.addPart(p);
    	        Write.jm(stave.getPhrase(),
                            fd.getDirectory()+fd.getFile());
    	    }
    	} 
    }

    public Phrase readMidiPhrase() {
        FileDialog loadMidi = new FileDialog(this, "Select a MIDI file.", FileDialog.LOAD);
        loadMidi.show();
        String fileName = loadMidi.getFile();
        Phrase phr = new Phrase(0.0);
        Score scr = new Score();
        if (fileName != null) {
            Read.midi(scr, loadMidi.getDirectory() + fileName); 
        }
        scr.clean();
        if (scr.size() > 0 && scr.getPart(0).size() > 0) phr = scr.getPart(0).getPhrase(0);
        //System.out.println("Size = " + phr.size());
        return phr;
    }
    
    private static Phrase getLastMeasure( Phrase  phrase ) {
        double beats;
        beats = phrase.getNumerator();
        int    noteIndex;
        noteIndex = phrase.size();             
        while ((noteIndex > 0) && (beats > 0.01)){                
            --noteIndex;                
            beats = beats - phrase.getNote(noteIndex)
                                   .getRhythmValue();
        }
        Phrase answer;
        answer = new Phrase();                                   
        while(noteIndex < phrase.size()) {
            answer.add(phrase.getNote(noteIndex));
            ++noteIndex;                                
        }
        return answer;            
    }
    
    private static double getRhythmAdjustment(
                        double  beats,
                        double  beatIncrement ) {
        double increments;
        increments = beats/beatIncrement;
        double tolerance;                                   
        tolerance = 0.00001;
        double answer;  
        answer = 0.0;
        double n;
        n = Math.floor(increments);
        while(( Math.floor(increments+tolerance) > n ) 
                && (tolerance > 0.00000000000001)) {
            answer = tolerance;
            tolerance = tolerance / 2;
        }                        
        return answer * beatIncrement;                            
    }                            
    
    private static void adjustTimeValues(Phrase phr) {
        int i;
        double t, dt, st;
        for( i = 0; i < phr.size(); ++i) {        
            t  = phr.getNote(i).getRhythmValue();
            dt = getRhythmAdjustment( t, 1.0 / 256.0 ); 
            phr.getNote(i).setRhythmValue(t+dt);
        }
        
        st = 0.0;
        for( i = 0; i < phr.size(); ++i) {        
            t  = phr.getNote(i).getRhythmValue();
            st = st + t;
            dt = getRhythmAdjustment( st, 1.0 ); 
            phr.getNote(i).setRhythmValue(t+dt);
            st = st + dt;
        }
    }        
    
    /**
     * Invoked when a window has been opened.
     */
    public void windowOpened(WindowEvent e) {
    }

    /**
     * Invoked when a window is in the process of being closed.
     * The close operation can be overridden at this point.
     */
    public void windowClosing(WindowEvent e) {
        if(e.getSource() == this) dispose();
        if(e.getSource() == keyDialog) keyDialog.dispose();
        if(e.getSource() == timeDialog) timeDialog.dispose();
    }

    

    /**
     * Invoked when a window has been closed.
     */
    public void windowClosed(WindowEvent e) {
    }

    /**
     * Invoked when a window is iconified.
     */
    public void windowIconified(WindowEvent e) {
    }

    /**
     * Invoked when a window is de-iconified.
     */
    public void windowDeiconified(WindowEvent e) {
    }

    /**
     * Invoked when a window is activated.
     */
    public void windowActivated(WindowEvent e) {
    }

    /**
     * Invoked when a window is de-activated.
     */
    public void windowDeactivated(WindowEvent e) {
    }
}
