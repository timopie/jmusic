import jm.music.data.*;
import jm.JMC;
import jm.util.*;
import jm.audio.*;

public class LFOFilterMusic implements JMC {
	
	public static void main(String[] args) {
		new LFOFilterMusic();
	}
	
	public LFOFilterMusic() {
		Phrase phrase = new Phrase();
		Score score = new Score( new Part(phrase));
		
		int[] intervalArray = {0,  7, 12, 7};
		for (int r=0; r<2; r++) {
                    for(int i = 0; i < 8; i++) {
                            for(int j = 0; j < intervalArray.length; j++) {
                                    Note n = new Note(C2 + intervalArray[j], DEMI_SEMI_QUAVER);
                                    n.setPan(Math.random() * 0.4 + 0.3);
                                    phrase.addNote(n);
                            }
                    }

                    for(int i = 0; i < 8; i++) {
                            for(int j = 0; j < intervalArray.length; j++) {
                                    Note n = new Note(E2 + intervalArray[j], DEMI_SEMI_QUAVER);
                                    phrase.addNote(n);
                            }
                    }
                }
		Instrument inst = new LFOFilteredSquareInst(44100);
		
		Write.au(score, "LFOFilterMusic.au", inst);
	}
}

