//==========================================================
//File:                 SimpleGUI2.java
//Function:             Demonstartes automatic phrase arranging 
//						in both time and pitch dimensions.
//Author:               Andrew Brown
//Environment           Java 1.1.7 with jMusic extensions
//============================================================
import jm.JMC;
import jm.music.data.*;
import jm.midi.*;


public class ShiftUpAndDown implements JMC{

	public static void main(String[] args) {
		Score score = new Score("JMDemo - SimpleUpAndDown");
		Part flutePart = new Part("Flute",0,FLUTE);
		Phrase phr = new Phrase(0.0); 
		int[] riff = {60,67,65,67,62,67,62,59};
		
		// create an initial phrase
		for(int i=0;i<20;i++){
			for(int j=0;j<riff.length;j++) {
				Note note = new Note(riff[j], SQ, (int)(Math.random()*50+60));
				phr.addNote(note);
			}
		}
		//add the phrase
		flutePart.addPhrase(phr);
		score.addPart(flutePart);
		
		//arrange at random
		int times = (int)(Math.random()*10)+1;
		for(int i=1;i<times;i++){
			// make a copy of the phrase
			Phrase temp = phr.copy();
			// adjust the start time of the copy
			temp.setStartTime((double)(int)(Math.random()*16));
			//shift the phrase in octaves at random
			temp.transpose(((int)(Math.random()*5)-2)*12);
			//add the copied phrase to the  part then score
			Part p = new Part("no name "+i,i,i);
			p.addPhrase(temp);
			score.addPart(p);
		}
		
		//write a MIDI file to disk
		score.writeMIDI("ShiftUpAndDown.mid");
	}
}
