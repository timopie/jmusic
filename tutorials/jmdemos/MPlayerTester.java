/*
 * JMMSTester.java
 *
 * Created on 22 March 2004, 15:20
 */

import jmms.*;
import jm.music.data.*;
/**
 *
 * @author  Rene Wooller
 */
public class MPlayerTester extends MPlayer {
    
    /** Creates a new instance of JMMSTester */
    public MPlayerTester() {
    }
    
    public Score nextScore() {
        System.out.println("making score");
        return new Score(new Part(new Phrase(new Note(60, 0.25, 120))));
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MPlayerTester jt = new MPlayerTester();
        jt.go();
        
    }
    
}
