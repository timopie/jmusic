import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class makes an  arpeggio with a varied rhythm.
* @author Andrew Brown
*/

public class Arpeggio12 implements JMC {
	
	public static void main(String[] args) {
		 new Arpeggio12();
    }
    
    public Arpeggio12() {
        int pitch = C2;
        int[] pitches = {pitch, pitch + 4, pitch + 7, pitch + 10};
        Phrase arpPhrase = new Phrase();
        int octaves = 2;
        int repeats = 4;
        
        // an arrahy of rhythm values
        double[] rhythmValues = {SEMI_QUAVER, SEMI_QUAVER, DOTTED_SEMI_QUAVER, DEMI_SEMI_QUAVER};
         
        // create a repeat loop
        for(int r = 0; r < repeats; r++) {
            // create an octave loop for up
            for(int oct = 0; oct < octaves; oct++) {
		        // up notes
		        for(int i = 0; i < pitches.length; i++) {
		            Note n = new Note(pitches[i] + (oct * 12), rhythmValues[i]);
		            arpPhrase.addNote(n);
		        }
		    }
		    // create an octave loop for down
            for(int oct = octaves -1; oct >= 0; oct--) {
		        // down notes
		        int rvIndex = 0;
		        for(int i = pitches.length - 1; i >= 0; i--) {
		            Note n = new Note(pitches[i] + (oct * 12), rhythmValues[rvIndex]);
		            arpPhrase.addNote(n);
		            rvIndex++;
		        }
	        }
        }
        
        // put the phrase in a part
        Part p = new Part("Arp", SYNTH_BASS, 1);
        p.addPhrase(arpPhrase);
        
        // save it as a file
        Write.midi(p, "Arpeggio12.mid");
	}
}
