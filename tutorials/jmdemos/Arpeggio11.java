import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class makes an up and down arpeggio over several octaves.
* @author Andrew Brown
*/

public class Arpeggio11 implements JMC {
	
	public static void main(String[] args) {
		 new Arpeggio11();
    }
    
    public Arpeggio11() {
        int pitch = C2;
        int[] pitches = {pitch, pitch + 4, pitch + 7, pitch + 10};
        Phrase arpPhrase = new Phrase();
        int octaves = 2;
         
        // create a repeat loop
        for(int r = 0; r < 3; r++) {
            // create an octave loop for up
            for(int oct = 0; oct < octaves; oct++) {
		        // up notes
		        for(int i = 0; i < pitches.length; i++) {
		            Note n = new Note(pitches[i] + (oct * 12), SEMI_QUAVER);
		            arpPhrase.addNote(n);
		        }
		    }
		    // create an octave loop for down
            for(int oct = octaves -1; oct >= 0; oct--) {
		        // down notes
		        for(int i = pitches.length - 1; i >= 0; i--) {
		            Note n = new Note(pitches[i] + (oct * 12), SEMI_QUAVER);
		            arpPhrase.addNote(n);
		        }
	        }
        }
        
        // put the phrase in a part
        Part p = new Part("Arp", SYNTH_BASS, 1);
        p.addPhrase(arpPhrase);
        
        // save it as a file
        Write.midi(p, "Arpeggio11.mid");
	}
}
