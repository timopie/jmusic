import jm.music.data.*;
import jm.JMC;
import jm.music.tools.*;
import jm.util.*;
import jmqt.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
* A simple game of 'pong' that demonstrates how jMusic can be used
* to add music to real time game play.
* jMusic API http://jmusic.ci.edu.au/ or at http://sourceforge.net
*
* This class relies on the QuickTime Java API from Apple
* so make sure it is installed (part of a customised QT install).
* The class uses the QuickTime musical instruments (General MIDI)
* for music playback. QuickTime does allow for MIDI data to be
* directed to an external MIDI device by setting the
* appropriate QuickTime Music preferences. If you have an dedicated
* MIDI synthesizer sending MIDI to it will probably give better timbral results.
*
* Because the music is generated at run time each game will always
* have unique music. (You can make it deterministic if you wish). In
* this demo the music evolves each time the ball hits the paddle
* which shows how the music can be interactive with game play.
*
* This class rlies upon the NewGameMusic class to run properly.
*
* @author Andrew Brown 2001
*/

public class BasicGame8 extends JPanel implements JMC, KeyListener {
	//----------------
    // class variables
    //----------------
    private int xLocation = 10;
	private int yLocation = 20;
    private int animationIncrement = 4;
    private int animationSleepTime = 20;
    private Thread animationThread;
    private int windowWidth = 600;
    private int windowHeight = 400;
    private int ballSize = 30;
    private boolean yDirection = true; // down
    private boolean xDirection = true; // right
    private Color ballColor = Color.black;
    protected Score s;
    protected QTCycle qtc;
    protected QTUtil qtu;
    private int paddleWidth = 60;
    private int paddleHeight = 15;
    private int paddleLocation = windowWidth / 2 - paddleWidth;
    private int highScore = 0;
    private boolean paddleHit = false;
    private NewGameMusic mClass = new NewGameMusic();
    
    //------------------
    // main - start here
    //------------------
    public static void main(String[] args) {
        new BasicGame8();
    }
    
    //------------
    // constructor
    //------------
    public BasicGame8() {
        setUpMusic();
        makeGraphics();
    }
	
    //--------
    // methods
    //--------
    protected void setUpMusic() {
        // create a new score
        s = mClass.makeMusic();
        // create a QTCycle object
        qtc = new QTCycle(s);
        // begin cycling
        qtc.startPlayback();
        // retrieve the QTUtil used by QTCycle
        // to be used to send notes to coincide
        // with wall and bat collisions
        qtu = qtc.getQTUtil();
    }
    
    
    private void makeGraphics() {
        JFrame window = new JFrame();
        window.addKeyListener(this);
        window.setSize(windowWidth, windowHeight);
        window.setContentPane(this);
        window.setVisible(true);
        // animation thread inner class
        animationThread = new Thread ( new Runnable() {
            public void run() {
                while(true) {
                    animate();
                    try {
                            animationThread.sleep(animationSleepTime);
                    } catch (InterruptedException e) {}
                }
            }
        });
        animationThread.start();
    }
	
    private void animate() {
        // dectect if we've hit anything
        // hit bottom?
        if (yLocation + ballSize >= this.getHeight()) {
            paddleHit = true; // prevent rebounds scoring
            yDirection = false; // go up
            qtu.playOneNote(new Note(47, 0.1, 127), 9); // make a noise
            System.out.println(":(  Your Score is " + --highScore);
        }
        // hit paddle
        if (!paddleHit && yLocation + ballSize >= this.getHeight() - paddleHeight &&
            xLocation >= paddleLocation - ballSize*0.75 &&
            xLocation <= paddleLocation + paddleWidth - ballSize*0.25) {
            // go up
                yDirection = false; 
                // make a noise
                qtu.playOneNote(new Note(60, 0.1, 127), 9); 
                ballColor = new Color((float)Math.random(), (float)Math.random(), (float)Math.random());
                // create a new score
                s = mClass.makeMusic();
                // update the QTCycle with the revised score
                qtc.setScore(s);
                if (animationSleepTime > 1) animationSleepTime--; // speed up ball
                System.out.println(":)  Your Score is " + ++highScore);
                paddleHit = true;
        }
        // right side
        if (xLocation >= this.getWidth() - ballSize) {
            paddleHit = false;
            xDirection = false; // go left
            qtu.playOneNote(new Note(47, 0.1, 70), 9); // make a noise
        }
        // top
        if (yLocation <= 0) {
            paddleHit = false;
			yDirection = true;
            qtu.playOneNote(new Note(47, 0.1, 70), 9); // make a noise
        }
        // left side
        if (xLocation <= 0) {
            paddleHit = false;
            xDirection = true;
            qtu.playOneNote(new Note(47, 0.1, 70), 9); // make a noise
        }
        // increment y direction of ball
        if (yDirection) {
                yLocation += animationIncrement + (int)(Math.random() * 2);
        } else yLocation -= animationIncrement;
        // increment x direction of ball
        if (xDirection) {
                xLocation  += animationIncrement;
        } else xLocation -= animationIncrement;
        repaint();
    }
	
    //---------------------------------------
    // over ride the component's paint method
    //---------------------------------------
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(ballColor);
        g.fillOval(xLocation, yLocation, ballSize, ballSize);
        // paddle
        g.setColor(Color.darkGray);
        g.fillRect(paddleLocation, this.getHeight() - paddleHeight, paddleWidth, paddleHeight - 5);
    }
    
    //-------------------------------
    // key listener interface methods
    //-------------------------------
    public void keyPressed(KeyEvent e) {
        // left arrow key
        if (e.getKeyCode() == 37 && paddleLocation > 0) paddleLocation -= 25;
        // right arrow key
        if (e.getKeyCode() == 39 && paddleLocation < (this.getWidth() - paddleWidth)) paddleLocation += 25;
    }
    
    public void keyTyped(KeyEvent e) {}
     
    public void keyReleased(KeyEvent e) {}
}