/*
 * DynamicLPFMusic.java
 *
 * Created on September 11, 2001, 8:20 PM
 */

/**
 *
 * @author  Andrew Brown
 * @version 
 */
import jm.music.data.*;
import jm.JMC;
import jm.util.*;
import jm.audio.*;

public class DynamicLPFMusic implements JMC{
    public static void main(String[] args) {
        new DynamicLPFMusic();
    }
    
    /** Creates new DynamicLPFMusic */
    public DynamicLPFMusic() {
        CPhrase ravel = new CPhrase(0.0);
        Phrase pedal = new Phrase(0.0);
        int[] rootPitches = {D4, EF4, F4, G4, EF4, DF4, C4, B3, C4, C4};
        int currentRoot = 0;
        double[] rhythmValues;
        for(int k=0; k < 4; k++) {
            for (int j = 0; j < 2; j++ ) {
                // choose rhythm
                if (Math.random() < 0.5) {
                    double[] temp = {2.0, 1.0};
                    rhythmValues = temp;
                } else {
                    double[] temp = {1.0, 1.0, 1.0};
                    rhythmValues = temp;
                }
                // make chords
                for(int i=0; i<rhythmValues.length; i++) {
                    int root = Math.abs(currentRoot) % rootPitches.length;
                    System.out.println("Root = " + root);
                    int[] chordPitches = {rootPitches[root], rootPitches[root] + 4, rootPitches[root] + 7};
                    ravel.addChord(chordPitches, rhythmValues[i], (int)(Math.random() * 120 + 5));
                    // choose next root note
                    if (Math.random() < 0.5) {
                        currentRoot++;
                    } else currentRoot--;
                }
            }
            // add pedal note
            pedal.addNote(new Note(REST, 1.0));
            pedal.addNote(new Note(G2, 5.0));
        }
        
        
        Score score = new Score("A tribute to Ravel's Minuet from 'Le Tombeau de Couperin'");
        Part piano = new Part();
        score.addPart(piano);
        piano.addPhrase(pedal);
        piano.addCPhrase(ravel);
        
        View.print(score);
        
        // audio
        Instrument inst = new DynamicFilterInst(44100);	
	Write.au(score, "DynamicLPF.au", inst);
    }

}
