import jm.JMC;
import jm.music.data.*;
import jm.audio.*;
import jm.util.*;

public final class NoiseAMTest implements JMC{

    public static void main(String[] args){
        Score score = new Score (new Part(new Phrase(new Note(C4, 4.0))));
        int sampleRate = 44100;
        Instrument inst = new NoiseAMInst(sampleRate);
        Write.au(score, "NoiseAMTest.au",inst);
    }
}