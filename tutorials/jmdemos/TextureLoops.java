import jm.music.data.*;
import jm.music.tools.*;
import jm.JMC;
import jm.audio.*;
import jm.util.*;

public class TextureLoops implements JMC {
	public static void main(String[] args) {
		new TextureLoops();
	}
	
	public TextureLoops() {
		Part part = new Part();
		Score score = new Score(part);
		
		// increase the texture over time
		for(int i = 0; i < 5; i++) {
			double currStageET = score.getEndTime();
			for (int j = 0; j < i+1; j++) {
				part.addPhrase(OnePhrase(currStageET, i));
			}
		} 
		
		// decrease the texture again
		for(int i = 4; i >= 0; i--) {
			double currStageET = score.getEndTime();
			for (int j = 0; j < i+1; j++) {
				part.addPhrase(OnePhrase(currStageET, i));
			}
		} 
		
		// add accents
		Mod.accents(score, 2.0, new double[] {0.0}, 40);
		
		// see the score
		View.show(score);
            
            // render as an audio file
		Instrument inst = new ResSawInst(11000);	//E	
		Write.au(score, "TextureLoops.au", inst);
	}
	
	private Phrase OnePhrase(double startTime, int densityFactor) {
		double currentST = startTime - (int)(Math.random() * 12) * 0.25;
		if (currentST < 0.0) currentST = 0.0;
		Phrase phrase = new Phrase(currentST);
		double panPos = Math.random();
		// get a pitch
		int pitch = C4 + (int)(Math.random() * 12 * densityFactor - 6 * densityFactor);
		Note n = new Note(pitch, 1.0);
            while (!n.isScale(PENTATONIC_SCALE)) {
                  n = new Note(pitch, 1.0);
                  pitch = C2 + (int)(Math.random() * 36);
            }
		for(int i =0; i < 24 ; i++) {
            	Note note = new Note(pitch, 
                  			DEMI_SEMI_QUAVER * (int)(Math.random() * (1.5 * densityFactor) + 1), 
                  			(int)(Math.random() * 80) + 47);
                 	note.setPan(panPos);
            	phrase.addNote(note);
		}
		return phrase;
	}
}
			

