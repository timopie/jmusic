/**
 * Sound-mass compositional technique that shows off the use of an envelope
 * to control the filter cutoff over the duration of a note.
 * @author  Andrew Brown
 */
import jm.music.data.*;
import jm.JMC;
import jm.util.*;
import jm.audio.*;

public class EnvelopedFilterMusic implements JMC{
    public static void main(String[] args) {
        new EnvelopedFilterMusic();
    }
    
    /** Creates new EnvelopedFilterMusic */
    public EnvelopedFilterMusic() {
        Part soundMass = new Part();
        CPhrase chords = new CPhrase();
        for (int times = 0; times < 4; times++) {
            // sect 1
            for (int i=0; i <8; i++) {
                // pitches
                int[] pitches = new int[7];
                for (int p=0; p<7; p++) {
                    pitches[p] = (int)(Math.random() * 24 + 50);
                }
                chords.addChord(pitches, DEMI_SEMI_QUAVER);
            }
            // sect 2
            int[] pitches = new int[7];
            for (int p=0; p<7; p++) {
                 pitches[p] = (int)(Math.random() * 36 + 45);
            }
            chords.addChord(pitches, JMC.SEMIBREVE, (int)(Math.random() * 50 + 70));
            
        }
        
        soundMass.addCPhrase(chords);
        Score score = new Score(soundMass);
        
        // apply panning to each phrase
        for (int i = 0; i<soundMass.size(); i++) {
            soundMass.getPhrase(i).setPan(Math.random());
        }
        
        // change duration of alll notes a bit
        for (int i = 0; i<soundMass.size(); i++) {
            Phrase temp = soundMass.getPhrase(i);
            for(int j=0; j < temp.size(); j++) {
            	Note tempNote = temp.getNote(j);
            	tempNote.setDuration(tempNote.getRhythmValue() * (Math.random() * 0.6 + 0.6));
            }
        }
        
        // audio
        Instrument inst = new LPFilterEnvInst(44100);	
        Write.au(score, "LPFilterEnvInst.au", inst);
    }
}
        
