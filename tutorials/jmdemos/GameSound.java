import jm.music.data.*;
import jm.music.tools.*;
import jm.JMC;
import jm.audio.*;
import jm.util.*;

// this class play old style computer game music
// wich sounds convincing given the simplistic timbres
// charracteristic of games harware of the time.

public class GameSound implements JMC {
	
	public static void main(String[] args) {
		new GameSound();
	}
	
	public GameSound() {
		// make the jmusic score
		Score score = new Score("Game Music", 140);
		Part bass = new Part("Bass part", 0);
		Part drums = new Part("Drum part", 1);
		Part harmony = new Part("Harmony part", 2);
		Part melody = new Part("Melody part", 3);
		
		int bars = 16;
		bass.addPhrase(makeBassPhrase(bars));
		drums.addPhrase(makeDrumPhrase(bars));
		harmony.addPhrase(makeHarmonyPhrase(bars, E4, 0.0));
		harmony.addPhrase(makeHarmonyPhrase(bars, G4, 1.0));
		melody.addPhrase(makeMelodyPhrase(bars));

		
		score.addPart(bass);
		score.addPart(drums);
		score.addPart(harmony);
		score.addPart(melody);
		
		Mod.fadeOut(score, 12);
		
		// set up audio instrument
		int sampleRate = 44100;
		Instrument bassSound = new SquareInst(sampleRate);
		Instrument drumSound = new ChiffInst(sampleRate);
		Instrument harmonySound = new SineInst(sampleRate);
		Instrument melodySound = new TriangleInst(sampleRate);
		
		// add instruments to an instrument array
		Instrument[] ensemble = {bassSound, drumSound, harmonySound, melodySound};
		
		// jMusic version 1.1 and earlier may need to elongate each phrase
		Mod.elongate(score, 0.5);
		
		//display score
		View.show(score);
		
		// render audio file of the score
		Write.au(score, "GameMusic.au", ensemble);
	}
	
	public Phrase makeBassPhrase(int bars) {
		Phrase bassPhrase = new Phrase(0.0);
		for(int i = 0; i < bars; i++) {
			Note n = new Note(C2, QUAVER, MP);
			bassPhrase.addNote(n);
			Note r = new Note(REST, QUAVER, MP);
			bassPhrase.addNote(r);
			Note n2 = new Note(G1, QUAVER, MP);
			bassPhrase.addNote(n2);
			bassPhrase.addNote(r.copy());
			Note n3 = new Note(C2, QUAVER, MP);
			bassPhrase.addNote(n3);
			bassPhrase.addNote(r.copy());
			Note n4 = new Note(G1, QUAVER, MP);
			bassPhrase.addNote(n4);
			bassPhrase.addNote(r.copy());
		}
		// last note
		Note n = new Note(C1, MINIM, MP);
		bassPhrase.addNote(n);
		return bassPhrase;
	}
	
	public Phrase makeDrumPhrase(int bars) {
		Phrase drumPhrase = new Phrase(0.0);
		for(int i = 0; i < bars; i++) {
			Note qr = new Note(REST, QUAVER);
			drumPhrase.addNote(qr);
			for (int j = 0; j < 3; j++) {
				Note n = new Note(C4, QUAVER);
				drumPhrase.addNote(n);
				Note r = new Note(REST, QUAVER);
				drumPhrase.addNote(r);
			}
			Note n = new Note(C4, QUAVER);
			drumPhrase.addNote(n);
		}
		// last note
		Note n = new Note(C4, MINIM);
		drumPhrase.addNote(n);
		return drumPhrase;
	}
	
	public Phrase makeHarmonyPhrase(int bars, int pitch, double pan) {
		Phrase harmonyPhrase = new Phrase(0.0);
		for(int i = 0; i < bars; i++) {
			Note qr = new Note(REST, QUAVER);
			harmonyPhrase.addNote(qr);
			Note n = new Note(pitch, CROTCHET);
			harmonyPhrase.addNote(n);
			Note n2 = new Note(pitch, SEMI_QUAVER);
			harmonyPhrase.addNote(n2);
			Note sqr = new Note(REST, SEMI_QUAVER);
			harmonyPhrase.addNote(sqr);
			Note n3 = new Note(pitch, CROTCHET_TRIPLET);
			harmonyPhrase.addNote(n3);
			harmonyPhrase.addNote(n3.copy());
			harmonyPhrase.addNote(n3.copy());
		}
		harmonyPhrase.setPan(pan);
		return harmonyPhrase;
	}
	
	public Phrase makeMelodyPhrase(int bars) {
		Phrase melodyPhrase = new Phrase();
		for(int i = 0; i < bars; i++) {
			double beatCounter = 0.0;
			int pitch = (int)(Math.random() * 3) * 2 + 72;
			for (; beatCounter < 3.0; ) {
				double length = 0.25;
				Note n = new Note(pitch, length);
				n.setPan(Math.random());
				if (n.isScale(PENTATONIC_SCALE)) {
					if (n.getPitch() % 12 == 0) {
						n.setRhythmValue(CROTCHET);
						n.setDuration(0.9);
						length = 1.0;
					}
					if (n.getPitch() % 12 == 4 || n.getPitch() % 12 == 7) {
						n.setRhythmValue(QUAVER);
						n.setDuration(0.45);
						length = 0.5;
					}
					melodyPhrase.addNote(n);
					beatCounter += length;
				}
				pitch += (int)(Math.random() * 8 - 4);
			}
			if (Math.random() > 0.8) {
				Note r = new Note(REST, (4.0 - beatCounter));
				melodyPhrase.addNote(r);
			} else {
				if (pitch % 2 == 1) pitch --;
				if (pitch % 2 == 0 && pitch % 12 > 4) pitch--;
				Note p = new Note(pitch, 4.0 - beatCounter);
				melodyPhrase.addNote(p);
			}
		}
		return melodyPhrase;
	}
}


	
