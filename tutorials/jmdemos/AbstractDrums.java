import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class is a simple outline for a drum class
* Being abstract it must be subclassed.
* It declares some useful protected objects for 
* making drum patterns in the subclasses.
* @author Andrew Brown
*/

abstract class AbstractDrums implements JMC {
	protected static Score drumScore = new Score("Drums", 130.0);
	protected static Part kickPart = new Part("Kick", 0, 9);
	protected static Part snarePart = new Part("Snare", 0, 9);
	protected static Part hatsPart = new Part("Hats", 0, 9);
	protected static Phrase kickPhrase = new Phrase("Kick Phrase", 0.0);
	protected static Phrase snarePhrase = new Phrase("Snare Phrase", 0.0);
	protected static Phrase hatsPhrase = new Phrase("Hats Phrase", 0.0);	
	protected static Note kickNote = new Note(C2, 1.0);
	protected static Note snareNote = new Note(D2, 1.0);
	protected static Note hatsNote = new Note(FS2, 1.0);
	protected static Note restNote = new Note(REST, 1.0);
	protected static String fileName = "Drums.mid";
	
	public AbstractDrums() {
		kick();
		kickPart.addPhrase(kickPhrase);
		drumScore.addPart(kickPart);
	    snare();
	    snarePart.addPhrase(snarePhrase);
		drumScore.addPart(snarePart);
	    hats();
	    hatsPart.addPhrase(hatsPhrase);
		drumScore.addPart(hatsPart);
	}
	
	abstract void kick();
	
	abstract void snare();
	
	abstract void hats();
}
