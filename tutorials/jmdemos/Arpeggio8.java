import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class adds an instrument to an arpeggio.
* @author Andrew Brown
*/

public class Arpeggio8 implements JMC {
	
	public static void main(String[] args) {
		 new Arpeggio8();
    }
    
    public Arpeggio8() {
        int pitch = C2;
        int[] pitches = {pitch, pitch + 5, pitch + 10};
        Phrase arpPhrase = new Phrase();
         
        // create a repeat loop
        for(int r = 0; r < 3; r++) {
	        // turn pitches into a phrase
	        for(int i = 0; i < pitches.length; i++) {
	            Note n = new Note(pitches[i], SEMI_QUAVER);
	            arpPhrase.addNote(n);
	        }
        }
        // put the phrase in a part
        Part p = new Part("Arp", SYNTH_BASS, 1);
        p.addPhrase(arpPhrase);
        
        // save it as a file
        Write.midi(p, "Arpeggio8.mid");

	}
	
}
