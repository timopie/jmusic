import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class makes an up-down  arpeggio without repeating top and bottom notes.
* @author Andrew Brown
*/

public class Arpeggio13 implements JMC {
	
	public static void main(String[] args) {
		 new Arpeggio13();
    }
    
    public Arpeggio13() {
        int pitch = C2;
        int[] pitches = {pitch, pitch + 4, pitch + 7, pitch + 10};
        Phrase arpPhrase = new Phrase();
        int octaves = 2;
        int repeats = 4;
        
        // an arrahy of rhythm values
        double[] rhythmValues = {SEMI_QUAVER, SEMI_QUAVER, DOTTED_SEMI_QUAVER, DEMI_SEMI_QUAVER};
        
        // rhythm index
        int rvIndex = 0;
        
        // create a repeat loop
        for(int r = 0; r < repeats; r++) {
            // create an octave loop for up
            for(int oct = 0; oct < octaves; oct++) {
		        // up notes
		        for(int i = 0; i < pitches.length; i++) {
		            Note n = new Note(pitches[i] + (oct * 12), rhythmValues[rvIndex]);
		            arpPhrase.addNote(n);
		            rvIndex++;
		            if (rvIndex >= rhythmValues.length) rvIndex = 0;
		        }
		    }
		    // create an octave loop for down
            for(int oct = octaves -1; oct >= 0; oct--) {
		        // down notes
		        for(int i = pitches.length - 1; i >= 0; i--) {
		            // avoid first and last note of the down cycle
		            if(!((i == pitches.length - 1 && oct == octaves -1) || ( i == 0 && oct == 0))) {
		                Note n = new Note(pitches[i] + (oct * 12), rhythmValues[rvIndex]);
		                arpPhrase.addNote(n);
		                rvIndex++;
		                if (rvIndex >= rhythmValues.length) rvIndex = 0;
		            }
		           
		        }
	        }
        }
        
        // put the phrase in a part
        Part p = new Part("Arp", SYNTH_BASS, 1);
        p.addPhrase(arpPhrase);
        
        // save it as a file
        Write.midi(p, "Arpeggio13.mid");
	}
}
