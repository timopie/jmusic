import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;
import qt.*;
import qt.QTCycle; 
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;


public class JM550 extends JFrame implements JMC, ActionListener, ChangeListener, ListSelectionListener {
    private static JButton start, stop;
    private static QTCycle player;
    private static JSlider tempoSlider, volumeSlider;
    private static JLabel tempoValue, volumeValue;
    private static JList patterns;
    private static Basic_550 pat;
    
    public static void main(String[] args) {
        new JM550();
        // pattern
        pat = new EightBeat1();
        pat.changeDynamics();
        // set up instrument
        pat.drums.setInstrument(POWER_KIT);
        // play with QuickTime
        player = new QTCycle(pat.getScore());
		player.startPlayback();
		player.suspendPlayback();
		// show adjustment controls
		player.settings();
    }
    
    public JM550() {
        super("JM-550 Drum Machine");
                        
        JPanel mainPanel = new JPanel(new BorderLayout());
        this.getContentPane().add(mainPanel);
        
        // Tempo JPanel
        Box tempoBox = new Box(BoxLayout.Y_AXIS);
        JLabel tempoLabel = new JLabel("Tempo" , JLabel.CENTER);
        // additional panel seems to be required to centre the components :(
        JPanel tempoLabelPanel = new JPanel();
        tempoLabelPanel.add(tempoLabel);
        tempoBox.add(tempoLabelPanel);

        // JSlider arguments - orientation,  min, max, initial value
        tempoSlider = new JSlider(Adjustable.VERTICAL, 0, 250, 130);
        tempoSlider.addChangeListener(this);
        JPanel tempoSliderPanel = new JPanel();
        tempoSliderPanel.add(tempoSlider);
        tempoBox.add(tempoSliderPanel);
        
        tempoValue = new JLabel("130" , JLabel.CENTER);
        JPanel tempoValuePanel = new JPanel();
        tempoValuePanel.add(tempoValue);
        tempoBox.add(tempoValuePanel);
        
        mainPanel.add(tempoBox, "West");
        
        // pattern JList
        String[] data = {"EightBeat1", "EightBeat1Fill", "SixteenBeat1", 
            "SixteenBeat2", "SixteenBeat3", "SixteenBeat4", "SixteenBeat4fillin",
             "SixteenBeat5","HardRock", "HardRockFill", "Rap1",
            "Rap1fillin", "Rap2", " Merengue", "Reggae", "Rhumba"};
        patterns = new JList(data);
        patterns.setSelectedIndex(0); // only select one item at a time
        patterns.addListSelectionListener(this);
        patterns.setSelectedIndex(0);
        JScrollPane scrollPane = new JScrollPane(patterns, 
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                
        mainPanel.add(scrollPane, "Center");
        
        // volume sider section
        Box volumeBox = new Box(BoxLayout.Y_AXIS); 
              
        JLabel volumeLabel = new JLabel("Volume", JLabel.CENTER);
        JPanel volumeLabelPanel = new JPanel();
        volumeLabelPanel.add(volumeLabel);
        volumeBox.add(volumeLabelPanel);

        // JSlider arguments - orientation,  min, max, initial value
        volumeSlider = new JSlider( Adjustable.VERTICAL, 1, 127, 100);
        volumeSlider.addChangeListener(this);
        JPanel volumeSliderPanel = new JPanel();
        volumeSliderPanel.add(volumeSlider);
        volumeBox.add(volumeSliderPanel);
        
        volumeValue = new JLabel("100", JLabel.CENTER);
        JPanel volumeValuePanel = new JPanel();
        volumeValuePanel.add(volumeValue);
        volumeBox.add(volumeValuePanel);
        
        mainPanel.add(volumeBox, "East");
        
        
        // create transport JPanel
        JPanel transport = new JPanel();
        
        start = new JButton("Play");
        start.addActionListener(this);
        transport.add(start);
        
        stop = new JButton("Stop");
        stop.addActionListener(this);
        transport.add(stop);
        
        mainPanel.add(transport, "South");
        
        this.setSize(250, 300);
             
        this.setVisible(true);
    }
    
    // deal with JButton clicks
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == start) player.resumePlayback();
        if (ae.getSource() == stop) player.suspendPlayback(); 
    }
    
    // deal with slider movements
    public void stateChanged(ChangeEvent ce) {
        // tempo
        if (ce.getSource() == tempoSlider) {
            int value = tempoSlider.getValue();
            tempoValue.setText(Integer.toString(value));
            player.setNextTempo(value);
        }
        // volume
        if (ce.getSource() == volumeSlider) {
            int value = volumeSlider.getValue();
            volumeValue.setText(Integer.toString(value));
            pat.setScoreVolume(value);
            pat.changeDynamics();
            player.setScore(pat.getScore());
        }
    }
    
    // deal with JList selections
    public void valueChanged(ListSelectionEvent lse) {
        if (lse.getSource() == patterns) {
            int selected = patterns.getSelectedIndex();
            if (selected == 0) pat = new EightBeat1();
            if (selected == 1) pat = new EightBeat1FillIn();
            if (selected == 2) pat = new SixteenBeat1();
            if (selected == 3) pat = new SixteenBeat2();
            if (selected == 4) pat = new SixteenBeat3();
            if (selected == 5) pat = new SixteenBeat4();
            if (selected == 6) pat = new SixteenBeat4fillin();
            if (selected == 7) pat = new SixteenBeat5();
            if (selected == 8) pat = new SixteenBeat5fillin();
            if (selected == 9) pat = new HardRock();
            if (selected == 10) pat = new HardRockFill();
            if (selected == 11) pat = new Rap1();
            if (selected == 12) pat = new Rap1fillin();
            if (selected == 13) pat = new Rap2();
            if (selected == 14) pat = new Merengue();
            if (selected == 15) pat = new Reggae();
            if (selected == 16) pat = new Rhumba();
            pat.changeDynamics();
            player.setScore(pat.getScore());
        }
    }
}
