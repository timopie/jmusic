import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class makes an arpeggio using a repeats variable.
* @author Andrew Brown
*/

public class Arpeggio9 implements JMC {
	
	public static void main(String[] args) {
		 new Arpeggio9();
    }
    
    public Arpeggio9() {
        int pitch = C2;
        int[] pitches = {pitch, pitch + 4, pitch + 7, pitch + 10};
        Phrase arpPhrase = new Phrase();
         
        // create a repeat loop
        int repeats = 3;
        for(int r = 0; r < repeats; r++) {
	        // turn pitches into a phrase
	        for(int i = 0; i < pitches.length; i++) {
	            Note n = new Note(pitches[i], SEMI_QUAVER);
	            arpPhrase.addNote(n);
	        }
        }
        
        // put the phrase in a part
        Part p = new Part("Arp", SYNTH_BASS, 1);
        p.addPhrase(arpPhrase);
        
        // save it as a file
        Write.midi(p, "Arpeggio9.mid");

	}
	
}
