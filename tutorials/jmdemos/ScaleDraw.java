/*

< This Java Class is part of the jMusic API >

Copyright (C) 2000 Andrew Sorensen & Andrew Brown

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
import jm.JMC;
import jm.music.data.*;
import jm.music.display.*;
import jm.midi.*;

/**
 * A short example which generates a one octave c chromatic scale
 * and writes to a MIDI file called scale.mid
 * @author Andrew Brown
 */
public final class ScaleDraw implements JMC{
	public static void main(String[] args){
		Score s = new Score("ScaleDraw Test");
		Part p = new Part("Flute", 1, FLUTE);
		Phrase phr = new Phrase(); 
		
		for(short i=0;i<24;i++){
			Note note = new Note(C4+(i*2), 1.0);
			phr.addNote(note);
		}
		// add the highest note to show where it appears 
		Note n = new Note(127, 4.0);
		phr.addNote(n);
		// add the lowest note to show where it appears
		n = new Note(0, 3.0);
		phr.addNote(n);
		
	    // combine the 
		p.addPhrase(phr);
		s.addPart(p);
		
		//testing ScoreDraw Object
		new DrawScore(s);
		//write a MIDI file to disk
		s.writeMIDI("ScaleDraw.mid");
	
		System.out.println("Done!");
	}
}
