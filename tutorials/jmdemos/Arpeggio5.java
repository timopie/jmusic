import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class sorts a series of pitches then arpeggiates them
* pitches are in an interesting order
* @author Andrew Brown
*/

public class Arpeggio5 implements JMC {
	
	public static void main(String[] args) {
		 new Arpeggio4();
    }
    
    public Arpeggio5() {
        int[] pitches = {C4, G4, F4, C5, E4};
        
        // sort
        bubbleSortArray(pitches);
        
        // turn pitches into a phrase
        Phrase arpPhrase = new Phrase();
        for(int i = 0; i < pitches.length; i++) {
            Note n = new Note(pitches[i], SEMI_QUAVER);
            arpPhrase.addNote(n);
        }
        
        // repeat the arpeggio a few times
        Mod.repeat(arpPhrase, 3);
        
        // save it as a file
        Write.midi(arpPhrase, "Arpeggio5.mid");

	}
	
	/**
	* Order the note pitches from lowest to highest
	*/
	public static void bubbleSortArray(int[] notes) {
	    int i, j;
	    for(i=0; i<notes.length; i++) {
	        for(j=notes.length-1; j> i; j--) {
	            if (notes[j] < notes [j-1]) {
	                //swap
	                int temp = notes[j];
	                notes[j] = notes[j-1];
	                notes[j-1] = temp;
	            }
	        }
	    }
	}
}
