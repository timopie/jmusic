import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class turns a one pitch into a series of pitches
* then into an arpeggio.
* @author Andrew Brown
*/

public class Arpeggio6 implements JMC {
	
	public static void main(String[] args) {
		 new Arpeggio6();
    }
    
    public Arpeggio6() {
        int pitch = C2;
        
        int[] pitches = {pitch, pitch + 5, pitch + 10};
        
        // turn pitches into a phrase
        Phrase arpPhrase = new Phrase();
        for(int i = 0; i < pitches.length; i++) {
            Note n = new Note(pitches[i], SEMI_QUAVER);
            arpPhrase.addNote(n);
        }
        
        // repeat the arpeggio a few times
        Mod.repeat(arpPhrase, 3);
        
        // save it as a file
        Write.midi(arpPhrase, "Arpeggio6.mid");

	}
	
}
