import jm.music.data.*;
import jm.JMC;
import jm.util.*;
import jmqt.QTPlayer;
import jm.music.tools.*;

public class Dot06 implements JMC {
    private Score aScore = new Score("Composition and Arrangement");
    private int partCounter = 0;
    
    public static void main(String[] args) { 
        new Dot06();
    }
    
    // the constructor method
    public Dot06() {
        // musical material
        double[] phraseOneData = {C4, QUARTER_NOTE};
        double[] phraseTwoData= {REST, EIGHTH_NOTE, E4, EIGHTH_NOTE, REST, QUARTER_NOTE};        
        double[] phraseThreeData= {REST, QUARTER_NOTE, F4, EIGHTH_NOTE, REST, EIGHTH_NOTE};
        
        // arrangement and orchestration
        notesToPart( phraseOneData, 0.0, 48, CLARINET);
        notesToPart( phraseTwoData, 4.0, 9, FLUTE);
        notesToPart( phraseThreeData, 8.0, 7, NYLON_GUITAR);
        notesToPart( phraseTwoData, 30.0, 7, FLUTE);
        notesToPart( phraseThreeData, 32.0, 7, HORN);
        
        aScore.setTempo(140.0);
        
        View.sketch(aScore);
        
        QTPlayer.display(aScore);
        //Play.midi(aScore); // alternate playback if you don't have QuickTime
        
    }
    
    private void notesToPart(double[] notes, double startTime, int repeats, int instrument) {
        // create a new phrase from the notes and loop it
        Phrase aPhrase = new Phrase(startTime);
        aPhrase.addNoteList(notes);
        Mod.repeat(aPhrase, repeats);
        // create a new part and add the phrase to it
        Part aPart = new Part("Part "+partCounter, instrument, partCounter);
        aPart.addPhrase(aPhrase);
        // keep track of how many parts have been created
        partCounter++;
        // add the part to the score
        aScore.addPart(aPart);
    }
}

