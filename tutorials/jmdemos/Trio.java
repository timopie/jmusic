//==========================================================
//File:                 Trio.java
//Function:             Shows the separataion of code into methods 
//						and the reuse of code through methods.
//Author:               Andrew Brown
//Environment           Java 1.1.7 with jMusic extensions
//============================================================
import jm.JMC;
import jm.music.data.*;
import jm.midi.*;

public class Trio implements JMC {
	//class attributes
	static Score score = new Score("JMDemo - Trio");
	static double beats = 0;//how long is the music?
		
	// simple main method called when the class in run
	public static void main(String[] args) {
		new Trio();
	}
	
	// constructor
	public Trio() {
		//flute part
		doFlute();
		//clarinet part
		doClarinet();
		//bassoon part
		doBassoon();
		//write a MIDI file to disk
		score.writeMIDI("Trio.mid");
	}
	
	private static void doFlute() {	
		Part flute = new Part("A", 1, FLUTE);	
		flute.addPhrase(doPhrase(C5));
		score.addPart(flute);
	}
	
	private static void doClarinet() {
		Part clarinet = new Part("B", 2, CLARINET);
		clarinet.addPhrase(doPhrase(C4));
		score.addPart(clarinet);
	}
	
	private static void doBassoon() {
		Part bassoon = new Part("C", 3, BASSOON);
		bassoon.addPhrase(doPhrase(C3));
		score.addPart(bassoon);
	}
	
	private static Phrase doPhrase(int pitch) {
		Phrase phr = new Phrase();
		for(int i=0;i<100;i++){
			double rv = SQ*((int)(Math.random()*2)+1);
			Note note = new Note(pitch+=((int)(Math.random()*3)-1)*2,rv,(int)(Math.random()*40)+50);
			phr.addNote(note);
			beats += rv;
		}
		return phr;
	}
}
