import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class turns a series of unordered pitches into an argeggio
* pitches are in an interesting order
* @author Andrew Brown
*/

public class Arpeggio4 implements JMC {
	
	public static void main(String[] args) {
		 new Arpeggio4();
    }
    
    public Arpeggio4() {
        int[] pitches = {C4, G4, F4, C5, C4, G4, A4, G4};
        // turn pitches into a phrase
        Phrase arpPhrase = new Phrase();
        for(int i = 0; i < pitches.length; i++) {
            Note n = new Note(pitches[i], SEMI_QUAVER);
            arpPhrase.addNote(n);
        }
        
        // reverse the pitches
        Mod.palindrome(arpPhrase); // optional boolean second argument
        
        // repeat the arpeggio a few times
        Mod.repeat(arpPhrase, 3);
        
        // save it as a file
        Write.midi(arpPhrase, "Arpeggio4.mid");

	}
}
