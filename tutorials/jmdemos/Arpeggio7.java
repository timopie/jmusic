import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class makes an arpeggio without the Mod methods.
* @author Andrew Brown
*/

public class Arpeggio7 implements JMC {
	
	public static void main(String[] args) {
		 new Arpeggio7();
    }
    
    public Arpeggio7() {
        int pitch = C2;
        int[] pitches = {pitch, pitch + 5, pitch + 10};
        Phrase arpPhrase = new Phrase();
         
        // create a repeat loop
        for(int r = 0; r < 3; r++) {
	        // turn pitches into a phrase
	        for(int i = 0; i < pitches.length; i++) {
	            Note n = new Note(pitches[i], SEMI_QUAVER);
	            arpPhrase.addNote(n);
	        }
        }
        
        // save it as a file
        Write.midi(arpPhrase, "Arpeggio7.mid");

	}
	
}
