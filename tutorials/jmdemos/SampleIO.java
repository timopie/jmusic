import jm.JMC;
import jm.music.data.*;
import jm.midi.*;
import jm.util.*;
import jm.audio.*;
import jm.audio.io.*;
import jm.audio.synth.*;

public class SampleIO implements JMC {
	
	public static void main(String[] args) {
	    new SampleIO();
	}
	
	public SampleIO() {
		Phrase phr = new Phrase();
		for(int i=0; i<4; i++) {
			Note n = new Note(C4, 0.25, 127);
			phr.addNote(n);
		}
		for(int i=0; i<2; i++) {
			Note n = new Note(C4, 0.5, 127);
			phr.addNote(n);
		}
		for(int i=0; i<4; i++) {
			Note n = new Note(C4, 0.25, 127);
			phr.addNote(n);
		}
		for(int i=0; i<1; i++) {
			Note n = new Note(C4, 0.5, 127);
			phr.addNote(n);
		}
		for(int i=0; i<4; i++) {
			Note n = new Note(C4, 0.125, 127);
			phr.addNote(n);
		}
		for(int i=0; i<1; i++) {
			Note n = new Note(C4, 4.0, 127);
			phr.addNote(n);
		}
		Score score = new Score(new Part (phr));

		Instrument ssi = new SimpleSampleInst("HiThere.au", FRQ[C4]);
		
		Write.au(score, "SampleIO.au", ssi);
	}
}
