import jm.JMC;
import jm.music.data.*;
import jm.music.tools.*;
import jm.util.*;

/**
* This class uses a random walk on dynamic
* values to change the balance of parts
* in a continuous fashion.
* @author Andrew Brown
*/

public class DynaMix implements JMC {
    private static int partCount = 0;
    
	public static void main(String[] args) {
		new DynaMix();
	}
	
	public DynaMix() {
	    Score s = new Score("Dyna Mix", 130);
	    s.addPart(makePart(BASSOON, 30)); 
	    s.addPart(makePart(XYLOPHONE, 60));
	    s.addPart(makePart(FLUTE, 80));
	    
	    Mod.normalise(s);
	    View.show(s);
	    //View.print(s);
	    Write.midi(s, "DynaMix.mid");
	}
	    
	    
	private Part makePart(int instrument, int startPitch) {
	    Part p = new Part("Part "+ partCount, instrument, partCount);
	    Phrase phr = new Phrase(Math.round(Math.random() * 20.0 * partCount));
	    partCount ++;
	    int pitch = startPitch;
	    int dynamic = (int)(Math.random() * 60 + 30);
	    int noteCount = 20;
	    int totalNotes = 100;
	    double[] rhythms = {CROTCHET, QUAVER};
	    
	    // make a note array in the phrase
	    for (int i = 0; i < noteCount; i++) {
	        Note n = new Note(pitch, rhythms[(int)(Math.random() * rhythms.length)], dynamic);
	        if (! n.isScale(MAJOR_SCALE)) n.setPitch(n.getPitch() + 1);
	        phr.addNote(n);
	        // update values
	        pitch += (int)(Math.random() * 9 - 4);
	        if (pitch < 12 ) pitch = 12;
	        if (pitch > 127) pitch = 127;
	        dynamic += (int)(Math.random() * 20 - 10);
	        if (dynamic < 12 ) dynamic = 20;
	        if (dynamic > 127) dynamic = 120;
	    }
	    
	    // repeat pitches and rhythms but alter dynamics
	    for (int i = noteCount; i < totalNotes; i++) {
	        Note n = phr.getNote(i%noteCount).copy();
	        n.setDynamic(dynamic);
	        phr.addNote(n);
	        // update dyn amount
	        dynamic += (int)(Math.random() * 30 - 15);
	        if (dynamic < 1 ) dynamic = 5;
	        if (dynamic > 127) dynamic = 120;
	    }
	    
	    // create a part
	    p.addPhrase(phr);
	    
	    return p;
	}
}
