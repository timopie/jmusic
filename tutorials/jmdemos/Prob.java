import jm.JMC;
import jm.music.data.*;
import jm.midi.*;
import jm.util.*;
import jm.audio.*;
import jm.gui.helper.HelperGUI;
 
/**
 * @author Andrew Brown
 */
 
public final class Prob extends HelperGUI implements JMC{
	public static void main(String[] args){
		new Prob();
	}
         
        public Prob() {
            super();
            setVariableA(100,"Kick down beat prob.");
            setVariableB(95,"Main snare & hat prob.");
            setVariableC(70,"Hat off-beat prob.");
            setVariableD(5,"Kick and Snare off-beat prob.");
            setVariableE(60,"Tempo");
        }
        
        public Score compose() {
            Score s = new Score("Test");
            Part p = new Part("Drums", 25, 9);
            Phrase kPhrase = new Phrase("Kick drum",0.0);
            Phrase sPhrase = new Phrase("Snare drum",0.0);
            Phrase hPhrase = new Phrase("Hi hats",0.0);
            
            s.setTempo(variableE * 2);
		
            // probability array
            double[] kickProbs = {variableA / 100.0, variableD / 100.0,
             variableD / 100.0, variableD / 100.0};
            double[] snareProbs = {variableD / 100.0, variableD / 100.0,
             variableB / 100.0, variableD / 100.0};
            double[] hatProbs = {variableB / 100.0, variableC / 100.0,
             variableB / 100.0, variableC / 100.0};
 
            //create the phrase note by note
            double choice;
            for (int r = 0; r < 8; r++) {
                for(int i=0; i<kickProbs.length; i++){
                  	choice = Math.random();
                  	if(choice < kickProbs[i]) {
      								Note n = new Note(C2, SEMI_QUAVER);
      								kPhrase.addNote(n);
      							} else {
      								Note n = new Note(REST, SEMI_QUAVER);
      								kPhrase.addNote(n);
      							}
                }
                for(int i=0; i<snareProbs.length; i++){
                  	choice = Math.random();
                  	if(choice < snareProbs[i]) {
			      					Note n = new Note(D2, SEMI_QUAVER);
			      					sPhrase.addNote(n);
			      				} else {
				      				Note n = new Note(REST, SEMI_QUAVER);
				      				sPhrase.addNote(n);
				      			}
                }
                for(int i=0; i<hatProbs.length; i++){
                  	choice = Math.random();
                  	if(choice < hatProbs[i]) {
				      				Note n = new Note(FS2, SEMI_QUAVER);
				      				hPhrase.addNote(n);
				      			} else {
				      				Note n = new Note(REST, SEMI_QUAVER);
				      				hPhrase.addNote(n);
				      			}
                }
            }
                
            p.addPhrase(kPhrase);
            p.addPhrase(sPhrase);
            p.addPhrase(hPhrase);
            s.addPart(p);
            
            return s;
        }     
}