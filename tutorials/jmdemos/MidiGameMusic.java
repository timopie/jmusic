import jm.JMC;
import jm.music.data.*;
import jm.music.tools.Mod;
import jm.util.Read;
import jmqt.*;
import java.awt.*;

/**
* This class provides a musical scores for any class that calls the makeMusic method.
*/

public class MidiGameMusic implements JMC{
    private int tempo = 60;
    private int numberOfBars = 4;
    
    public MidiGameMusic() {}

    /**
    * Create a score and return it to the calling class.
    */
    protected Score makeMusic() {
        Score score = new Score();
        // prompt to read in a file
        FileDialog fd = new FileDialog(new Frame(), 
                "Select a MIDI file to open.", 
                FileDialog.LOAD);
        fd.show();
        if (fd.getFile() != null) {
            Read.midi(score, fd.getDirectory() + fd.getFile());                
        }
        return score;
    }
}