
import jm.music.data.*;
import jm.JMC;
import jm.util.*;
import jm.audio.*;

/**
 * An example which creates a known chorale
 * It is an example of the use of the Phrase.addNoteList() method
 * @author Andrew Sorensen
 */

public final class AudioChorale implements JMC{
	public static void main(String[] args){
	    //set up instruments and ensemble
		Instrument[] ensemble = new Instrument[4];
		for (int i=0; i<4; i++) {
			TriangleInst tri = new TriangleInst(8000);
			ensemble[i] = tri;
		}
	    
		// set up the pitches and durations of each line
		int[] pitchSop = {C5,G4,E4,D4,G4,a4,c4,D4,E4,D4,F4,E4,A4,G4,E4};
		double[] rhythmSop = {Q,Q,DQ,SQ,Q,Q,Q,Q,Q,Q,Q,Q,C,Q,Q};
		int[] pitchAlto = {E4,D4,C4,A3,B3,C4,D4,C4,D4,CS4};
		double[] rhythmAlto = {Q,Q,M,Q,Q,Q,Q,M,Q,Q};
		int[] pitchTenor = {G3,C4,B3,A3,G3,F3,E3,G3,G3,C4,B3,A3,B3,A3};
		double[] rhythmTenor = {C,Q,SQ,SQ,Q,Q,Q,Q,C,Q,Q,C,Q,Q};
		int[] pitchBass = {C3,B2,A2,G2,F2,E2,F2,A2,G2,C3,B2,A2,G2,F2,F3,E3,A3};
		double[] rhythmBass = {Q,Q,Q,SQ,SQ,Q,Q,Q,Q,Q,Q,Q,Q,Q,Q,Q,Q};
		
		//create the jMusic objects
		Phrase soprano = new Phrase();
		Phrase alto = new Phrase();
		Phrase tenor = new Phrase();
		Phrase bass = new Phrase();
		
		// add the notes to each phrase
		soprano.addNoteList(pitchSop, rhythmSop);
		alto.addNoteList(pitchAlto, rhythmAlto);
		tenor.addNoteList(pitchTenor, rhythmTenor);
		bass.addNoteList(pitchBass, rhythmBass);
		
		// create jMusic parts
		Part s = new Part("Soprano", 0);
		Part a = new Part("Alto", 1);
		Part t = new Part("Tenor",  2);
		Part b = new Part("Bass", 3);
		
		// add the phrases to the parts
		s.addPhrase(soprano);
		a.addPhrase(alto);
		t.addPhrase(tenor);
		b.addPhrase(bass);
		
		//create a score
		Score score = new Score("Chorale");
		
		//add the parts to the score
		score.addPart(s);
		score.addPart(a);
		score.addPart(t);
		score.addPart(b);
		
		//display the result for the world to see
		View.show(score);
				
		// save score as an audio file
		Write.au(score, "AudioChorale.au", ensemble);
	}
}
	
