import jm.music.data.*;
import jm.music.tools.Mod;
import jm.JMC;
import jm.util.*;
import jm.audio.*;

/**
* 12 tone serial composition
* @author Andrew Brown
*/


public class ControlledHPFMusic implements JMC {
	// run me
	public static void main(String[] args) {
		new ControlledHPFMusic();
	}
	// constructor
	public ControlledHPFMusic() {
            // make a tone row
            int[] row = new int[12];
            int completed = 0;
            while(completed < 12) {
                int nextPitch = (int)(Math.random() * 12 + 36);
                // is it already chosen?
                boolean unique = true;
                for(int i=0; i < row.length;i++ ) {
                    if(row[i] == nextPitch) unique = false;
                }
                if(unique) row[completed++] = nextPitch;
                System.out.println("Number in row selected = " + completed);
            }
            // make a phrase of the row
            Phrase original = new Phrase(0.0);
            for(int i =0; i < row.length; i++) {
                Note n = new Note(row[i], CROTCHET, (int)(Math.random() * 40 + 80));
                original.addNote(n);
            }
            // make variations
            Phrase backwards = original.copy();
            backwards.setStartTime(12.0);
            Mod.transpose(backwards, 24);
            Mod.retrograde(backwards);
            Mod.elongate(backwards, 0.5);
            Phrase backwards2 = backwards.copy();
            backwards2.setStartTime(30.0);
            
            Phrase flipped = original.copy();
            Mod.transpose(flipped, 48);
            Mod.inversion(flipped);
            Mod.elongate(flipped, 0.25);
            
            Mod.repeat(original, 4);
            
            // arrange
            Part part = new Part();
            part.addPhrase(original);
            part.addPhrase(backwards);
            part.addPhrase(backwards2);
            for(int i=0; i<6; i++) {
                Phrase phr = flipped.copy();
                phr.setStartTime(Math.random() * 40.0);
                part.addPhrase(phr);
            }
            
            Score s = new Score("Twelve tone serialist piece");
            s.addPart(part);
            
            // audio
            Instrument inst = new ControlledHPFInst(44100);
            Write.au(s, "ControlledHPFMusic2.au", inst);
      }
}
            
                    
                    
            
