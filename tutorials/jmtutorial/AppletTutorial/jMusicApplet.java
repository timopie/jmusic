import java.awt.Graphics;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.applet.Applet;

import jm.JMC;
import jm.music.data.*;
import jm.util.Read;
import qt.QTUtil;

public class jMusicApplet extends Applet implements JMC, ActionListener {
	private Button playBtn;
	private Score score = new Score();
	
	/**
	* Initialize the applet. First resize it, then get the
	 * "snd" attribute.
	 */
	public void init() {
		setBackground(Color.yellow);
		playBtn = new Button("Play");
		playBtn.addActionListener(this);
		this.add(playBtn);
	}
	/**
	* When the applet is started play the next sound.
	 */
	public void start() {	}

	/**
	* Deal with the mouse click.
	 */
	public void actionPerformed( ActionEvent e ) {
		if ( e.getSource() == playBtn) {
			compose();
			play();
		}
	}

	/**
	* Create a new score.
	 */
	private void compose() {
		int[] pitchSop = {C5, G4, E4, D4, G4, A4,C4,D4,E4,D4,F4,E4,A4,G4,E4};
		double[] rhythmSop = {CROTCHET, C,DC,Q,C,C,C,C,C,C,C,C,M,C,C};
		Phrase soprano = new Phrase();
		soprano.addNoteList(pitchSop, rhythmSop);
		this.score = new Score(new Part (soprano));;
		this.score.setTempo(130);
	}

	/**
		* Play the score with QuickTime.
	 */
	private void play() {
		QTUtil qtu = new QTUtil();
		qtu.playback(score);
	}

	public static void main(String[] args) {
		Applet theApplet = new jMusicApplet();
		Frame theFrame = new Frame();
		theFrame.setSize(100,100);
		theFrame.add(theApplet);;
		theApplet.init();
		theApplet.start();
		theFrame.setVisible(true);
	}
}