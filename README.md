jMusic version 1.6.6 - Now Git Enabled
--------------------
`git clone https://git.code.sf.net/p/jmusic/src jmusic-src`   


Development is on the development Branch:
-----------------------------------------
\# Check out the "public" branch  
`git checkout development`  

\# Get the latest version from remote    
`git pull`  

\# create and checkout a new feature branch     
`git checkout -b feature_you_are_working_on`  

\# do stuff here.. Make commits.. test...   
`git add .`  
`git commit -m "My feature goes bing"`

\# Update your repository's origin branches from remote repo     
`git fetch origin`  

\# Update branch and place your commits on top  
`git rebase development`  

\# Switch to the development branch    
`git checkout development`  

\# merge your commits to dev branch    
`git merge feature_you_are_working_on`  

\# Push the public branch up to repository    
`git push -u origin development`  


>See these for more details:  
>(https://randyfay.com/content/rebase-workflow-git)  
>(https://git-scm.com/docs/git-rebase)*  


Tagged Releases are on the master Branch (need privileges to write):
--------------------------------------------------------------------
`git checkout master`   
  

Historical version are on the historical branch:
------------------------------------------------  
`git checkout historical`   
(read only)   
